//
//  NavigationView.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//


import UIKit
import SnapKit
import RxSwift
import RxCocoa

enum NavigationType {
    case small
    case smallBlue
    case normal
    case none
}

class NavigationView: UIView {
    let disposeBag = DisposeBag()
    
    var onBackgroundChange:((UIColor)->Void)?
    
    var navType: NavigationType = .normal {
        didSet {
            var tintColor = AppTheme.Color.navTint
            var btnColor = AppTheme.Color.navBtn
            var bgColor = AppTheme.Color.navBG
//            let buttons = [btnBack, btnClose]
            
            switch navType {
            case .small:
                lblTitle.isHidden = false
                titleView.isHidden = true
                
            case .smallBlue:
                tintColor = AppTheme.Color.navBlueTint
                btnColor = AppTheme.Color.navBlueBtn
                bgColor = AppTheme.Color.navBlueBG
                
                lblTitle.isHidden = false
                titleView.isHidden = true
                
            case .none:
                btnView.isHidden = true
                titleView.isHidden = true
                
            default:
                lblTitle.isHidden = true
                titleView.isHidden = false
            }
            
//            buttons.forEach { (button) in
//                button.tintColor = btnColor
//            }
            lblTitle.textColor = tintColor
            lblTitleLarge.textColor = tintColor
            backgroundColor = bgColor
            onBackgroundChange?(bgColor)
        }
    }
    
    var navTitle: String = "" {
        didSet {
//            lblTitle.sdkLocKey = navTitle
//            lblTitleLarge.sdkLocKey = navTitle
            
            if navTitle.isEmpty {
                lblTitle.isHidden = true
                titleView.isHidden = true
            }
        }
    }
    
    lazy var vStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        
        return stack
    }()
    
    lazy var btnView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.snp.makeConstraints({ (maker) in
            maker.height.equalTo(30.0)
        })
        
        return view
    }()
    
    lazy var lhStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        
        return stack
    }()
    
    lazy var btnBack: UIButton = {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = .scaleAspectFill
//        button.setImage(UIImage.initSDK("ic_nav_back"), for: .normal)
        button.tintColor = AppTheme.Color.navTint
        button.isHidden = true
        button.snp.makeConstraints({ (maker) in
            maker.width.equalTo(button.snp.height)
        })
        
        button.rx.controlEvent(.touchUpInside).bind(onNext: { [unowned self] (_) in
            if self.btnBackAction != nil {
                self.btnBackAction!()
            }
            else if (self.parentVC?.navigationController?.popViewController(animated: true)) == nil {
                self.parentVC?.dismiss(animated: true, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        return button
    }()
//
//    lazy var btnClose: UIButton = {
//        let button = UIButton(type: .custom)
//        button.isHidden = true
//        button.snp.makeConstraints({ (maker) in
//            maker.width.equalTo(button.snp.height)
//        })
//
//        button.rx.controlEvent(.touchUpInside).bind(onNext: { [unowned self] (_) in
//            if self.btnBackAction != nil {
//                self.btnBackAction!()
//            }
//            else if (self.parentVC?.navigationController?.popViewController(animated: true)) == nil {
//                self.parentVC?.dismiss(animated: true, completion: nil)
//            }
//        }).disposed(by: disposeBag)
//
//        let imgView = UIImageView(image: UIImage.initSDK("ic_cancel_trans")?
//                                    .withRenderingMode(.alwaysTemplate))
//        imgView.tintColor = AppTheme.Color.navTint
//        button.addSubview(imgView)
//        imgView.snp.makeConstraints({ (maker) in
//            maker.width.equalTo(imgView.snp.height)
//            maker.center.equalToSuperview()
//            maker.width.equalTo(button.snp.width).multipliedBy(0.5)
//        })
//
//        return button
//    }()
    
    var btnBackAction:(() -> Void)?
    
    lazy var rhStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        
        return stack
    }()
    
    lazy var titleView: UIView = {
        let view = UIView()
        
        return view
    }()
    
    lazy var lblTitle : UILabel = {
        let label = UILabel()
        label.font = AppTheme.Font.normalBold
        label.textColor = AppTheme.Color.navTint
        label.textAlignment = .center
        label.isHidden = true
        
        return label
    }()
    
    lazy var lblTitleLarge : UILabel = {
        let label = UILabel()
        label.font = AppTheme.Font.large24Bold
        label.textColor = AppTheme.Color.navTint
        label.textAlignment = .center
        titleView.addSubview(label)
        label.snp.makeConstraints({ (maker) in
            maker.top.equalTo(12)
            maker.left.equalTo(16)
            maker.bottom.equalTo(-12)
            maker.right.equalTo(-16)
        })
        
        return label
    }()
    
    lazy var avatarView: UIView = {
        let view = UIView()
        //      view.isHidden = true
        view.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints { maker in
            maker.width.height.equalTo(40)
            maker.leading.equalToSuperview()
            maker.bottom.equalToSuperview().offset(-8)
        }
        avatarImageView.layer.cornerRadius = 20
        
        view.addSubview(badgeLabel)
        badgeLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview()
            maker.leading.equalTo(avatarImageView.snp.trailing).offset(-5)
            maker.width.height.equalTo(22)
        }
        return view
    }()
    
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.clipsToBounds = true
        
        imageView.layer.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
        imageView.layer.borderWidth = 2
        
        return imageView
    }()
    
    lazy var badgeLabel: UILabel = {
        let label = UILabel()
        label.font = AppTheme.Font.smallBold
        label.textColor = .white
        label.backgroundColor = AppTheme.Color.red
//        label.sdkIsCircle = true
        label.textAlignment = .center
        label.isHidden = true

//        label.rx.observe(CGRect.self, #keyPath(UIView.bounds)).subscribe(onNext: { (rect) in
//            label.sdkIsCircle = true
//        }).disposed(by: disposeBag)
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    func setupView() {
        self.addSubview(vStack)
        vStack.addArrangedSubview(btnView)
        btnView.addSubViews(lhStack, rhStack, lblTitle)
        lhStack.addArrangedSubview(btnBack)
//        rhStack.addArrangedSubview(btnClose)
        
        vStack.addArrangedSubview(titleView)
        
        
        vStack.snp.makeConstraints({ (maker) in
            var topPadding: CGFloat = 26
            if #available(iOS 11.0, *) {
                topPadding = CGFloat.maximum(UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0, topPadding)
            } else {
                topPadding = CGFloat.maximum(parentVC?.topLayoutGuide.length ?? 0, topPadding)
            }
            maker.top.equalTo(topPadding)
            maker.left.bottom.right.equalToSuperview()
        })
        
        lhStack.snp.makeConstraints({ (maker) in
            maker.top.bottom.equalToSuperview()
            maker.left.equalTo(8)
        })
        
        rhStack.snp.makeConstraints({ (maker) in
            maker.top.bottom.equalToSuperview()
            maker.right.equalTo(-8)
        })
        
        lblTitle.snp.makeConstraints({ (maker) in
            maker.centerX.centerY.equalToSuperview()
            maker.left.greaterThanOrEqualTo(lhStack.snp.right)
            maker.right.greaterThanOrEqualTo(rhStack.snp.left)
        })
        
        
    }
    
    func hiddenBackButton(isHidden: Bool) {
        hiddenButton(button: btnBack, isHidden: isHidden)
    }
    
    func hiddenCloseButton(isHidden: Bool) {
//        hiddenButton(button: btnClose, isHidden: isHidden)
    }
    
    func hiddenButton(button: UIButton, isHidden: Bool) {
        button.isHidden = isHidden
        hiddenViewButton()
    }
    
    func hiddenViewButton() {
//        btnView.isHidden = btnBack.isHidden && btnClose.isHidden
    }
}
