//
//  AlertVC.swift
//  SpiderSDK
//
//  Created by NghiaVD on 2/18/22.
//

import UIKit

class AlertVC: UIViewController {
    static func newFromXib<T: UIViewController>() -> T {
        return T(nibName: T.className(), bundle: Bundle(for: T.self))
    }
    
    var message: String?
    var messageAttributed: NSMutableAttributedString?
    
    var titleOk: String?
    var titleCancel: String?
    
    var isOkHidden = true
    
    var onOk: (() -> Void)?
    var onCancel: (() -> Void)?

    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppTheme.Font.large18Medium
        }
    }
    @IBOutlet weak var lblMessage: UILabel! {
        didSet {
            lblMessage.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var btnOk: UIButtonOneClick! {
        didSet {
            btnOk.titleLabel?.font = AppTheme.Font.normalSemiBold
            btnOk.delegate = self
        }
    }
    @IBOutlet weak var btnCancel: UIButtonOneClick! {
        didSet {
            btnCancel.titleLabel?.font = AppTheme.Font.normalSemiBold
            btnCancel.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = title, title.isNotEmpty {
            lblTitle.isHidden = false
            lblTitle.xibLocKey = title
        }
        
        if let message = message, message.isNotEmpty {
            lblMessage.isHidden = false
            lblMessage.xibLocKey = message
        }
        
        if let messageAttributed = messageAttributed {
            lblMessage.isHidden = false
            lblMessage.attributedText = messageAttributed
        }
        
        if let titleOk = titleOk, titleOk.isNotEmpty {
            btnOk.xibLocKey = titleOk
        }
        else {
            btnOk.xibLocKey = "Đồng ý"
        }
        
        if let titleCancel = titleCancel, titleCancel.isNotEmpty {
            btnCancel.xibLocKey = titleCancel
        }
        else {
            btnCancel.xibLocKey = isOkHidden ? "Đóng" : "Bỏ qua"
        }
        
        btnOk.isHidden = isOkHidden
    }

    class func show(title: String? = "Thông báo", message: String? = nil, messageAttributed: NSMutableAttributedString? = nil, titleOk: String? = nil, titleCancel: String? = nil, isOkHidden: Bool = true, onOk: (() -> Void)? = nil, onCancel: (() -> Void)? = nil) {
        if let message = message, message.isNotEmpty, let topVC = UIApplication.topViewController() {
            let alert: AlertVC = AlertVC.newFromXib()
            alert.title = title
            alert.message = message
            alert.messageAttributed = messageAttributed
            
            if let titleOk = titleOk {
                alert.titleOk = titleOk
            }
            if let titleCancel = titleCancel {
                alert.titleCancel = titleCancel
            }
            
            alert.isOkHidden = isOkHidden
            alert.onOk = onOk
            alert.onCancel = onCancel
            
            alert.modalPresentationStyle = .custom
            topVC.present(alert, animated: false)
        }
    }
    
}

extension AlertVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        popNavigationOrDismiss(false) {
            switch button {
            case self.btnOk:
                self.onOk?()
                
            case self.btnCancel:
                self.onCancel?()
                
            default:
                break
            }
        }
    }
}

