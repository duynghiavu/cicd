//
//  BaseVC.swift
//  AppToAppDemo
//
//  Created by Nghia Vu on 6/25/19.
//  Copyright © 2019 Nghia Vu. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD

class BaseVC: UIViewController {
    let disposeBag = DisposeBag()
    
    class func newInstance<T: UIViewController>(storyboardName: String = "Main") -> T {
        let name = NSStringFromClass(T.self).components(separatedBy: ".").last
        let vc = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: name!) as! T
        return vc
    }
    
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppTheme.Font.large22Medium
        }
    }
    
    func hideKeyboardWhenTappedAround() {
           let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       }
       
       @objc func dismissKeyboard() {
           view.endEditing(true)
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        OperationQueue.main.addOperation { [weak self] in
            guard let `self` = self else { return }
            self.view.reloadxibLocKey()
            self.view.reloadxibIsCircle()
        }
    }
    
    func showLoading(view: UIView? = nil, animated: Bool = true) {
        let v = (view ?? self.view)!
        let hud:MBProgressHUD = MBProgressHUD.showAdded(to: v, animated: animated)
        hud.bezelView.color = AppTheme.Color.lightGray
        hud.bezelView.style = .solidColor
    }
    
    func hideLoading(view: UIView? = nil, animated: Bool = true) {
        let v = (view ?? self.view)!
        MBProgressHUD.hide(for: v, animated: animated)
    }

}
