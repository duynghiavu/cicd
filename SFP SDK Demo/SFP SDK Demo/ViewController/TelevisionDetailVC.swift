//
//  TelevisionDetailVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/28/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class TelevisionDetailVC: BaseVC {
    var item: FilmModel?
    
    @IBOutlet weak var imgPoster: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCreateAtTitle: UILabel!
    @IBOutlet weak var lblCreateAt: UILabel!
    @IBOutlet weak var lblAuthorTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblDurationTitle: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    @IBOutlet weak var btnRate: UIButtonOneClick!
    @IBOutlet weak var btnPlaylist: UIButtonOneClick!
    @IBOutlet weak var btnPlay: UIButtonOneClick!
    @IBOutlet weak var btnBack: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [lblName, btnRate.titleLabel, btnPlaylist.titleLabel, btnPlay.titleLabel, lblAmount, lblCreateAtTitle, lblCreateAt, lblAuthorTitle, lblAuthor, lblDurationTitle, lblDuration].forEach { (item) in
            item?.font = AppTheme.Font.normal
        }
        
        [btnRate, btnPlaylist, btnPlay, btnBack].forEach { (item) in
            item?.delegate = self
        }
        
        if let item = item {
            imgPoster.image = UIImage(named: item.image)
            lblName.text = item.name
            btnRate.setTitle(item.rate, for: .normal)
            lblAmount.text = item.amount
            lblCreateAt.text = item.createAt
            lblAuthor.text = item.author
            lblDuration.text = item.duration
        }
    }
    
    
}
extension TelevisionDetailVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnRate:
            print("btnRate")
            
        case btnPlaylist:
            print("btnPlaylist")
            
        case btnPlay:
            let vc: PayMentMethodsVC = PayMentMethodsVC.newInstance(storyboardName: "Film")
            show(vc, sender: nil)
            
        default:
            break
        }
    }
}
