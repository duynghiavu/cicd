//
//  MerchantVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/7/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//
import UIKit
import MBProgressHUD
import SpiderSDK

class MerchantVC: BaseVC{

    @IBOutlet weak var tblContent: UITableView!{
        didSet {
            tblContent.register(HomeCell.self)
            tblContent.dataSource = self
            tblContent.estimatedRowHeight = 44
            tblContent.rowHeight = UITableView.automaticDimension
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContent.reloadDataAndView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
extension MerchantVC{
    func setupUI(){
        tblContent.dataSource = self
        tblContent.delegate = self
        tblContent.register(HomeCell.nib, forCellReuseIdentifier: HomeCell.identifier)
    
    }

}



extension MerchantVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeCell = tableView.dequeueCell()

        
        return cell
    }
}
