//
//  InforDetailProductImageVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/15/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK

class InforDetailProductImageVC: BaseVC {
    var vm: ShoppingVM?
    var currentProduct: ProductModel?
    
    var cellSize: CGSize = .zero
    @IBOutlet weak var cvContent: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 16
            layout.minimumInteritemSpacing = 16
            layout.sectionInset = .init(top: 16, left: 16, bottom: 16, right: 16)
            cvContent.collectionViewLayout = layout
            
            cvContent.delegate = self
            cvContent.dataSource = self
            cvContent.register(InforDetailProductImageCell.self, forCellWithReuseIdentifier: "InforDetailProductImageCell")
        }
    }
    
    var cellOtherSize: CGSize = .zero
    @IBOutlet weak var cvOther: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 16
            layout.minimumInteritemSpacing = 16
            layout.sectionInset = .init(top: 16, left: 0, bottom: 16, right: 0)
            cvOther.collectionViewLayout = layout
            
            cvOther.delegate = self
            cvOther.dataSource = self
            cvOther.register(ProductHomeCell.self)
        }
    }
    
    @IBOutlet weak var lblPage: UILabel! {
        didSet {
            lblPage.font = AppTheme.Font.normal
        }
    }
    
    //MARK: -
    @IBOutlet weak var lbTitleProduct: UILabel! {
        didSet {
            lbTitleProduct.font = AppTheme.Font.large24Bold
        }
    }
    
    @IBOutlet weak var lbPriceProduct: UILabel! {
        didSet {
            lbPriceProduct.font = AppTheme.Font.largeMedium
        }
    }
    @IBOutlet weak var lbOldPrice: UILabel! {
        didSet {
            lbOldPrice.font = AppTheme.Font.normalMedium
        }
    }
    
    @IBOutlet weak var lblRate: UILabel! {
        didSet {
            lblRate.font = AppTheme.Font.normal
        }
    }
    @IBOutlet weak var lblViews: UILabel! {
        didSet {
            lblViews.font = AppTheme.Font.normal
        }
    }
    @IBOutlet weak var lblBuys: UILabel! {
        didSet {
            lblBuys.font = AppTheme.Font.smallMedium
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblShopName: UILabel! {
        didSet {
            lblShopName.font = AppTheme.Font.normalSemiBold
        }
    }
    @IBOutlet weak var lblShopStore: UILabel! {
        didSet {
            lblShopStore.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblDescTitle: UILabel! {
        didSet {
            lblDescTitle.font = AppTheme.Font.largeBold
        }
    }
    @IBOutlet weak var lblDesc: UILabel! {
        didSet {
            lblDesc.font = AppTheme.Font.normal
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblReview: UILabel! {
        didSet {
            lblReview.font = AppTheme.Font.largeBold
        }
    }
    @IBOutlet weak var lblReviewRate: UILabel! {
        didSet {
            lblReviewRate.font = AppTheme.Font.largeMedium
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblReviewName1: UILabel! {
        didSet {
            lblReviewName1.font = AppTheme.Font.normalSemiBold
        }
    }
    @IBOutlet weak var lblReviewDate1: UILabel! {
        didSet {
            lblReviewDate1.font = AppTheme.Font.small
        }
    }
    @IBOutlet weak var lblReviewDesc1: UILabel! {
        didSet {
            lblReviewDesc1.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblReviewName2: UILabel! {
        didSet {
            lblReviewName2.font = AppTheme.Font.normalSemiBold
        }
    }
    @IBOutlet weak var lblReviewDate2: UILabel! {
        didSet {
            lblReviewDate2.font = AppTheme.Font.small
        }
    }
    @IBOutlet weak var lblReviewDesc2: UILabel! {
        didSet {
            lblReviewDesc2.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblReviewName3: UILabel! {
        didSet {
            lblReviewName3.font = AppTheme.Font.normalSemiBold
        }
    }
    @IBOutlet weak var lblReviewDate3: UILabel! {
        didSet {
            lblReviewDate3.font = AppTheme.Font.small
        }
    }
    @IBOutlet weak var lblReviewDesc3: UILabel! {
        didSet {
            lblReviewDesc3.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet weak var lblMore: UILabel! {
        didSet {
            lblMore.font = AppTheme.Font.normalMedium
        }
    }
    
    @IBOutlet weak var lblOther: UILabel! {
        didSet {
            lblOther.font = AppTheme.Font.largeBold
        }
    }
    
    //MARK: -
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnShoppingCard: UIButtonOneClick!
    @IBOutlet weak var vShoppingCardRed: UIView!
    @IBOutlet weak var btnMessage: UIButtonOneClick!
    @IBOutlet weak var btnAddToShop: UIButtonOneClick!
    @IBOutlet weak var btnBuyNow: UIButtonOneClick! {
        didSet {
            btnBuyNow.titleLabel?.font = AppTheme.Font.normalSemiBold
        }
    }
    
    //MARK: -
    @IBOutlet weak var vBuyNow: UIView!
    @IBOutlet weak var btnBuyClose: UIButtonOneClick!
    
    @IBOutlet weak var imgBuyIcon: UIImageView!
    
    @IBOutlet weak var lblBuyTitle: UILabel! {
        didSet {
            lblBuyTitle.font = AppTheme.Font.large18Bold
        }
    }
    @IBOutlet weak var lblBuyAmount: UILabel! {
        didSet {
            lblBuyAmount.font = AppTheme.Font.largeMedium
        }
    }
    @IBOutlet weak var lblBuyAmountOld: UILabel! {
        didSet {
            lblBuyAmountOld.font = AppTheme.Font.normalMedium
        }
    }
    
    
    @IBOutlet weak var lblBuyCountTitle: UILabel! {
        didSet {
            lblBuyCountTitle.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet weak var lblBuyCount: UILabel! {
        didSet {
            lblBuyCount.font = AppTheme.Font.large18Medium
        }
    }
    @IBOutlet weak var lblBuyReduction: UILabel! {
        didSet {
            lblBuyReduction.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet weak var lblBuyIncrease: UILabel! {
        didSet {
            lblBuyIncrease.font = AppTheme.Font.normalMedium
        }
    }
    
    @IBOutlet weak var btnBuyReduction: UIButtonOneClick!
    @IBOutlet weak var btnBuyPluss: UIButtonOneClick!
    @IBOutlet weak var btnBuyContinue: UIButtonOneClick! {
        didSet {
            btnBuyContinue.titleLabel?.font = AppTheme.Font.normalSemiBold
        }
    }
    
    
    override func loadView() {
        super.loadView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        
        if let item = currentProduct {
            lblPage.text = "\(item.images.count > 0 ? 1 : 0)/\(item.images.count) Foto"
            cvContent.reloadData()
            
            lbTitleProduct.text = item.name
            lbPriceProduct.text = item.totalAmountDisPlay()
            
            let attributedString = NSMutableAttributedString(string: item.desc)
            // *** Create instance of `NSMutableParagraphStyle`
            let paragraphStyle = NSMutableParagraphStyle()
            // *** set LineSpacing property in points ***
            paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
            // *** Apply attribute to string ***
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
            // *** Set Attributed String to your label ***
            lblDesc.attributedText = attributedString
            
            imgBuyIcon.image = UIImage(named: item.images.first ?? "")
            
            lblBuyTitle.text = item.name
            lblBuyAmount.text = item.amountDisPlay()
            lblBuyAmountOld.attributedText = item.amountOldDisPlay().strikeThrough(font: lblBuyAmountOld.font, color: lblBuyAmountOld.textColor)
            lbOldPrice.isHidden = item.amountOld <= 0
            lbOldPrice.attributedText = item.amountOldDisPlay().strikeThrough(font: lbOldPrice.font, color: lbOldPrice.textColor)
        }
    }
    private func setUpUI(){
        [btnBack, btnShoppingCard, btnMessage, btnAddToShop, btnBuyNow,
         btnBuyClose, btnBuyReduction, btnBuyPluss, btnBuyContinue].forEach { (item) in
            item?.delegate = self
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        vShoppingCardRed.isHidden = vm?.shopItems.isEmpty ?? true
    }

}

extension InforDetailProductImageVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case cvContent:
            return currentProduct?.images.count ?? 0
            
        case cvOther:
            return vm?.items.count ?? 0
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case cvContent:
            let productcell = collectionView.dequeueReusableCell(withReuseIdentifier: "InforDetailProductImageCell", for: indexPath) as! InforDetailProductImageCell
            
            if let images = currentProduct?.images,
               indexPath.item < images.count {
                productcell.imgIcon.image = UIImage(named: images[indexPath.row])
            }
            return productcell
            
        case cvOther:
            let cell: ProductHomeCell = collectionView.dequeueCell(indexPath: indexPath)
            if let vm = vm, indexPath.row < vm.searchItems.count {
                cell.set(vm.searchItems[indexPath.row])
            }
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

extension InforDetailProductImageVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vm = vm, indexPath.row < vm.searchItems.count {
            let vc: InforDetailProductImageVC = InforDetailProductImageVC.newInstance()
            vc.vm = vm
            vc.currentProduct = vm.searchItems[indexPath.row]
            show(vc, sender: nil)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == cvContent {
            var page = Int(scrollView.contentOffset.x / scrollView.bounds.width) + 1
            if Int(scrollView.contentOffset.x) % Int(scrollView.bounds.width) > Int(scrollView.bounds.width / 2) {
                page += 1
            }
            lblPage.text = "\(page)/\(currentProduct?.images.count ?? 0)"
        }
    }
}

extension InforDetailProductImageVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let width = SCREEN_WIDTH - layout.sectionInset.left - layout.sectionInset.right - 32
        switch collectionView {
        case cvContent:
            if cellSize == .zero {
                let numberOfRow = CGFloat(1)
                let size = (width - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
                cellSize = CGSize(width: size, height: size / 1.2)
            }
            return cellSize
            
        case cvOther:
            if cellOtherSize == .zero {
                let numberOfRow = CGFloat(2)
                let size = (width - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
                cellOtherSize = CGSize(width: size, height: size * 22 / 15)
            }
            return cellOtherSize
            
        default:
            return .zero
        }
    }
}

extension InforDetailProductImageVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnShoppingCard:
            let vc: ShoppingCardVC = ShoppingCardVC.newInstance()
            vc.vm = vm
            show(vc, sender: nil)
            
        case btnAddToShop:
            addToShop()
            addToShopAction()
            
        case btnBuyNow:
            vBuyNow.isHidden = false
            
        case btnBuyClose:
            vBuyNow.isHidden = true
            
        case btnBuyPluss:
            currentProduct!.addCount(count: 1)
            lblBuyCount.text = "\(currentProduct!.count)"
            
        case btnBuyReduction:
            currentProduct!.addCount(count: -1)
            lblBuyCount.text = "\(currentProduct!.count)"
            
        case btnBuyContinue:
            addToShop(true)
//            let vc: ShoppingCardVC = ShoppingCardVC.newInstance()
//            vc.vm = vm
//            show(vc, sender: nil)
            let vc: PayMentVC = PayMentVC.newInstance()
            vc.isNow = true
            vc.vm = vm
            show(vc, sender: nil)
            
        default:
            break
        }
    }
    
    func addToShop(_ isBuy: Bool = false) {
        guard let vm = vm, let currentProduct = currentProduct else {
            return
        }
        
        vm.shopItems.forEach { item in
            item.isChecked = false
        }
        if vm.shopItems.filter({ $0.id == currentProduct.id }).isEmpty {
            vm.shopItems.append(currentProduct)
            vm.saveShopItems()
        }
        else {
            currentProduct.addCount(count: 1)
        }
        currentProduct.isChecked = isBuy
        vShoppingCardRed.isHidden = vm.shopItems.isEmpty
    }
    
    func addToShopAction() {
        let imageView = UIImageView()
        if let icon = currentProduct?.images.first {
            imageView.image = UIImage(named: icon)
        }
        imageView.frame = view.convert(btnAddToShop.frame, from: btnAddToShop.superview)
        view.addSubview(imageView)
        
        UIView.animate(withDuration: 1) {
            imageView.frame = self.view.convert(self.btnShoppingCard.frame, from: self.btnShoppingCard.superview)
            self.view.addSubview(imageView)
        } completion: { _ in
            imageView.removeFromSuperview()
        }

    }
}
