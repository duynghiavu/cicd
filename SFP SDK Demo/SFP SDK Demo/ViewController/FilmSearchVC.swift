//
//  FilmSearchVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 2/26/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class FilmSearchVC: BaseEditVC {
    var vm: FilmVM?
    var timer: Timer?
    
    @IBOutlet weak var tfSearch: UITextField! {
        didSet {
            tfSearch.setClearButton(with: UIImage(named: "ic_close")!)
        }
    }
    
    @IBOutlet weak var vOption: UIView!
    @IBOutlet weak var lblNear: UILabel!
    @IBOutlet weak var lblQuick: UILabel!
    
    @IBOutlet weak var tblResult: UITableView!
    @IBOutlet weak var tblNear: UITableView!
    @IBOutlet weak var tblQuick: UITableView!
    
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnSearch: UIButtonOneClick!
    @IBOutlet weak var btnClear: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [lblNear, lblQuick].forEach { (item) in
            item?.font = AppTheme.Font.normalBold
        }
        
        [tblResult, tblNear, tblQuick].forEach { (item) in
            item?.delegate = self
            item?.dataSource = self
        }
        
        [btnBack, btnSearch, btnClear].forEach { (item) in
            item?.delegate = self
        }
        
        if let vm = vm {
            vm.reloadSearchFilms.next({ [weak self] (value) in
                guard let `self` = self else { return }
                if value {
                    self.tblResult.reloadData()
                }
            }) ~> disposeBag
            
            vm.reloadSearchNear.next { [weak self] (value) in
                guard let `self` = self else { return }
                if value {
                    self.tblNear.reloadData()
                }
            } ~> disposeBag
            
            vm.reloadSearchQuick.next { [weak self] (value) in
                guard let `self` = self else { return }
                if value {
                    self.tblQuick.reloadData()
                }
            } ~> disposeBag
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func tfSearchChange(_ sender: Any) {
        vm?.searchText = tfSearch.text?.trim() ?? ""
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.checkSearch), userInfo: nil, repeats: false)
    }
    
    @objc func checkSearch() {
        vm?.addSearchNear()
    }
}

extension FilmSearchVC: UIButtonOneClickAction{
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnSearch:
            vm?.addSearchNear()
            
        case btnClear:
            vm?.clearSearchNear()
                    
        default:
            break
        }
    }
}

extension FilmSearchVC:  UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let vm = vm {
            switch tableView {
            case tblNear:
                return vm.searchNear.count
                
            case tblQuick:
                return vm.searchQuick.count
                
            case tblResult:
                DispatchQueue.main.async {
                    self.vOption.isHidden = vm.searchText.count != 0
                    self.tblResult.isHidden = !self.vOption.isHidden
                }
                return vm.searchFilms.count
                
            default:
                break
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell?.backgroundColor = .clear
            cell?.contentView.backgroundColor = .clear
            cell?.textLabel?.font = AppTheme.Font.normal
            cell?.textLabel?.textColor = .white
        }
        if let vm = vm {
            switch tableView {
            case tblNear:
                if indexPath.row < vm.searchNear.count {
                    cell?.textLabel?.text = vm.searchNear[indexPath.row]
                }
                
            case tblQuick:
                if indexPath.row < vm.searchQuick.count {
                    cell?.textLabel?.text = vm.searchQuick[indexPath.row]
                }
                
            case tblResult:
                if indexPath.row < vm.searchFilms.count {
                    cell?.textLabel?.text = vm.searchFilms[indexPath.row].name
                }
                
            default:
                break
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vm = vm {
            switch tableView {
            case tblNear:
                if indexPath.row < vm.searchNear.count {
                    tfSearch.text = vm.searchNear[indexPath.row]
                    vm.searchText = tfSearch.text!
                    view.endEditing(true)
                }
                
            case tblQuick:
                if indexPath.row < vm.searchQuick.count {
                    tfSearch.text = vm.searchQuick[indexPath.row]
                    vm.searchText = tfSearch.text!
                    view.endEditing(true)
                }
                
            case tblResult:
                if indexPath.row < vm.searchFilms.count {
                    let vc: TelevisionDetailVC = TelevisionDetailVC.newInstance(storyboardName: "Film")
                    vc.item = vm.searchFilms[indexPath.row]
                    self.show(vc, sender: nil)
                }
                
            default:
                break
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
