//
//  TestAlertController.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/19/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//
import UIKit
import SpiderSDK


class TestAlertController: BaseVC {

    @IBOutlet weak var btnClick: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClick.addTarget(self, action: #selector(self.submitChange(_:)), for: .touchUpInside)
    }
}
extension TestAlertController{
    
        @objc func submitChange(_ sender: UIButton) {
            AlertVC.show(message: "Click")
        }
    
}
