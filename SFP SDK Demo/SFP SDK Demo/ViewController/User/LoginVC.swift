//
//  LoginVC.swift
//  AppToAppDemo
//
//  Created by Nghia Vu on 6/25/19.
//  Copyright © 2019 Nghia Vu. All rights reserved.
//

import UIKit
import CryptoSwift
import CommonCrypto
import MBProgressHUD
import SpiderSDK

class LoginVC: BaseEditVC {

    @IBOutlet var tfUsername: UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    @IBOutlet weak var imgEye: UIImageView!
    
    @IBOutlet weak var btnShowPass: UIButtonOneClick!
    @IBOutlet weak var btnLogin: UIButtonOneClick!
    @IBOutlet weak var btnForgotPass: UIButtonOneClick!
    @IBOutlet weak var btnRegister: UIButtonOneClick!
    @IBOutlet weak var btnFacebook: UIButtonOneClick!
    @IBOutlet weak var btnGoogle: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(AppTheme.Font.normal)
        print(AppTheme.Font.normalBold)
        
        [tfUsername, tfPassword].forEach { v in
            v?.font = AppTheme.Font.normalBold
            v?.xibPlaceholderFont = AppTheme.Font.normal
            v?.tintColor = tfUsername.textColor
            v?.keyboardAppearance = .light
        }
        
        tfUsername.text = UserDefaultsHelper.share().getString(key: .username)
        #if DEBUG
        tfPassword.text = UserDefaultsHelper.share().getString(key: .password)
        #endif
        
        [btnShowPass, btnLogin, btnForgotPass, btnRegister, btnFacebook, btnGoogle].forEach({ v in
            v?.titleLabel?.font = AppTheme.Font.normalSemiBold
            v?.delegate = self
        })
    }
}

extension LoginVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnShowPass:
            tfPassword.isSecureTextEntry = imgEye.isHighlighted
            imgEye.isHighlighted = !imgEye.isHighlighted
            
        case btnLogin:
            guard let username = tfUsername.text?.trimmingCharacters(in: .whitespacesAndNewlines), username.count != 0,
                  let password = tfPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines), password.count != 0 else {
                AlertVC.show(message: "Vui lòng nhập đầy đủ thông tin!")
                return
            }
            guard username.count == 10 else {
                AlertVC.show(message: "Số điện thoại không đúng cú pháp!")
                return
            }
            let first = username.subString(from: 0, to: 1)
            if first != "0" {
                AlertVC.show(message: "Số điện thoại có đầu số chưa được hỗ trợ!")
                return
            }

            guard password.count >= 6 else {
                AlertVC.show(message: "Quý khách vui lòng nhập mật khẩu tối thiểu 6 ký tự!")
                return
            }
            
            showLoading()

            let req = LoginRequest()
            req.username = username
            req.password = password

            ServiceAPI.login(model: req) { (success, res) in
                if success {
                    let helper = UserDefaultsHelper.share()
                    helper.set(value: res.customerName, key: .customerName)
                    helper.set(value: username, key: .username)
                    helper.set(value: password, key: .password)
                    helper.set(value: res.merchantCode, key: .merchantCode)
                    SpiderSDK.setUsername(username)

                    let vc: HomeTabVC = HomeTabVC.newInstance()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                self.hideLoading()
            }
            
        case btnRegister:
            let vc: RegisterVC = RegisterVC.newInstance()
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break
        }
    }
}
