//
//  RegisterVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/2/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import CryptoSwift
import CommonCrypto
import MBProgressHUD
import SpiderSDK

class RegisterVC: BaseEditVC {

    @IBOutlet var tfName: UITextField!
    @IBOutlet var tfUsername: UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    @IBOutlet weak var imgEye: UIImageView!
    
    @IBOutlet weak var btnShowPass: UIButtonOneClick!
    @IBOutlet weak var btnLogin: UIButtonOneClick!
    @IBOutlet weak var btnRegister: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [tfUsername, tfName, tfPassword].forEach { v in
            v?.font = AppTheme.Font.normalBold
            v?.xibPlaceholderFont = AppTheme.Font.normal
            v?.tintColor = tfUsername.textColor
            v?.keyboardAppearance = .light
        }
        
        tfName.maxLength = 100
        
        [btnShowPass, btnLogin, btnRegister].forEach({ v in
            v?.titleLabel?.font = AppTheme.Font.normalSemiBold
            v?.delegate = self
        })
    }
}

extension RegisterVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnShowPass:
            tfPassword.isSecureTextEntry = imgEye.isHighlighted
            imgEye.isHighlighted = !imgEye.isHighlighted
            
        case btnLogin:
            onBackPressed()
            
        case btnRegister:
            guard let name = tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines), name.isNotEmpty else {
                AlertVC.show(message: "Quý khách vui lòng nhập họ và tên!")
                return
            }
            
            if !checkName(name: name) {
                return
            }
            
            guard let username = tfUsername.text?.trimmingCharacters(in: .whitespacesAndNewlines), username.count == 10 else {
                AlertVC.show(message: "Số điện thoại không đúng cú pháp!")
                return
            }
            let first = username.subString(from: 0, to: 1)
            if first != "0" {
                AlertVC.show(message: "Số điện thoại có đầu số chưa được hỗ trợ!")
                return
            }
            
            guard let password = tfPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines), password.count >= 6 else {
                AlertVC.show(message: "Quý khách vui lòng nhập mật khẩu tối thiểu 6 ký tự!")
                return
            }
            
            showLoading()
            
            let req = RegisterRequest()
            req.customerName = name
            req.customerPhone = username
            req.password = password
            
            ServiceAPI.register(model: req) { (success, res) in
                if success {
                    let helper = UserDefaultsHelper.share()
                    helper.set(value: name, key: .customerName)
                    helper.set(value: username, key: .username)
                    helper.set(value: password, key: .password)
                    helper.set(value: res.merchantCode, key: .merchantCode)
                    SpiderSDK.setUsername(username)
                    
                    let vc: HomeTabVC = HomeTabVC.newInstance()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                self.hideLoading()
            }
            
        default:
            break
        }
    }
    
    func checkName(name: String) -> Bool {
        let name = name.lowercased()
        
        var strs = ["<html>", "</html>"]
        for str in strs {
            if name.contains(find: str) {
                AlertVC.show(message: "Họ vả tên không được chứa ký tự đặc biệt!")
                return false
            }
        }
        
        strs = ["<", ">"]
        for str in strs {
            if name.contains(find: str) {
                AlertVC.show(message: "Họ và tên không hợp lệ vui lòng sử dụng họ tên khác!")
                return false
            }
        }
        
        if Validation.isContainSql(string: name) {
            AlertVC.show(message: "Họ và tên không hợp lệ vui lòng sử dụng họ tên khác!")
            return false
        }
        
        return true
    }
}

