//
//  TestCollectionVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/12/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK


class TestCollectionVC: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var clvContent: UICollectionView!{
        didSet {
            clvContent.register(MyCollectionViewCelll.self)
            clvContent.dataSource = self
            
            
        }
    }
    override func loadView() {
        super.loadView()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = true
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
//        self.collectionView = clvContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    let reuseIdentifier = "cell"
    var items = ["1", "2", "3", "4", "5", "6", "7", "8"]

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCelll

        let index = indexPath.item
        switch index{
        case 0:
            cell.lblTitle.text = "ahihi nyc"
           case 1:

            cell.lblTitle.text = "ahihi nyc"
           default:
            cell.lblTitle.text = "ahihi nyc la do con cho"
           }
        
//        cell.lblContent.text = self.items[indexPath.row]
//        //        cell.backgroundColor = UIColor.cyan
//
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.red
    }

    // change background color back when user releases touch
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.cyan
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")

    }
    
}
