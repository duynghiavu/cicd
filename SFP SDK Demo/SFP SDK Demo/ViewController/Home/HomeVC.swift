//
//  HomeVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK


class HomeVC: BaseVC {
    
    var timer: Timer?
    
    @IBOutlet weak var svContent: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var vShopping: UIView!
    @IBOutlet weak var vTelevision: UIView!
    @IBOutlet weak var vTaxi: UIView!
    
    @IBOutlet weak var btnShopping: UIButtonOneClick!
    @IBOutlet weak var btnTelevision: UIButtonOneClick!
    @IBOutlet weak var btnTaxi: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [btnShopping, btnTelevision, btnTaxi].forEach { v in
            v?.delegate = self
        }
        
        startScrolling()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.vShopping.addGradientBackground(colors: [#colorLiteral(red: 0.1176470588, green: 0.6470588235, blue: 0.9882352941, alpha: 1), #colorLiteral(red: 0.0862745098, green: 0.5921568627, blue: 0.9176470588, alpha: 1)])
            self.vTelevision.addGradientBackground(colors: [#colorLiteral(red: 0.1137254902, green: 0.7294117647, blue: 0.7294117647, alpha: 1), #colorLiteral(red: 0, green: 0.7568627451, blue: 0.7568627451, alpha: 1)])
            self.vTaxi.addGradientBackground(colors: [#colorLiteral(red: 0.2901960784, green: 0.8862745098, blue: 0.1921568627, alpha: 1), #colorLiteral(red: 0.1921568627, green: 0.7960784314, blue: 0.09803921569, alpha: 1)])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var size = svContent.contentSize
        size.height = svContent.bounds.height
        svContent.contentSize = size
    }
    
    func stopScrolling() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    func startScrolling() {
        stopScrolling()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.scrolling), userInfo: nil, repeats: true)
    }
    
    @objc func scrolling() {
        var page = pageControl.currentPage + 1
        if page >= pageControl.numberOfPages {
            page = 0
        }
        pageControl.currentPage = page
        svContent.setContentOffset(CGPoint(x: CGFloat(page) * svContent.bounds.width, y: 0), animated: true)
    }
}

extension HomeVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnShopping:
            let vc: LoginVC = LoginVC.newInstance()
            show(vc, sender: nil)
            
        case btnTelevision, btnTaxi:
            let vc: CommingSoonVC = CommingSoonVC.newInstance()
            show(vc, sender: nil)
            
//        case btnTelevision:
//            let vc: TelevisionVC = TelevisionVC.newInstance(storyboardName: "Film")
//            show(vc, sender: nil)
//
//        case btnTaxi:
//            let vc: TaxiVC = TaxiVC.newInstance(storyboardName: "Taxi")
//            show(vc, sender: nil)
            
        default:
            break
        }
    }
}

extension HomeVC: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.width)
    }
}
