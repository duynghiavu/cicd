//
//  HomeShoppingVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/11/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK
import RxSwift

class HomeShoppingVC: BaseVC {
    var vm: ShoppingVM?
    var isShowVerify = false
    var isNeedReload = true
    
    var cellSize: CGSize = .zero
    var walletCell: CategoryWalletCollCell?
    
    @IBOutlet weak var cvContent: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 16
            layout.minimumInteritemSpacing = 16
            layout.sectionInset = .init(top: 16, left: 16, bottom: 0, right: 16)
            cvContent.collectionViewLayout = layout
            
            cvContent.contentInset = .init(top: 0, left: 0, bottom: 16, right: 0)
            
            cvContent.delegate = self
            cvContent.dataSource = self
            cvContent.register(ShoppingBannerCollCell.self)
            cvContent.register(CategoryWalletCollCell.self)
            cvContent.register(CategoryListCollCell.self)
            cvContent.register(CategoryFlashCollCell.self)
            cvContent.register(CategoryFilterCollCell.self)
            cvContent.register(ProductHomeCell.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vm?.reloaItems.next { [weak self] (_) in
            guard let `self` = self else { return }
            self.cvContent.reloadData()
        }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isNeedReload, let vm = vm, !vm.isLoading.value {
            vm.getSpiderInfor()
        }
        isNeedReload = true
    }
}

extension HomeShoppingVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return UICollectionReusableView(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.width, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 5
            
        default:
            return vm?.searchItems.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 0:
                let cell: ShoppingBannerCollCell = collectionView.dequeueCell(indexPath: indexPath)
                cell.startScrolling()
                return cell
                
            case 1:
                if walletCell == nil {
                    walletCell = collectionView.dequeueCell(indexPath: indexPath)
                    vm?.isLoading.next { (value) in
                        guard let cell = self.walletCell else { return }
                        if value {
                            cell.progress.startAnimating()
                        }
                        else {
                            cell.progress.stopAnimating()
                        }
                    }.disposed(by: disposeBag)
                    vm?.spiderData.next { (value) in
                        self.walletCell!.lblName.text = "Ví Shopify Pay"
                        if let fullName = value["fullName"] as? String, fullName.isNotEmpty {
                            self.walletCell!.lblName.text = fullName
                        }
                        
                        self.walletCell!.lblAmount.isHidden = true
                        if let availableBalance = value["availableBalance"] as? String,
                           availableBalance.isNotEmpty, let balance = Int(availableBalance) {
                            self.walletCell!.lblAmount.text = FormatHelper.formatCurrency(balance)
                            self.walletCell!.lblAmount.isHidden = false
                        }
                        
                        if !self.isShowVerify, let ekycStatus = value["ekycStatus"] as? String, ekycStatus == "N" {
                            self.isShowVerify = true
                            let vc: WalletActivateVC = WalletActivateVC.newInstance()
                            vc.presentedVC = self
                            vc.modalPresentationStyle = .custom
                            self.present(vc, animated: true, completion: nil)
                        }
                    }.disposed(by: disposeBag)
                }
                return walletCell!
                
            case 2:
                let cell: CategoryListCollCell = collectionView.dequeueCell(indexPath: indexPath)
                if let vm = vm {
                    cell.onSelected = { category in
                        self.vm!.categorySelected = category
                        let vc: CategoryVC = CategoryVC.newInstance()
                        vc.vm = self.vm
                        self.show(vc, sender: nil)
                    }
                    cell.categories = vm.categories
                }
                return cell
                
            case 3:
                let cell: CategoryFlashCollCell = collectionView.dequeueCell(indexPath: indexPath)
                cell.startCountDown()
                return cell
                
            case 4:
                let cell: CategoryFilterCollCell = collectionView.dequeueCell(indexPath: indexPath)
                return cell
                
            default:
                return UICollectionViewCell()
            }
            
        default:
            let cell: ProductHomeCell = collectionView.dequeueCell(indexPath: indexPath)
            if let vm = vm, indexPath.row < vm.searchItems.count {
                cell.set(vm.searchItems[indexPath.row])
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 1:
                showLoading()
                let helper = UserDefaultsHelper.share()
                let data = helper.getTicketData()
                SpiderSDK.gotoSpiderPay(muid: data.muid, suid: data.suid, fromVC: self) { code, message, data in
                    if code != "0" {
//                        AlertVC.show(message: message)
                    }
                    self.hideLoading()
                }
                
            case 3:
                self.vm!.categorySelected = nil
                let vc: CategoryVC = CategoryVC.newInstance()
                vc.vm = self.vm
                self.show(vc, sender: nil)
                
            default:
                break
            }
            
        default:
            if let vm = vm, indexPath.row < vm.searchItems.count {
                let vc: InforDetailProductImageVC = InforDetailProductImageVC.newInstance()
                vc.vm = vm
                vc.currentProduct = vm.searchItems[indexPath.row]
                show(vc, sender: nil)
            }
        }
    }
}

extension HomeShoppingVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let width = SCREEN_WIDTH - layout.sectionInset.left - layout.sectionInset.right
        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 0:
                return CGSize(width: width, height: width * 188 / 375)
                
            case 1:
                return CGSize(width: width, height: width * 100 / 375)
                
            case 2:
                return CGSize(width: width, height: width * 108 / 375)
                
            case 3:
                return CGSize(width: width, height: width * 60 / 375)
                
            case 4:
                return CGSize(width: width, height: width * 50 / 375)
                
            default:
                return .zero
            }
            
        default:
            if cellSize == .zero {
                let numberOfRow = CGFloat(2)
                let size = (width - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
                cellSize = CGSize(width: size, height: size * 22 / 15)
            }
            return cellSize
        }
    }
}
