//
//  NotifyVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/3/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit
import Foundation

class NotifyVC: BaseVC {
    @IBOutlet var lblEmpty: UILabel! {
        didSet {
            lblEmpty.font = AppTheme.Font.large
        }
    }
}
