//
//  HomeTabVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/3/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK
import RxSwift

class HomeTabVC: BaseVC {
    let vm = ShoppingVM()
    
    var tab1VC: HomeShoppingVC = HomeShoppingVC.newInstance()
    var tab3VC: NotifyVC?
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var vContent: UIView!
    
    @IBOutlet weak var btnMessenger: UIButtonOneClick!
    @IBOutlet weak var btnShoppingCard: UIButtonOneClick!
    @IBOutlet weak var vShoppingCardRed: UIView!
    @IBOutlet weak var btnLogout: UIButtonOneClick!
    
    @IBOutlet weak var lblTabHome: UILabel! {
        didSet {
            lblTabHome.font = AppTheme.Font.tinyMedium
        }
    }
    @IBOutlet weak var lblTabCategory: UILabel! {
        didSet {
            lblTabCategory.font = AppTheme.Font.tinyMedium
        }
    }
    @IBOutlet weak var lblTabNotify: UILabel! {
        didSet {
            lblTabNotify.font = AppTheme.Font.tinyMedium
        }
    }
    @IBOutlet weak var lblTabMe: UILabel! {
        didSet {
            lblTabMe.font = AppTheme.Font.tinyMedium
        }
    }
    
    @IBOutlet weak var imgHomeSelected: UIImageView!
    @IBOutlet weak var imgCategorySelected: UIImageView!
    @IBOutlet weak var imgNotifySelected: UIImageView!
    @IBOutlet weak var imgMeSelected: UIImageView!
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var imgNotify: UIImageView!
    @IBOutlet weak var imgMe: UIImageView!
    
    @IBOutlet weak var btnHome: UIButtonOneClick!
    @IBOutlet weak var btnCategory: UIButtonOneClick!
    @IBOutlet weak var btnNotify: UIButtonOneClick!
    @IBOutlet weak var btnMe: UIButtonOneClick!
    
    override func loadView() {
        super.loadView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [btnMessenger, btnShoppingCard, btnLogout, btnHome, btnCategory, btnNotify, btnMe].forEach { v in
            v?.delegate = self
        }
        
        txtSearch.font = AppTheme.Font.smallBold
        txtSearch.textColor = AppTheme.Color.primaryTextColor
        txtSearch.xibPlaceholderColor = AppTheme.Color.primaryTextColor.withAlphaComponent(0.5)
        txtSearch.xibPlaceholderFont = AppTheme.Font.smallMedium
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
       
        self.hideKeyboardWhenTappedAround()
        
        setTabSelected(btnHome)
        tab1VC.vm = vm
        setTabVC(tab1VC)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        vm.searchText = txtSearch.text ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        vShoppingCardRed.isHidden = vm.shopItems.isEmpty
    }
}

extension HomeTabVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnShoppingCard:
            let vc: ShoppingCardVC = ShoppingCardVC.newInstance()
            vc.vm = vm
            show(vc, sender: nil)
            
        case btnLogout:
            SpiderSDK.clearCached()
            onBackPressed()
            
        case btnHome:
            setTabSelected(button)
            setTabVC(tab1VC)
            
        case btnNotify:
            setTabSelected(button)
            if tab3VC == nil {
                tab3VC = NotifyVC.newInstance()
            }
            setTabVC(tab3VC!)
            
        case btnMe:
            showLoading()
            let helper = UserDefaultsHelper.share()
            let data = helper.getTicketData()
            SpiderSDK.gotoSpiderPay(muid: data.muid, suid: data.suid, fromVC: self) { code, message, data in
                if code != "0" {
//                    AlertVC.show(message: message)
                }
                self.hideLoading()
            }
            
        default:
            let vc: CommingSoonVC = CommingSoonVC.newInstance()
            show(vc, sender: true)
        }
    }
    
    func setTabSelected(_ button: UIButtonOneClick) {
        guard !button.isSelected else { return }
        
        btnHome.isSelected = btnHome == button
        btnCategory.isSelected = btnCategory == button
        btnNotify.isSelected = btnNotify == button
        btnMe.isSelected = btnMe == button
        
        let selectedColor = UIColor(hexString: "#3669C9")
        let titleColor = UIColor(hexString: "#252525")
        let iconColor = UIColor(hexString: "#626365")
        
        lblTabHome.textColor = btnHome.isSelected ? selectedColor : titleColor
        lblTabCategory.textColor = btnCategory.isSelected ? selectedColor : titleColor
        lblTabNotify.textColor = btnNotify.isSelected ? selectedColor : titleColor
        lblTabMe.textColor = btnMe.isSelected ? selectedColor : titleColor
        
        imgHome.tintColor = btnHome.isSelected ? selectedColor : iconColor
        imgCategory.tintColor = btnCategory.isSelected ? selectedColor : iconColor
        imgNotify.tintColor = btnNotify.isSelected ? selectedColor : iconColor
        imgMe.tintColor = btnMe.isSelected ? selectedColor : iconColor
        
        imgHomeSelected.isHidden = !btnHome.isSelected
        imgCategorySelected.isHidden = !btnCategory.isSelected
        imgNotifySelected.isHidden = !btnNotify.isSelected
        imgMeSelected.isHidden = !btnMe.isSelected
    }
    
    func setTabVC(_ vc: BaseVC) {
        vContent.subviews.forEach { v in
            v.removeFromSuperview()
        }
        vc.view.frame = vContent.bounds
        vContent.addSubview(vc.view)
        self.children.forEach { vc in
            vc.removeFromParent()
        }
        self.addChild(vc)
    }
}
