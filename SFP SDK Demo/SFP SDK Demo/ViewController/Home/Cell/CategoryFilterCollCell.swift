//
//  CategoryFilterCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryFilterCollCell: UICollectionViewCell {

    @IBOutlet weak var btnFilter1: UIButtonOneClick! {
        didSet {
            btnFilter1.titleLabel?.font = AppTheme.Font.normal
            btnFilter1.delegate = self
        }
    }
    
    @IBOutlet weak var btnFilter2: UIButtonOneClick! {
        didSet {
            btnFilter2.titleLabel?.font = AppTheme.Font.normal
            btnFilter2.delegate = self
        }
    }
    
    @IBOutlet weak var btnFilter3: UIButtonOneClick! {
        didSet {
            btnFilter3.titleLabel?.font = AppTheme.Font.normal
            btnFilter3.delegate = self
        }
    }
    
}

extension CategoryFilterCollCell: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        if !button.isSelected {
            button.backgroundColor = UIColor(hex: 0x3669C9)
            button.setTitleColor(.white, for: .normal)
            
            if button != btnFilter1 {
                btnFilter1.backgroundColor = .clear
                btnFilter1.setTitleColor(UIColor(hex: 0x58595B), for: .normal)
            }
            if button != btnFilter2 {
                btnFilter2.backgroundColor = .clear
                btnFilter2.setTitleColor(UIColor(hex: 0x58595B), for: .normal)
            }
            if button != btnFilter3 {
                btnFilter3.backgroundColor = .clear
                btnFilter3.setTitleColor(UIColor(hex: 0x58595B), for: .normal)
            }
        }
    }
}
