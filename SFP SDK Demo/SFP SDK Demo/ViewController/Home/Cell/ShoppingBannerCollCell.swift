//
//  ShoppingBannerCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class ShoppingBannerCollCell: UICollectionViewCell {
    
    var timer: Timer?
    
    @IBOutlet weak var svContent: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    func stopScrolling() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    func startScrolling() {
        stopScrolling()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.scrolling), userInfo: nil, repeats: true)
    }
    
    @objc func scrolling() {
        var page = pageControl.currentPage + 1
        if page >= pageControl.numberOfPages {
            page = 0
        }
        pageControl.currentPage = page
        svContent.setContentOffset(CGPoint(x: CGFloat(page) * svContent.bounds.width, y: 0), animated: true)
    }

}

extension ShoppingBannerCollCell: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.width)
    }
}
