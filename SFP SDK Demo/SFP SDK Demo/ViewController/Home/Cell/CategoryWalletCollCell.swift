//
//  CategoryWalletCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryWalletCollCell: UICollectionViewCell {

    @IBOutlet weak var progress: UIActivityIndicatorView!
    
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.normalSemiBold
        }
    }
    
    @IBOutlet weak var lblAmount: UILabel! {
        didSet {
            lblAmount.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var vPromo: UIView!
    @IBOutlet weak var lblPromo: UILabel! {
        didSet {
            lblPromo.font = AppTheme.Font.tinyMedium
        }
    }
    
    @IBOutlet weak var vScan: UIView!
}
