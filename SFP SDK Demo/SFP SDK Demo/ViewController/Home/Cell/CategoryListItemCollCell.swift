//
//  CategoryListItemCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryListItemCollCell: UICollectionViewCell {

    @IBOutlet weak var vImageBG: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppTheme.Font.small
        }
    }
    
    func set(_ item: CategoryModel) {
        vImageBG.backgroundColor = item.color
        imgIcon.image = UIImage(named: item.image)
        lblTitle.text = item.name
    }
    
}
