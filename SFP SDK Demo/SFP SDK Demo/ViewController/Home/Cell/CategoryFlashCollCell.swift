//
//  CategoryFlashCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryFlashCollCell: UICollectionViewCell {
    
    var time = 60 * 60 * 24
    var timer: Timer?

    @IBOutlet weak var lblHour: UILabel! {
        didSet {
            lblHour.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var lblMinuter: UILabel! {
        didSet {
            lblMinuter.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var lblSecond: UILabel! {
        didSet {
            lblSecond.font = AppTheme.Font.normal
        }
    }
    
    func startCountDown() {
        stopCountDown()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
    }
    
    @objc func countDown() {
        time -= 1
        var second = time
        var minute = second / 60
        second = second % 60
        let hour = minute / 60
        minute = minute % 60
        if second > 9 {
            lblSecond.text = "\(second)"
        }
        else {
            lblSecond.text = "0\(second)"
        }
        if minute > 9 {
            lblMinuter.text = "\(minute)"
        }
        else {
            lblMinuter.text = "0\(minute)"
        }
        if hour > 9 {
            lblHour.text = "\(hour)"
        }
        else {
            lblHour.text = "0\(hour)"
        }
        
        if time == 0 {
            stopCountDown()
        }
    }
    
    func stopCountDown() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
}
