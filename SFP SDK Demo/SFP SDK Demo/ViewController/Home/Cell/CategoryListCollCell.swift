//
//  CategoryListCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryListCollCell: UICollectionViewCell {
    
    /**CategoryId**/
    var onSelected: ((CategoryModel) -> Void)?
    var categories = [CategoryModel]() {
        didSet {
            cvContent.reloadData()
        }
    }
    
    var cellSize = CGSize.zero
    
    @IBOutlet weak var cvContent: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = .zero
            cvContent.collectionViewLayout = layout
            
            cvContent.delegate = self
            cvContent.dataSource = self
            
            cvContent.showsVerticalScrollIndicator = false
            cvContent.showsHorizontalScrollIndicator = false
            
            cvContent.register(CategoryListItemCollCell.self)
        }
    }
    
}

extension CategoryListCollCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CategoryListItemCollCell = collectionView.dequeueCell(indexPath: indexPath)
        if indexPath.row < categories.count {
            cell.set(categories[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < categories.count {
            onSelected?(categories[indexPath.item])
        }
    }
}

extension CategoryListCollCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let width = collectionView.bounds.width - layout.sectionInset.left - layout.sectionInset.right
        let height = collectionView.bounds.height - layout.sectionInset.top - layout.sectionInset.bottom
        if cellSize == .zero {
            let numberOfRow = CGFloat(4)
            let size = (width - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
            cellSize = CGSize(width: size, height: height)
        }
        return cellSize
    }
}
