//
//  ShoppingCardVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/21/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
class ShoppingCardVC: BaseVC {
    var vm: ShoppingVM?
    
    @IBOutlet weak var imgCheckAll: UIImageView!
    @IBOutlet weak var lblCheckAll: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var btnCheckAll: UIButtonOneClick!
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnBuy: UIButtonOneClick!
    
    @IBOutlet weak var tbvProduct: UITableView! {
        didSet{
            tbvProduct.register(ShoppingCardCell.self)
            tbvProduct.dataSource = self
            tbvProduct.delegate = self
            tbvProduct.estimatedRowHeight = 44
            tbvProduct.rowHeight = UITableView.automaticDimension
        }
    }
    @IBOutlet weak var imgNotFound: UIImageView!
    @IBOutlet weak var vButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [lblCheckAll, lblTotal].forEach { (item) in
            item?.font = AppTheme.Font.normalMedium
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        lblAmount.font = AppTheme.Font.large18SemiBold
        lblAmount.textColor = AppTheme.Color.red
        [btnBack, btnCheckAll, btnBuy].forEach { (item) in
            item?.delegate = self
            item?.titleLabel?.font = AppTheme.Font.normalBold
        }
        tbvProduct.reloadData()
        refreshCheckAll()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
extension ShoppingCardVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnCheckAll:
            imgCheckAll.isHighlighted = !imgCheckAll.isHighlighted
            self.vm?.shopItems.forEach({ (item) in
                item.isChecked = imgCheckAll.isHighlighted
            })
            tbvProduct.reloadData()
            showTotalAmount()
            
        case btnBuy:
            let vc: PayMentVC = PayMentVC.newInstance()
            vc.vm = vm
            show(vc, sender: nil)
            
        default:
            break
        }
    }
}
extension ShoppingCardVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = vm?.getShopCount() ?? 0
        imgNotFound.isHidden = count > 0
        return count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = ShoppingCardHeader(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        view.onCheckChange = { [weak self] (isCheck) in
            guard let `self` = self else { return }
            let shopItems = self.vm?.getShopItems(section) ?? []
            shopItems.forEach { (item) in
                item.isChecked = isCheck
            }
            self.refreshCheckAll()
        }
        view.lblName.text = vm?.getShopName(section)
        
        let shopItems = self.vm?.getShopItems(section) ?? []
        let shopItemSelected = shopItems.filter { (item) -> Bool in
            return item.isChecked
        }
        view.imgCheck.isHighlighted = shopItems.count == shopItemSelected.count
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 5.0))
        view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        
        return  view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm?.getShopItemCount(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShoppingCardCell = tableView.dequeueCell()
        if let item = vm?.getShopItem(indexPath) {
            item.onRemove = {
                AlertVC.show(message: "Xóa sản phẩm khỏi giỏ hàng thành công", onCancel: { [weak self] () in
                    guard let `self` = self else { return }
                    self.vm?.removeBuy(item: item)
                    tableView.reloadData()
                    self.refreshCheckAll()
                })
            }
            cell.onCheckChange = { [weak self] (isSelected) in
                guard let `self` = self else { return }
                tableView.reloadSections([indexPath.section], with: .none)
                self.refreshCheckAll()
            }
            item.onAmountDisplay = { [weak self] (count, amount, oldAmount) in
                guard let `self` = self else { return }
                cell.tfCount.text = count
                cell.lblPriceOld.text = oldAmount
                cell.lblPrice.text = amount
                self.showTotalAmount()
            }
            cell.set(item)
        }
        return cell
    }
    
    func refreshCheckAll() {
        let shopItems = self.vm?.shopItems ?? []
        let shopItemSelected = shopItems.filter { (item) -> Bool in
            return item.isChecked
        }
        self.imgCheckAll.isHighlighted = shopItems.count == shopItemSelected.count
        showTotalAmount()
    }
    
    func showTotalAmount() {
        lblAmount.text = vm?.getTotalAmountDisplay()
        btnBuy.isEnabled = (vm?.getTotalAmount() ?? 0) > 0
        btnBuy.alpha = btnBuy.isEnabled ? 1 : 0.5
        vButton.isHidden = (vm?.shopItems.count ?? 0) == 0
    }
}
