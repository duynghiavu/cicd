//
//  PinMapVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/5/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import Polyline

class PinMapVC: BaseVC {
    var address: AddressModel?
    var onSelected: ((AddressModel) -> Void)?
    
    lazy var mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: 21.027989806468753, longitude: 105.85230588912964, zoom: 15)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.rx.observe(CGRect.self, #keyPath(UIView.bounds)).subscribe(onNext: { (rect) in
            if let rect = rect {
                mapView.padding = .init(top: rect.height / 2 - 20, left: 0, bottom: 0, right: 0)
            }
        }).disposed(by: disposeBag)
        return mapView
    }()
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var vMap: UIView! {
        didSet {
            vMap.addSubview(mapView)
            mapView.snp.makeConstraints { (maker) in
                maker.top.left.right.bottom.equalToSuperview()
            }
        }
    }
    
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnSelecte: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [btnBack, btnSelecte].forEach { (item) in
            item?.delegate = self
        }
        
        checkLocation()
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(recognizer:)))
        longPressRecognizer.minimumPressDuration = 0.5
        longPressRecognizer.delegate = self
        self.mapView.addGestureRecognizer(longPressRecognizer)
    }
    
    func checkLocation() {
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        if let coor = mapView.myLocation?.coordinate {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coor.latitude, longitude: coor.longitude, zoom: 15)
        }
    }
    
    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizer.State.began) {
            let longPressPoint = recognizer.location(in: self.mapView);
            let coordinate = mapView.projection.coordinate(for: longPressPoint)
            
            getAddressFromLatLon(coordinate) { [weak self] (address) in
                guard let `self` = self else { return }
                self.address = address
                self.mapView.clear()
                let marker = GMSMarker(position: coordinate)
                marker.title = address?.lines
                marker.map = self.mapView
                self.mapView.selectedMarker = marker
            }
        }
    }
    
    func getAddressFromLatLon(_ coordinate: CLLocationCoordinate2D, complete: ((AddressModel?) -> Void)?) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                complete?(AddressModel(address))
                return
            }
            complete?(nil)
        }
    }
}

extension PinMapVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnSelecte:
            if let address = address {
                onSelected?(address)
                onBackPressed()
            }
                        
        default:
            break
        }
    }
}

extension PinMapVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if address == nil, let coordinate = manager.location?.coordinate {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
            manager.stopUpdatingLocation()
            getAddressFromLatLon(coordinate) { [weak self] (address) in
                guard let `self` = self else { return }
                if let address = address {
                    self.address = address
                }
            }
        }
    }
}

extension PinMapVC : UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
