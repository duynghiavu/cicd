//
//  TaxiVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 2/1/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import Polyline
import SpiderSDK

enum TaxiType: Int {
    case moto = 4
    case card4 = 8
    case card7 = 10
}

class TaxiVC: BaseVC {
    var addressFrom: AddressModel? {
        didSet {
            if let lines = addressFrom?.lines {
                lblFrom.text = lines
                checkDirection()
            }
        }
    }
    var addressTo: AddressModel? {
        didSet {
            if let lines = addressTo?.lines {
                lblTo.text = lines
                checkDirection()
            }
        }
    }
    var taxiType: TaxiType = .moto {
        didSet {
            switch taxiType {
            case .moto:
                imgPointMoto?.isHidden = false
                imgPointCar4?.isHidden = true
                imgPointCar7?.isHidden = true
                
                lblMoto.font = AppTheme.Font.normalBold
                lblCard4.font = AppTheme.Font.normal
                lblCard7.font = AppTheme.Font.normal
                
            case .card4:
                imgPointMoto?.isHidden = true
                imgPointCar4?.isHidden = false
                imgPointCar7?.isHidden = true
                
                lblMoto.font = AppTheme.Font.normal
                lblCard4.font = AppTheme.Font.normalBold
                lblCard7.font = AppTheme.Font.normal
                
            case .card7:
                imgPointMoto?.isHidden = true
                imgPointCar4?.isHidden = true
                imgPointCar7?.isHidden = false
                
                lblMoto.font = AppTheme.Font.normal
                lblCard4.font = AppTheme.Font.normal
                lblCard7.font = AppTheme.Font.normalBold
            }
            calculateDirection()
        }
    }
    
    lazy var mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: 21.027989806468753, longitude: 105.85230588912964, zoom: 15)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapView.settings.compassButton = true
        mapView.rx.observe(CGRect.self, #keyPath(UIView.bounds)).subscribe(onNext: { (rect) in
            if let rect = rect {
                mapView.padding = .init(top: rect.height / 2 - 20, left: 0, bottom: 0, right: 0)
            }
        }).disposed(by: disposeBag)
        return mapView
    }()
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var lblFromTitle: UILabel!
    @IBOutlet weak var lblToTitle: UILabel!
    
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    
    @IBOutlet weak var lblDirection: UILabel!
    @IBOutlet weak var lblPresent: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet weak var lblMoto: UILabel!
    @IBOutlet weak var lblCard4: UILabel!
    @IBOutlet weak var lblCard7: UILabel!
    
    @IBOutlet weak var vMap: UIView! {
        didSet {
            vMap.addSubview(mapView)
            mapView.snp.makeConstraints { (maker) in
                maker.top.left.right.bottom.equalToSuperview()
            }
        }
    }
    
    @IBOutlet weak var imgPointMoto: UIImageView!
    @IBOutlet weak var imgPointCar4: UIImageView!
    @IBOutlet weak var imgPointCar7: UIImageView!
    
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnNotify: UIButtonOneClick!
    @IBOutlet weak var btnFrom: UIButtonOneClick!
    @IBOutlet weak var btnTo: UIButtonOneClick!
    
    @IBOutlet weak var btnMoto: UIButtonOneClick!
    @IBOutlet weak var btnCar4: UIButtonOneClick!
    @IBOutlet weak var btnCar7: UIButtonOneClick!
    
    @IBOutlet weak var btnAlam: UIButtonOneClick!
    @IBOutlet weak var btnBook: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [lblFromTitle, lblToTitle].forEach { (item) in
            item?.font = AppTheme.Font.normalBold
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        
        [lblFrom, lblTo, lblMoto, lblCard4, lblCard7].forEach { (item) in
            item?.font = AppTheme.Font.normal
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        
        [lblPresent, lblNote].forEach { (item) in
            item?.font = AppTheme.Font.small
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        
        [lblDirection, btnAlam.titleLabel, btnBook.titleLabel].forEach { (item) in
            item?.font = AppTheme.Font.normalBold
        }
        
        [btnBack, btnNotify, btnFrom, btnTo, btnMoto, btnCar4, btnCar7, btnAlam, btnBook].forEach { (item) in
            item?.delegate = self
        }
        
        checkLocation()
    }
    
    func checkLocation() {
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        if let coor = mapView.myLocation?.coordinate {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coor.latitude, longitude: coor.longitude, zoom: 15)
        }
    }
    
    func checkDirection() {
        if let fromCoordinate = addressFrom?.coordinate, let toCoordinate = addressTo?.coordinate {
            let fromLocation = CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude)
            let toLocation = CLLocation(latitude: toCoordinate.latitude, longitude: toCoordinate.longitude)
            let distance = Int(toLocation.distance(from: fromLocation))
            lblDirection.isHidden = false
            var distanceStr = ""
            if distance < 1000 {
                distanceStr += "\(distance)m"
            }
            else {
                distanceStr += String(format: "%0.1fkm", CGFloat(distance)/1000)
            }
            distanceStr += " \(FormatHelper.formatCurrency(distance * taxiType.rawValue))đ"
            lblDirection.text = distanceStr
            
//            ServiceAPI.directions(origin: fromCoordinate, destination: toCoordinate) { (success, data) in
//                if success,
//                   let json = data as? [String: Any],
//                   let routes = json["routes"] as? [[String: Any]] {
//                    for route in routes {
//                        if let routeOverviewPolyline = route["overview_polyline"] as? [String: Any],
//                           let points = routeOverviewPolyline["points"] as? String {
//                            let path = GMSPath.init(fromEncodedPath: points)
//
//                            let polyline = GMSPolyline(path: path)
//                            polyline.strokeColor = .black
//                            polyline.strokeWidth = 10.0
//                            polyline.map = self.mapView
//                        }
//                    }
//                }
//            }
                    
            
            let request = MKDirections.Request()
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: fromCoordinate, addressDictionary: nil))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: toCoordinate, addressDictionary: nil))
            request.requestsAlternateRoutes = false
            request.transportType = .walking
            
            let directions = MKDirections(request: request)
            
            directions.calculate { [unowned self] response, error in
                guard let response = response else { return }
                self.mapView.clear()
                show(polylines: googlePolylines(from: response))
                
                var marker = GMSMarker()
                marker.position = fromCoordinate
                marker.title = "Điểm đón"
                marker.map = mapView
                
                marker = GMSMarker()
                marker.position = toCoordinate
                marker.title = "Điểm đến"
                marker.map = mapView
            }
        }
    }
    
    func calculateDirection() {
        if let fromCoordinate = addressFrom?.coordinate, let toCoordinate = addressTo?.coordinate {
            let fromLocation = CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude)
            let toLocation = CLLocation(latitude: toCoordinate.latitude, longitude: toCoordinate.longitude)
            let distance = Int(toLocation.distance(from: fromLocation))
            lblDirection.isHidden = false
            var distanceStr = ""
            if distance < 1000 {
                distanceStr += "\(distance)m"
            }
            else {
                distanceStr += String(format: "%0.1fkm", CGFloat(distance)/1000)
            }
            distanceStr += " \(FormatHelper.formatCurrency(distance * taxiType.rawValue))đ"
            lblDirection.text = distanceStr
        }
    }
    
    func googlePolylines(from response: MKDirections.Response) -> [GMSPolyline] {
        let polylines: [GMSPolyline] = response.routes.map({ route in
            let route = response.routes[0]

            var coordinates = [CLLocationCoordinate2D](
                repeating: kCLLocationCoordinate2DInvalid,
                count: route.polyline.pointCount
            )

            route.polyline.getCoordinates(
                &coordinates,
                range: NSRange(location: 0, length: route.polyline.pointCount)
            )
            
            let polyline = Polyline(coordinates: coordinates)
            let encodedPolyline: String = polyline.encodedPolyline
            let path = GMSPath(fromEncodedPath: encodedPolyline)
            return GMSPolyline(path: path)
        })
        return polylines
    }
    
    func show(polylines: [GMSPolyline]) {
        polylines.forEach { polyline in
            let strokeStyles = [
                GMSStrokeStyle.solidColor(.blue)
            ]
            let strokeLengths = [
                NSNumber(value: 10),
                NSNumber(value: 6)
            ]
            if let path = polyline.path {
                polyline.spans = GMSStyleSpans(path, strokeStyles, strokeLengths, .rhumb)
            }
            polyline.strokeWidth = 3
            polyline.map = mapView
        }
    }
}

extension TaxiVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnNotify:
            print("btnNotify")
            
        case btnFrom:
            let vc: PlaceSearchVC = PlaceSearchVC.newInstance(storyboardName: "Taxi")
            vc.onSelected = { [weak self] (address) in
                guard let `self` = self else { return }
                self.addressFrom = address
            }
            show(vc, sender: nil)
            
        case btnTo:
            let vc: PlaceSearchVC = PlaceSearchVC.newInstance(storyboardName: "Taxi")
            vc.onSelected = { [weak self] (address) in
                guard let `self` = self else { return }
                self.addressTo = address
            }
            show(vc, sender: nil)
            
        case btnMoto:
            taxiType = .moto
            
        case btnCar4:
            taxiType = .card4
            
        case btnCar7:
            taxiType = .card7
            
        case btnAlam:
            print("btnAlam")
            
        case btnBook:
            let vc: PayMentMethodsVC = PayMentMethodsVC.newInstance(storyboardName: "Film")
            show(vc, sender: nil)
                        
        default:
            break
        }
    }
}

extension TaxiVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if addressFrom == nil, let coordinate = manager.location?.coordinate {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
            manager.stopUpdatingLocation()
            getAddressFromLatLon(coordinate) { [weak self] (address) in
                guard let `self` = self else { return }
                if let address = address {
                    self.addressFrom = address
                }
            }
        }
    }
    
    func getAddressFromLatLon(_ coordinate: CLLocationCoordinate2D, complete: ((AddressModel?) -> Void)?) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                complete?(AddressModel(address))
                return
            }
            complete?(nil)
        }
    }
}

extension TaxiVC: GMSMapViewDelegate {
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
//        polylineRenderer.strokeColor = UIColor.blue
//        polylineRenderer.fillColor = UIColor.red
//        polylineRenderer.lineWidth = 2
//        return polylineRenderer
//    }
//
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        if annotation is MKUserLocation { return nil }
//
//        let annotationIdentifier = "AnnotationIdentifier"
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
//
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
//            annotationView!.canShowCallout = true
//        }
//        else {
//            annotationView!.annotation = annotation
//        }
//        annotationView!.image = UIImage(named: "taxi_to")
//
//        return annotationView
//    }
//
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        print("calloutAccessoryControlTapped")
//    }
}
