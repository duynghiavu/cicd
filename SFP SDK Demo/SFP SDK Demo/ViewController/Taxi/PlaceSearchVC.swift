//
//  PlaceSearchVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/2/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PlaceSearchVC: BaseVC {
    var timer: Timer?
    var listNear = [AddressModel]()
    var listResult = [AddressModel]()
    var addressHome: AddressModel? {
        didSet {
            if let address = addressHome {
                lblHome.text = address.lines
            }
        }
    }
    var addressOffice: AddressModel? {
        didSet {
            if let address = addressOffice {
                lblOffice.text = address.lines
            }
        }
    }
    var onSelected: ((AddressModel) -> Void)?
    
    @IBOutlet weak var tfSearch: UITextField! {
        didSet {
            tfSearch.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var lblFromTitle: UILabel!
    @IBOutlet weak var lblToTitle: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblOffice: UILabel!
    
    @IBOutlet weak var tblNear: FitContentTableView!
    @IBOutlet weak var tblResult: UITableView!
    
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnMap: UIButtonOneClick!
    @IBOutlet weak var btnHome: UIButtonOneClick!
    @IBOutlet weak var btnOffice: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [lblFromTitle, lblToTitle].forEach { (item) in
            item?.font = AppTheme.Font.normalBold
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        
        [lblHome, lblOffice].forEach { (item) in
            item?.font = AppTheme.Font.normal
            item?.textColor = AppTheme.Color.primaryTextColor
        }
        
        [tblNear, tblResult].forEach { (item) in
            item?.delegate = self
            item?.dataSource = self
        }
        
        [btnBack, btnMap, btnHome, btnOffice].forEach { (item) in
            item?.delegate = self
        }
        
        let helper = UserDefaultsHelper.share()
        if helper.contain(key: .addressHome) {
            addressHome = AddressModel(helper.getDic(key: .addressHome))
        }
        if helper.contain(key: .addressOffice) {
            addressOffice = AddressModel(helper.getDic(key: .addressOffice))
        }
        if helper.contain(key: .addressNear) {
            listNear = AddressModel.listAddress(helper.getArray(key: .addressNear))
        }
    }
    
    @IBAction func tfSearchChange(_ sender: Any) {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkSearch), userInfo: nil, repeats: false)
    }
    
    @objc func checkSearch() {
        if let address = tfSearch.text?.trim(), address.isNotEmpty {
            getLatLonFromAddress(address: address) { [weak self] (listResult) in
                self?.listResult = listResult
                self?.tblResult.reloadDataAndView()
            }
            return
        }
        listResult.removeAll()
        tblResult.reloadDataAndView()
    }
    
    func getLatLonFromAddress(address: String, complete: (([AddressModel]) -> Void)?) {
        let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(address) { (placemarks, error) in
                var result = [AddressModel]()
                if let placemarks = placemarks {
                    for placemark in placemarks {
                        result.append(AddressModel(placemark))
                    }
                }
                complete?(result)
            }
    }
    
    func selected(_ address: AddressModel) {
        for (index, item) in listNear.enumerated() {
            if item.lines == address.lines {
                listNear.remove(at: index)
            }
        }
        listNear.insert(address, at: 0)
        var array = [[String: Any]]()
        listNear.forEach { (item) in
            array.append(item.toJson())
        }
        UserDefaultsHelper.share().set(value: array, key: .addressNear)
        onSelected?(address)
        onBackPressed()
    }
}

extension PlaceSearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tblNear:
            return listNear.count
            
        case tblResult:
            tblResult.isHidden = listResult.count == 0
            return listResult.count
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell?.textLabel?.font = AppTheme.Font.normal
            cell?.textLabel?.textColor = AppTheme.Color.primaryTextColor
        }
        
        switch tableView {
        case tblNear:
            if indexPath.row < listNear.count {
                cell?.textLabel?.text = listNear[indexPath.row].lines
            }
            
        case tblResult:
            if indexPath.row < listResult.count {
                cell?.textLabel?.text = listResult[indexPath.row].lines
            }
            
        default:
            break
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case tblNear:
            if indexPath.row < listNear.count {
                selected(listNear[indexPath.row])
            }
            
        case tblResult:
            if indexPath.row < listResult.count {
                selected(listResult[indexPath.row])
            }
            
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
        onBackPressed()
    }
}

extension PlaceSearchVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnMap:
            let vc: PinMapVC = PinMapVC.newInstance(storyboardName: "Taxi")
            vc.onSelected = { [weak self] (address) in
                guard let `self` = self else { return }
                self.selected(address)
            }
            show(vc, sender: nil)
            
        case btnHome:
            if let address = addressHome {
                selected(address)
            }
            else {
                let vc: PinMapVC = PinMapVC.newInstance(storyboardName: "Taxi")
                vc.onSelected = { [weak self] (address) in
                    guard let `self` = self else { return }
                    self.addressHome = address
                    UserDefaultsHelper.share().set(value: address.toJson(), key: .addressHome)
                }
                show(vc, sender: nil)
            }
            
        case btnOffice:
            if let address = addressOffice {
                selected(address)
            }
            else {
                let vc: PinMapVC = PinMapVC.newInstance(storyboardName: "Taxi")
                vc.onSelected = { [weak self] (address) in
                    guard let `self` = self else { return }
                    self.addressOffice = address
                    UserDefaultsHelper.share().set(value: address.toJson(), key: .addressOffice)
                }
                show(vc, sender: nil)
            }
                        
        default:
            break
        }
    }
}
