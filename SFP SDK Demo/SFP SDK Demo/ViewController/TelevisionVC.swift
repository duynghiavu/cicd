//
//  TelevisionVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/25/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class TelevisionVC: BaseVC{
    var vm = FilmVM()
    
    @IBOutlet weak var tbvMovie: UITableView!{
        didSet{
            tbvMovie.register(TelevisionCell.self)
            tbvMovie.dataSource = self
            tbvMovie.delegate = self
        }
    }
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var btnNotify: UIButtonOneClick!
    @IBOutlet weak var btnSearch: UIButtonOneClick!
    @IBOutlet weak var cvContent: UICollectionView!{
        didSet {
            let numberOfRow = CGFloat(1)
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = .zero
            let size = (SCREEN_WIDTH - 32 - layout.sectionInset.left - layout.sectionInset.right - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
            layout.itemSize = CGSize(width: size, height: size)
            layout.scrollDirection = .horizontal
            cvContent.collectionViewLayout = layout
            cvContent.isPagingEnabled = true
            cvContent.delegate = self
            cvContent.dataSource = self
            cvContent.register(TelevisionCVCell.self)
        }
    }
    @IBOutlet weak var pageControll: UIPageControl!
    
    var storedOffsets = [Int: CGFloat]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [btnBack, btnNotify, btnSearch].forEach { (item) in
            item?.delegate = self
        }
        
        vm.reloadBanner.next { [weak self] (value) in
            guard let `self` = self else { return }
            if value {
                self.cvContent.reloadData()
            }
        } ~> disposeBag
        
        vm.reloadCategorys.next { [weak self] (value) in
            guard let `self` = self else { return }
            if value {
                self.tbvMovie.reloadData()
            }
        } ~> disposeBag
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
}

extension TelevisionVC: UIButtonOneClickAction{
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnNotify:
            print("btnNotify")
            
        case btnSearch:
            let vc: FilmSearchVC = FilmSearchVC.newInstance(storyboardName: "Film")
            vc.vm = vm
            show(vc, sender: nil)
                    
        default:
            break
        }
    }
}

extension TelevisionVC:  UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return vm.categorys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView(frame: .zero)
        v.backgroundColor = .clear
        
        let lbl = UILabel(frame: .zero)
        lbl.backgroundColor = .clear
        lbl.font = AppTheme.Font.largeBold
        lbl.textColor = .white
        lbl.text = vm.categorys[section].name
        
        let btn = UIButton(type: .custom)
        btn.backgroundColor = .clear
        btn.setImage(UIImage(named: "add_box"), for: .normal)
        
        v.addSubViews(lbl, btn)
        lbl.snp.makeConstraints { (maker) in
            maker.top.equalTo(8)
            maker.left.equalTo(16)
            maker.bottom.equalToSuperview()
        }
        btn.snp.makeConstraints { (maker) in
            maker.top.equalTo(8)
            maker.left.equalTo(lbl.snp.right)
            maker.right.equalTo(-16)
            maker.bottom.equalToSuperview()
        }
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TelevisionCell") as? TelevisionCell else {
            return UITableViewCell()
        }
        cell.onSelected = { (item) in
            let vc: TelevisionDetailVC = TelevisionDetailVC.newInstance(storyboardName: "Film")
            vc.item = item
            self.show(vc, sender: nil)
        }
        if indexPath.row < vm.categorys.count {
            cell.set(vm.categorys[indexPath.row])
        }
        return cell
    }
}

extension TelevisionVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControll.numberOfPages = vm.banner.count
        return vm.banner.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TelevisionCVCell = collectionView.dequeueCell(indexPath: indexPath)
        if indexPath.item < vm.banner.count {
            cell.imgItem.image = UIImage(named: vm.banner[indexPath.item])
        }
        return cell
    }
}

extension TelevisionVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == cvContent {
            let width = Int(scrollView.bounds.width)
            var page = Int(scrollView.contentOffset.x) / width
            if Int(scrollView.contentOffset.x) % width > width / 2 {
                page += 1
            }
            pageControll.currentPage = page
        }
    }
}
