//
//  CollectionFlowLayoutVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/13/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class CollectionFlowLayoutVC: BaseVC {
    
    @IBOutlet weak var collectionview: UICollectionView!
    override func loadView() {
        super.loadView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionview.backgroundColor = .white
        self.collectionview.dataSource = self
        self.collectionview.delegate = self
        
        self.collectionview.register(MyCell.self, forCellWithReuseIdentifier: "MyCell")
    }
}

extension CollectionFlowLayoutVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionview {
            return 16
        }else{
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! MyCell
        let index = indexPath.row
        switch index {
        case 0:
            cell.imgIcon.image = UIImage(named: "Group 189")
            cell.textLabel.text = "Flash Sale"
        case 1:
            cell.imgIcon.image = UIImage(named: "storefront-24px")
            cell.textLabel.text = "Shop ưa thích"
        case 2:
            cell.imgIcon.image = UIImage(named: "coupon (1)")
            cell.textLabel.text = "Deal cho bạn chỉ từ 1K"
        case 3:
            cell.imgIcon.image = UIImage(named: "free-delivery")
            cell.textLabel.text = "Miễn phí vận chuyển"
        case 4:
            cell.imgIcon.image = UIImage(named: "Group 190")
            cell.textLabel.text = "Deal cho bạn chỉ từ 1K"
        case 5:
            cell.imgIcon.image = UIImage(named: "coins")
            cell.textLabel.text = "Săn xu mỗi ngày"
        case 6:
            cell.imgIcon.image = UIImage(named: "Path 3583")
            cell.textLabel.text = "Hàng quốc tế"
        case 7:
            cell.imgIcon.image = UIImage(named: "Path 3586")
            cell.textLabel.text = "Hàng quốc tế"
        default:
            cell.imgIcon.image = UIImage(named: "Path 3586")
            cell.textLabel.text = String(indexPath.row + 1)
        }
        
        return cell
    }
    
}

extension CollectionFlowLayoutVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row + 1)
    }
}

extension CollectionFlowLayoutVC: UICollectionViewDelegateFlowLayout {
    
            func collectionView(_ collectionView: UICollectionView,
                                layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {
                if collectionView == self.collectionview {
                    let size = collectionview.bounds.width/4
                    let sizeH = collectionview.bounds.height/2
                    return CGSize(width: size, height: sizeH)
                }else{
                    let size = collectionview.bounds.width/2
                    let sizeH = collectionview.bounds.height
                    return CGSize(width: size, height: sizeH)
                }
               
            }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    
}

