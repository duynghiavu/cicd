//
//  WalletActivateVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/9/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class WalletActivateVC: BaseVC {
    
    var isPayment = false
    var presentedVC: UIViewController?

    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var btnActivate: UIButtonOneClick!
    @IBOutlet weak var btnSkip: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblContent.attributedText = NSMutableAttributedString()
            .normal("Kích hoạt ngay ví ")
            .add(value: "Shopify Pay", font: AppTheme.Font.normalBold, color: UIColor(hex: 0x3669C9))
            .normal(" để nhận được nhiều ưu đãi, giảm giá và tích điểm.")
        
        [btnActivate, btnSkip].forEach { v in
            v?.delegate = self
        }
    }

}

extension WalletActivateVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnActivate:
            popNavigationOrDismiss(true) {
                if let vc = self.presentedVC {
                    let data = UserDefaultsHelper.share().getTicketData()
                    SpiderSDK.gotoVerify(muid: data.muid, suid: data.suid, isPayment: self.isPayment, fromVC: vc) { code, message, data in
                        if code != "0" {
//                            AlertVC.show(message: message)
                        }
                    }
                }
            }
            
        case btnSkip:
            onBackPressed()
            
        default:
            break
        }
    }
}
