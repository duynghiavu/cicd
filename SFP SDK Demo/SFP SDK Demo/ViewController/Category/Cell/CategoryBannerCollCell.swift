//
//  CategoryBannerCollCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/4/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit

class CategoryBannerCollCell: UICollectionViewCell {

    @IBOutlet weak var imgBanner: UIImageView!

}
