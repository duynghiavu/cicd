//
//  CategoryVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/3/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit
import MBProgressHUD
import SpiderSDK
import RxSwift

class CategoryVC: BaseVC {
    var vm: ShoppingVM?
    var isShowVerify = false
    
    var cellSize: CGSize = .zero
    var walletCell: CategoryWalletCollCell?
    
    @IBOutlet weak var btnBack: UIButtonOneClick! {
        didSet {
            btnBack.delegate = self
        }
    }
    
    @IBOutlet weak var cvContent: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 16
            layout.minimumInteritemSpacing = 16
            layout.sectionInset = .init(top: 16, left: 16, bottom: 0, right: 16)
            cvContent.collectionViewLayout = layout
            
            cvContent.contentInset = .init(top: 0, left: 0, bottom: 16, right: 0)
            
            cvContent.delegate = self
            cvContent.dataSource = self
            cvContent.register(CategoryBannerCollCell.self)
            cvContent.register(ProductHomeCell.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vm = vm {
            vm.reloaItems.next { [weak self] (_) in
                guard let `self` = self else { return }
                self.cvContent.reloadData()
            }.disposed(by: disposeBag)
            
            if let title = vm.categorySelected?.name {
                lblTitle.text = title
            }
            else {
                lblTitle.text = "Flash deal"
            }
        }
    }
}

extension CategoryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return UICollectionReusableView(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.width, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        default:
            return vm?.getCategoryItems().count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell: CategoryBannerCollCell = collectionView.dequeueCell(indexPath: indexPath)
            cell.imgBanner.image = UIImage(named: "banner_category_\(vm?.categorySelected?.id ?? "0")")
            return cell
            
        default:
            let cell: ProductHomeCell = collectionView.dequeueCell(indexPath: indexPath)
            if let vm = vm {
                let categorys = vm.getCategoryItems()
                if indexPath.row < categorys.count {
                    cell.set(categorys[indexPath.row], isFlash: vm.categorySelected == nil)
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            break
            
        default:
            if let vm = vm {
                let categorys = vm.getCategoryItems()
                if indexPath.row < categorys.count {
                    let vc: InforDetailProductImageVC = InforDetailProductImageVC.newInstance()
                    vc.vm = vm
                    vc.currentProduct = categorys[indexPath.row]
                    show(vc, sender: nil)
                }
            }
        }
    }
}

extension CategoryVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let width = SCREEN_WIDTH - layout.sectionInset.left - layout.sectionInset.right
        switch indexPath.section {
        case 0:
            return CGSize(width: width, height: width * 150 / 345)
            
        default:
            if cellSize == .zero {
                let numberOfRow = CGFloat(2)
                let size = (width - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
                cellSize = CGSize(width: size, height: size * 22 / 15)
            }
            return cellSize
        }
    }
}

extension CategoryVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        onBackPressed()
    }
}
