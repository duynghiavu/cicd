//
//  BaseEditVC.swift
//  SFPSDK
//
//  Created by NghiaVD on 7/24/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation
import UIKit

class BaseEditVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addTapGesture(target: self, action: #selector(self.tapInView), cancelInView: false)
    }
    
    @objc func tapInView() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: .keyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: .keyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        
        NotificationCenter.default.removeObserver(self, name: .keyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .keyboardWillHide, object: nil)
    }
    
    var frame: CGRect!
    @objc func keyboardDidShow(notification: Notification) {
        if frame == nil {
            frame = view.frame
        }
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var rect = frame!
            rect.size.height -= keyboardSize.height - safeAreaInsets.bottom
            view.frame = rect
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        if frame == nil {
            frame = view.frame
        }
        view.frame = frame
    }
}

extension BaseEditVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
