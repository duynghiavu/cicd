//
//  HomeCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//
import UIKit

class HomeCell: UITableViewCell {
    


    @IBOutlet weak var viewContent: UIView!
    
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewBg: UIView!
    
}
