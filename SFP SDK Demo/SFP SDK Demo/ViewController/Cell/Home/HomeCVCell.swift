//
//  HomeCVCell.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 1/20/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class HomeCVCell: UICollectionViewCell {

    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel! {
        didSet {
            lblTitle.font = AppTheme.Font.normalBold
            lblTitle.textColor = AppTheme.Color.primaryTextColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
