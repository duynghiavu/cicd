//
//  CategoryCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/11/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit



class CategoryCell: UITableViewCell {
    
    
    @IBOutlet weak var imgCategory1: UIImageView!
    @IBOutlet weak var lblTitleCategory1: UILabel!
    @IBOutlet weak var imgCategory2: UIImageView!
    @IBOutlet weak var imgCategory3: UIImageView!
    @IBOutlet weak var imgCategory4: UIImageView!

    @IBOutlet weak var lblTitleCategory2: UILabel!
    @IBOutlet weak var lblTitleCategory3: UILabel!
    @IBOutlet weak var lblTitleCategory4: UILabel!
    
    
}

