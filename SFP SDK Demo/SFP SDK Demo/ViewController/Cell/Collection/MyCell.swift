//
//  MyCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/13/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class MyCell: UICollectionViewCell {
    var textLabel: UILabel!
    var imgIcon: UIImageView!
    var imgView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textLabel = UILabel(frame: .zero)
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 0
        textLabel.font = AppTheme.Font.small
        textLabel.textColor = AppTheme.Color.primaryTextColor
        
        imgIcon = UIImageView(frame: .zero)
        imgView = UIView(frame: .zero)
        imgView.xibCornerRadius = 16
        imgView.xibBorderColor = .lightGray
        imgView.xiBorderWidth = 1
        
        imgView.addSubview(imgIcon)
        imgIcon.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
//            maker.width.height.equalToSuperview().multipliedBy(0.6)
            maker.height.equalTo(30)
            maker.width.equalTo(30)
        }
        
        contentView.addSubview(imgView)
        imgView.snp.makeConstraints { (maker) in
            maker.top.equalTo(10)            
            maker.centerX.equalToSuperview()
//            maker.width.equalToSuperview().multipliedBy(0.5)
//            maker.height.equalTo(imgView.snp.width)
            maker.height.equalTo(50)
            maker.width.equalTo(50)
        }
        
        contentView.addSubview(textLabel)
        textLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(imgView.snp.bottom).offset(8)
            maker.centerX.equalToSuperview()
//            maker.left.equalTo(8)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.textLabel.text = nil
        self.imgIcon.image = UIImage(named: "Path 3583")

    }
    
}
