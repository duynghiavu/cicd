//
//  InforDetailProductImageCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/15/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//
import UIKit
class InforDetailProductImageCell: UICollectionViewCell {
    var imgIcon: UIImageView!
    var btnBack: UIButton!
    var btnMenu: UIButton!
    var lbAmount: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imgIcon = UIImageView(frame: .zero)
        imgIcon.contentMode = .scaleAspectFit

        contentView.addSubview(imgIcon)
        imgIcon.snp.makeConstraints { (maker) in
            maker.top.left.equalTo(16)
            maker.bottom.right.equalTo(-16)
        }
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        self.imgIcon.image = nil

    }
    
    
}


