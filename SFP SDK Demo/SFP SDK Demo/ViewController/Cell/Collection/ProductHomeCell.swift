//
//  ProductHomeCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/14/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class ProductHomeCell: UICollectionViewCell {
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.normalMedium
            lblName.textColor = AppTheme.Color.primaryTextColor
            lblName.numberOfLines = 0
        }
    }
    @IBOutlet var lblPrice: UILabel! {
        didSet {
            lblPrice.font = AppTheme.Font.smallBold
            lblPrice.textColor = AppTheme.Color.red
        }
    }
    @IBOutlet weak var vRate: UIView!
    @IBOutlet var lblRate:UILabel! {
        didSet {
            lblRate.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblViewNumber: UILabel! {
        didSet {
            lblViewNumber.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblViews: UILabel! {
        didSet {
            lblViews.font = AppTheme.Font.small
        }
    }
    
    @IBOutlet weak var vFlash: UIView!
    @IBOutlet var lblDiscount: UILabel! {
        didSet {
            lblDiscount.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblStatus: UILabel! {
        didSet {
            lblStatus.font = AppTheme.Font.tiny
        }
    }
    let colors = [UIColor(hexString: "#E22727").cgColor, UIColor(hexString: "#FDB600").cgColor]
    let startPoint = CGPoint(x: 0, y: 0.5)
    let endPoint = CGPoint(x: 1, y: 0.5)
    @IBOutlet weak var vFlashBG: UIView! {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.vFlashBG.addGradientBackground(colors: self.colors, startPoint: self.startPoint, endPoint: self.endPoint)
            }
        }
    }
    
    func set(_ item: ProductModel, isFlash: Bool = false) {
        imgIcon.image = UIImage(named: item.images.first ?? "")
        lblName.text = item.name
        lblPrice.text = item.amountDisPlay()
        lblRate.text = item.rate
        lblViewNumber.text = item.viewCount
        
        vRate.alpha = isFlash ? 0 : 1
        vFlash.isHidden = !isFlash
        lblDiscount.text = item.discount()
        if item.sold == 0 {
            lblStatus.text = "Vừa mở bán"
        }
        else if item.maxSell - item.sold <= 1 {
            lblStatus.text = "Sắp bán hết"
        }
        else {
            lblStatus.text = "Đã bán \(item.sold)"
        }
        let max = CGFloat(item.maxSell - 1)
        var min = CGFloat(item.sold)
        if min > max {
            min = max
        }
        var f = min / max
        if f < 0.18 {
            f = 0.18
        }
        UIView.animate(withDuration: 0) {
            self.vFlashBG.snp.remakeConstraints { make in
                make.width.equalToSuperview().multipliedBy(f)
            }
        } completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.vFlashBG.addGradientBackground(colors: self.colors, startPoint: self.startPoint, endPoint: self.endPoint)
            }
        }
    }
}

