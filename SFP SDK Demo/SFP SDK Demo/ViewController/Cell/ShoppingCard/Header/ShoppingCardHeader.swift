//
//  ShoppingCardHeader.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/21/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
class ShoppingCardHeader: UIView {
    var onCheckChange: ((Bool) -> Void)?
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButtonOneClick! {
        didSet {
            btnCheck.delegate = self
        }
    }
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.normalBold
            lblName.textColor = AppTheme.Color.primaryTextColor
        }
    }
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
         super.init(frame: frame)
         commonInit()
     }
     
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         commonInit()
     }
     
     private func commonInit() {
        Bundle.main.loadNibNamed("ShoppingCardHeader", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(contentView)
     }
    
}

extension ShoppingCardHeader: UIButtonOneClickAction{
    func buttonSendAction(button: UIButtonOneClick) {
        imgCheck.isHighlighted = !imgCheck.isHighlighted
        onCheckChange?(imgCheck.isHighlighted)
    }
}
