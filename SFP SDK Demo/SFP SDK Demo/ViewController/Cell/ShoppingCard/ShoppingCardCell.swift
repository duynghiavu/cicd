//
//  ShoppingCardCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/21/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit



class ShoppingCardCell: UITableViewCell {
    var onCheckChange: ((Bool) -> Void)?
    var item: ProductModel?
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.largeBold
        }
    }
    @IBOutlet weak var lblPriceOld: UILabel! {
        didSet {
            lblPriceOld.font = AppTheme.Font.normal
        }
    }
    @IBOutlet weak var lblPrice: UILabel! {
        didSet {
            lblPrice.font = AppTheme.Font.normal
        }
    }
    
    @IBOutlet weak var tfCount: UITextField! {
        didSet {
            tfCount.font = AppTheme.Font.small
            tfCount.textColor = AppTheme.Color.primaryTextColor
        }
    }
    
    @IBOutlet weak var btnCheck: UIButtonOneClick!
    @IBOutlet weak var btnSum: UIButtonOneClick!
    @IBOutlet weak var btnMinus: UIButtonOneClick!
    
    static let identifier = "ShoppingCardCell"
    static var nib: UINib {
        return UINib(nibName: "ShoppingCardCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // getAmount()
        
        //        // tam thoi khong cho nhap so luong san pham
        self.tfCount.isUserInteractionEnabled = false
        tfCount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        [btnCheck, btnSum, btnMinus].forEach { (item) in
            item?.delegate = self
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(_ item: ProductModel) {
        self.item = item
        
        imgIcon.image = UIImage(named: item.images.first ?? "")
        imgCheck.isHighlighted = item.isChecked
        
        lblName.text = item.name
        lblPriceOld.isHidden = item.amountOld <= 0
        lblPriceOld.attributedText = item.totalOldAmountDisPlay().strikeThrough()
        lblPrice.text = item.totalAmountDisPlay()
        tfCount.text = "\(item.count)"
        
        item.onCheckChange = { [weak self] (isCheck) in
            guard let `self` = self else { return }
            self.imgCheck.isHighlighted = isCheck
        }
    }
}
extension ShoppingCardCell: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnCheck:
            imgCheck.isHighlighted = !imgCheck.isHighlighted
            item?.isChecked = imgCheck.isHighlighted
            onCheckChange?(imgCheck.isHighlighted)
            
        case btnSum:
            item?.addCount(count: 1)
            
        case btnMinus:
            item?.addCount(count: -1)
            
        default:
            break
        }
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        if sender.text == "" {
            item?.count = 0
        }else{
            if let txtAmount = tfCount.text,
               let amount:Int = Int(txtAmount){
                item?.count = amount
            }
        }
        
    }
}
