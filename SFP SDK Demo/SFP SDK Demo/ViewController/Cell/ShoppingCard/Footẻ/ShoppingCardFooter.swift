//
//  ShoppingCardFooter.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/26/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class ShoppingCardFooter: UIView {
    
    @IBOutlet var contentView: UIView!
    
    
    override init(frame: CGRect) {
         super.init(frame: frame)
         commonInit()
     
     }
     
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         commonInit()
     }
     
     private func commonInit() {
        Bundle.main.loadNibNamed("ShoppingCardFooter", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(contentView)
     }
}
