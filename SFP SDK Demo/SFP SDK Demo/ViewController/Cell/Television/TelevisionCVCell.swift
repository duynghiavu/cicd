//
//  TelevisionCVCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/27/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
class TelevisionCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(image: String)  {
        imgItem.image = UIImage(named: image)
    }
}
