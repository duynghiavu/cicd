//
//  TelevisionCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/27/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class TelevisionCell: UITableViewCell {
    var item: FilmCategoryModel?
    var onSelected: ((FilmModel) -> Void)?
    
    @IBOutlet weak var cvContent: UICollectionView!{
        didSet {
            let numberOfRow = CGFloat(3)
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 8
            layout.minimumInteritemSpacing = 8
            layout.sectionInset = .init(top: 0, left: 16, bottom: 16, right: 16)
            let size = (SCREEN_WIDTH - layout.sectionInset.left - layout.sectionInset.right - (layout.minimumInteritemSpacing * (numberOfRow - 1))) / numberOfRow
            layout.itemSize = CGSize(width: size, height: size * 1.5)
            cvContent.collectionViewLayout = layout
            layout.scrollDirection = .horizontal
//            cvContent.isPagingEnabled = true
            cvContent.delegate = self
            cvContent.dataSource = self
                  cvContent.register(TelevisionCVCell.self)
              }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func set(_ item: FilmCategoryModel) {
        self.item = item
        cvContent.reloadData()
    }

}

extension TelevisionCell: UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item?.films.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "TelevisionCVCell", for: indexPath) as? TelevisionCVCell else {
            return UICollectionViewCell()
        }
        if let films = item?.films, indexPath.item < films.count {
            cell.imgItem.image = UIImage(named: films[indexPath.item].image)
        }
       
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let films = item?.films, indexPath.item < films.count {
            onSelected?(films[indexPath.item])
        }
    }
}
