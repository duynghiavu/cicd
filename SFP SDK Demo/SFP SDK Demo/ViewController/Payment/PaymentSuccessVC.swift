//
//  PaymentSuccessVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/26/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class PaymentSuccessVC: BaseVC {
    
    var transId = ""
    var desc = ""
    var amount = 0
    
    @IBOutlet var lblStatus: UILabel!
    
    @IBOutlet var lblTransId: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblAmount: UILabel!
    
    @IBOutlet weak var btnHome: UIButtonOneClick!
    @IBOutlet weak var btnDetail: UIButtonOneClick!
    @IBOutlet weak var btnBack: UIButtonOneClick!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [lblStatus].forEach { (item) in
            item?.font = AppTheme.Font.normalBold
        }
        [lblTransId, lblDesc, lblAmount].forEach { (item) in
            item?.font = AppTheme.Font.normal
        }
        
        [btnBack, btnHome, btnDetail].forEach { (item) in
            item?.delegate = self
            item?.titleLabel?.font = AppTheme.Font.normalBold
        }

        if transId.isNotEmpty {
            lblTransId.text = transId
        }
        if desc.isNotEmpty {
            lblDesc.text = desc
        }
        lblAmount.text = FormatHelper.formatCurrency(amount)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension PaymentSuccessVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnHome:
            if let nav = self.navigationController {
                nav.viewControllers.forEach { vc in
                    if vc is HomeShoppingVC {
                        nav.popToViewController(vc, animated: true)
                    }
                }
            }
            
        default:
            break
        }
    }
}
