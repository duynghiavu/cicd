//
//  PaymentMethodVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 1/21/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class PaymentMethodVC: BaseVC {
    var balanceText = "Vui lòng xác thực thiết bị"
    
    @IBOutlet weak var btnBack: UIButtonOneClick! {
        didSet {
            btnBack.delegate = self
        }
    }
    
    @IBOutlet weak var lblWalletTitle: UILabel! {
        didSet {
            lblWalletTitle.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet weak var lblWalletBalance: UILabel! {
        didSet {
            lblWalletBalance.font = AppTheme.Font.normalMedium
            lblWalletBalance.text = balanceText
        }
    }
    
    @IBOutlet weak var lblCard: UILabel! {
        didSet {
            lblCard.font = AppTheme.Font.normalMedium
        }
    }
    
    @IBOutlet weak var lblPayment: UILabel! {
        didSet {
            lblPayment.font = AppTheme.Font.normalMedium
        }
    }
    
    @IBOutlet weak var btnContinue: UIButtonOneClick! {
        didSet {
            btnContinue.titleLabel?.font = AppTheme.Font.normalSemiBold
            btnContinue.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension PaymentMethodVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        case btnContinue:
            onBackPressed()
            
        default:
            break
        }
    }
}
