//
//  PayMentMethodsVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 2/1/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class PayMentMethodsVC: BaseVC {
    
    @IBOutlet weak var tbvContent: UITableView!{
        didSet{
            tbvContent.register(PayMentsCell.self)
            tbvContent.dataSource = self
            tbvContent.delegate = self
            tbvContent.estimatedRowHeight = 44
            tbvContent.rowHeight = UITableView.automaticDimension
            
        }
    }
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(clickBack(_:)), for: .touchUpInside)
        btnSubmit.addTarget(self, action: #selector(SubmitChange(_:)), for: .touchUpInside)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
extension PayMentMethodsVC{
    @objc func clickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @objc func SubmitChange(_ sender: UIButton) {
        showLoading()
        let data = UserDefaultsHelper.share().getTicketData()
        let body = ["amount": 100000, "totalAmount": 100000 + 15000, "merchantTxtRef": "merchantTxtRef", "shippingFee": 15000, "taxFee": 0, "discount": 0, "orderDescription": "", "currency": "đ"] as [String : Any]
        SpiderSDK.gotoSpiderPay(muid: data.muid, suid: data.suid, fromVC: self) { code, message, data in
//            if code != "0" {
//                AlertVC.show(message: message)
//            }
            self.hideLoading()
        }
    }
}
extension PayMentMethodsVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PayMentsCell = tableView.dequeueCell()
        switch indexPath.row {
        case 0:
            cell.lbTitle.text = "Spider"
            cell.imgCheck.image = UIImage(named: "true-checked")
            cell.lbTitle.font = AppTheme.Font.normalBold
        case 1:
            cell.lbTitle.text = "Thẻ tín dụng/Ghi nợ"
            cell.imgIcon.image = UIImage(named: "credit-card (11)")
            cell.lbTitle.font = AppTheme.Font.normal
        case 2:
            cell.lbTitle.text = "Thanh toán khi nhận hàng"
            cell.imgCheck.isHidden = true
            cell.imgIcon.image = UIImage(named: "bill")
            cell.lbTitle.font = AppTheme.Font.normal
        default:
            cell.lbTitle.text = "Spider"
            cell.imgCheck.image = UIImage(named: "true-check")
        }
        return cell
    }
}
