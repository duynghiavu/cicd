//
//  PayMentCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/22/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class PayMentCell: UITableViewCell {
    var item: ProductModel?
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.large18Bold
            lblName.textColor = AppTheme.Color.gray0C1A30
        }
    }
    @IBOutlet weak var lblOldPrice: UILabel! {
        didSet {
            lblOldPrice.font = AppTheme.Font.normalMedium
            lblOldPrice.textColor = AppTheme.Color.gray8C969F
        }
    }
    @IBOutlet weak var lblPrice: UILabel! {
        didSet {
            lblPrice.font = AppTheme.Font.largeMedium
            lblPrice.textColor = AppTheme.Color.red
        }
    }
    
    @IBOutlet weak var lblCount: UILabel! {
        didSet {
            lblCount.font = AppTheme.Font.small
            lblCount.textColor = AppTheme.Color.primaryTextColor
        }
    }
    
    static let identifier = "PayMentCell"
    static var nib: UINib {
        return UINib(nibName: "PayMentCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(_ item: ProductModel) {
        self.item = item
        
        imgIcon.image = UIImage(named: item.images.first ?? "")
        
        lblName.text = item.name
        lblOldPrice.isHidden = item.amountOld <= 0
        lblOldPrice.attributedText = item.amountOldDisPlay().strikeThrough()
        lblPrice.text = item.amountDisPlay()
        lblCount.text = "X\(item.count)"
    }
}
