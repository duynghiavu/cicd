//
//  PayMentsCell.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 2/1/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit

class PayMentsCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
}
