//
//  PayMentVC.swift
//  SFP SDK Demo
//
//  Created by Nguyen Minh Hieu on 1/22/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class PayMentVC: BaseVC {
    var isNow = false
    var vm: ShoppingVM?
    
    //MARK: -
    @IBOutlet var lblAddressTitle: UILabel! {
        didSet {
            lblAddressTitle.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblName: UILabel! {
        didSet {
            lblName.font = AppTheme.Font.smallBold
        }
    }
    @IBOutlet var lblAddress: UILabel! {
        didSet {
            lblAddress.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet var lblBill: UILabel! {
        didSet {
            lblBill.font = AppTheme.Font.smallMedium
        }
    }
    @IBOutlet var lblBillRequire: UILabel! {
        didSet {
            lblBillRequire.font = AppTheme.Font.small
        }
    }
    
    //MARK: -
    @IBOutlet var lblPostSelect: UILabel! {
        didSet {
            lblPostSelect.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPostName: UILabel! {
        didSet {
            lblPostName.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPostFee: UILabel! {
        didSet {
            lblPostFee.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPostDate: UILabel! {
        didSet {
            lblPostDate.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblPostNote: UILabel! {
        didSet {
            lblPostNote.font = AppTheme.Font.smallMedium
        }
    }
    
    //MARK: -
    @IBOutlet var lblPostMessage: UILabel! {
        didSet {
            lblPostMessage.font = AppTheme.Font.smallMedium
        }
    }
    @IBOutlet var lblPostMessageContent: UILabel! {
        didSet {
            lblPostMessageContent.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblPostTotal: UILabel! {
        didSet {
            lblPostTotal.font = AppTheme.Font.smallMedium
        }
    }
    @IBOutlet var lblPostTotalAmount: UILabel! {
        didSet {
            lblPostTotalAmount.font = AppTheme.Font.normalSemiBold
        }
    }
    
    //MARK: -
    @IBOutlet var lblPayTitle: UILabel! {
        didSet {
            lblPayTitle.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPay: UILabel! {
        didSet {
            lblPay.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPayAmount: UILabel! {
        didSet {
            lblPayAmount.font = AppTheme.Font.normalMedium
        }
    }
    
    //MARK: -
    @IBOutlet var lblPayTotal: UILabel! {
        didSet {
            lblPayTotal.font = AppTheme.Font.smallMedium
        }
    }
    @IBOutlet var lblPayTotalAmount: UILabel! {
        didSet {
            lblPayTotalAmount.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblPayFee: UILabel! {
        didSet {
            lblPayFee.font = AppTheme.Font.smallMedium
        }
    }
    @IBOutlet var lblPayFeeAmount: UILabel! {
        didSet {
            lblPayFeeAmount.font = AppTheme.Font.small
        }
    }
    @IBOutlet var lblPayTotalLast: UILabel! {
        didSet {
            lblPayTotalLast.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblPayTotalLastAmount: UILabel! {
        didSet {
            lblPayTotalLastAmount.font = AppTheme.Font.normalSemiBold
        }
    }
    
    //MARK: -
    @IBOutlet var lblTotal: UILabel! {
        didSet {
            lblTotal.font = AppTheme.Font.normalMedium
        }
    }
    @IBOutlet var lblTotalAmount: UILabel! {
        didSet {
            lblTotalAmount.font = AppTheme.Font.large18SemiBold
        }
    }
    
    //MARK: -
    @IBOutlet weak var btnPaymentType: UIButtonOneClick!
    @IBOutlet weak var btnPayMent: UIButtonOneClick! {
        didSet {
            btnPayMent.titleLabel?.font = AppTheme.Font.normalSemiBold
        }
    }
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var tbvProduct: UITableView! {
        didSet{
            tbvProduct.register(PayMentCell.self)
            tbvProduct.dataSource = self
            tbvProduct.delegate = self
            tbvProduct.rowHeight = UITableView.automaticDimension
            tbvProduct.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 1))
        }
    }
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [btnBack, btnPayMent, btnPaymentType].forEach { (item) in
            item?.delegate = self
        }
        tbvProduct.reloadData()

        if let vm = vm {
            vm.spiderData.next { (value) in
                if let availableBalance = value["availableBalance"] as? String,
                   availableBalance.isNotEmpty, let balance = Int(availableBalance) {
                    self.lblPayAmount.text = FormatHelper.formatCurrency(balance)
                    self.lblPayAmount.textColor = AppTheme.Color.primaryTextColor
                }
            } ~> disposeBag
            
            var count = 0
            vm.getBuyItems().forEach { item in
                count += item.count
            }
            
            lblPostTotal.text = "Tổng số tiền (\(count) sản phẩm)"
            lblPostTotalAmount.text = vm.getTotalAmountDisplay()
            lblPayTotalAmount.text = lblPostTotalAmount.text
            lblPayTotalLastAmount.text = "\(vm.getTotalAmount() + 15000)".currencyToDisplay()
            lblTotalAmount.text = lblPayTotalLastAmount.text
        }
        
        let helper = UserDefaultsHelper.share()
        lblName.text = helper.getString(key: .customerName) + " " + helper.getString(key: .username)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        vm?.getSpiderInfor()
    }
}

extension PayMentVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm?.getBuyItemCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PayMentCell = tableView.dequeueCell()
        if let item = vm?.getBuyItem(index: indexPath.row) {
            cell.set(item)
        }
        return cell
    }
}

extension PayMentVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            if isNow, let vm = vm {
                let items = vm.getBuyItems()
                vm.shopItems.removeAll { item in
                    items.contains { obj in
                        return obj.id == item.id
                    }
                }
                vm.saveShopItems()
            }
            onBackPressed()
            
        case btnPaymentType:
            let vc: PaymentMethodVC = PaymentMethodVC.newInstance()
            if let text = lblPayAmount.text {
                vc.balanceText = text
            }
            self.show(vc, sender: nil)
            
        case btnPayMent:
            if let vm = vm {
                let spiderData = vm.spiderData.value
                if let ekycStatus = spiderData["ekycStatus"] as? String, ekycStatus == "N" {
                    let vc: WalletActivateVC = WalletActivateVC.newInstance()
                    vc.presentedVC = self
                    vc.isPayment = true
                    vc.modalPresentationStyle = .custom
                    self.present(vc, animated: true, completion: nil)
                    return
                }
                if let balance = spiderData["availableBalance"] as? String, vm.getTotalAmount() > balance.numberOnly.nsNumber.intValue {
                    AlertVC.show(message: "Tài khoản của Quý khách không đủ tiền. Vui lòng nạp tiền để thực hiện thanh toán.", isOkHidden: false, onOk: {
                        let helper = UserDefaultsHelper.share().getTicketData()
                        SpiderSDK.gotoDepositNapas(muid: helper.muid, suid: helper.suid, isPayment: true, fromVC: self, sdkCallback: nil)
                    })
                    return
                }
                
                let data = UserDefaultsHelper.share().getTicketData()
                let totalAmount = vm.getTotalAmount() + 15000
                let desc = vm.getBuyItems().map({ return "\($0.name) (\($0.count))" }).joined(separator: "\n")
                print("desc: \(desc)")
                let params: [String: Any] = ["amount": totalAmount, "orderDescription": desc as Any]
                SpiderSDK.payment(muid: data.muid, suid: data.suid, data: params, fromVC: self) { code, message, data in
                    if code == "0" {
                        if let nav = self.navigationController {
                            nav.viewControllers.forEach { vc in
                                if vc is HomeShoppingVC {
                                    nav.popToViewController(vc, animated: true)
                                }
                            }
                        }
                        vm.shopItems.removeAll()
                        vm.saveShopItems()
                    }
                }
            }
            
        default:
            break
        }
    }
}
