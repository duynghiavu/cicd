//
//  TransDetailVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 1/13/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK

class TransDetailVC: BaseVC {
    
    var transId = ""
    var transTime = ""
    var transDesc = ""
    var transAmount = ""
    let vm = ShoppingVM()
    
    @IBOutlet var lblAddressTitle: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    
    @IBOutlet var lblPostSelect: UILabel!
    @IBOutlet var lblPostName: UILabel!
    @IBOutlet var lblPostFee: UILabel!
    @IBOutlet var lblPostDate: UILabel!
    
    @IBOutlet var lblPayTitle: UILabel!
    @IBOutlet var lblPay: UILabel!
    
    @IBOutlet var lblTransIdTitle: UILabel!
    @IBOutlet var lblTransId: UILabel!
    @IBOutlet var lblTransTimeTitle: UILabel!
    @IBOutlet var lblTransTime: UILabel!
    
    @IBOutlet var lblPayTotalLast: UILabel!
    @IBOutlet var lblPayTotalLastAmount: UILabel!
    
    @IBOutlet weak var btnBack: UIButtonOneClick!
    @IBOutlet weak var tbvProduct: UITableView! {
        didSet{
            tbvProduct.register(PayMentCell.self)
            tbvProduct.dataSource = self
            tbvProduct.delegate = self
            tbvProduct.rowHeight = UITableView.automaticDimension
            tbvProduct.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 1))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblName.font = AppTheme.Font.normalBold
        [lblAddressTitle, lblPostFee, lblPay, lblAddress, lblPostSelect, lblPostName, lblPostDate, lblPayTitle,
          lblTransIdTitle, lblTransId, lblTransTimeTitle, lblTransTime, lblPayTotalLast].forEach { (item) in
            item?.font = AppTheme.Font.normalMedium
        }
        
        lblPayTotalLastAmount.font = AppTheme.Font.normalSemiBold
        
        btnBack.delegate = self
        tbvProduct.reloadData()

        lblTransId.text = transId
        lblTransTime.text = transTime
        lblPayTotalLastAmount.text = transAmount
        
        let helper = UserDefaultsHelper.share()
        lblName.text = helper.getString(key: .customerName) + " " + helper.getString(key: .username)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension TransDetailVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.getDetailItem(transDesc).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PayMentCell = tableView.dequeueCell()
        let items = vm.getDetailItem(transDesc)
        if indexPath.row < items.count {
            cell.set(items[indexPath.row])
        }
        return cell
    }
}

extension TransDetailVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        switch button {
        case btnBack:
            onBackPressed()
            
        default:
            break
        }
    }
}
