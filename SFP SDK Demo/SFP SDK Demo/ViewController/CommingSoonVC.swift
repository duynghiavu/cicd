//
//  CommingSoonVC.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 2/28/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit

class CommingSoonVC: BaseVC {
    
    @IBOutlet weak var btnHome: UIButtonOneClick! {
        didSet {
            btnHome.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension CommingSoonVC: UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick) {
        onBackPressed()
    }
}
