//
//  CategoryVM.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 1/28/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CryptoSwift
import SpiderSDK

class ShoppingVM: BaseVM {
    var spiderData = BehaviorRelay<[String: Any]>(value: [:])
    
    var categories = [CategoryModel]()
    var categorySelected: CategoryModel?
    var items = [ProductModel]()
    var searchItems = [ProductModel]()
    var reloaItems = BehaviorRelay<Bool>(value: false)
    
    var searchText = "" {
        didSet {
            let text = searchText.trim().removeVNSigned().lowercased()
            if text.isEmpty {
                searchItems = items
            }
            else {
                searchItems = items.filter({ (item) -> Bool in
                    return item.name.removeVNSigned().lowercased().contains(find: text)
                })
            }
            reloaItems.accept(true)
        }
    }
    
    var shopItems = [ProductModel]()
    var reloaShopItem = BehaviorRelay<Bool>(value: false)
    
    override func bindRX() {
        var category = CategoryModel()
        category.id = "1"
        category.name = "Thực phẩm"
        category.color = #colorLiteral(red: 0.8941176471, green: 0.9529411765, blue: 0.9176470588, alpha: 1)
        category.image = "ic_food"
        categories.append(category)
        
        category = CategoryModel()
        category.id = "2"
        category.name = "Đồ gia dụng"
        category.color = #colorLiteral(red: 1, green: 0.9254901961, blue: 0.9098039216, alpha: 1)
        category.image = "ic_student"
        categories.append(category)
        
        category = CategoryModel()
        category.id = "3"
        category.name = "Mẹ & Bé"
        category.color = #colorLiteral(red: 1, green: 0.9647058824, blue: 0.8941176471, alpha: 1)
        category.image = "ic_baby"
        categories.append(category)
        
        category = CategoryModel()
        category.id = "4"
        category.name = "Thiết bị điện tử"
        category.color = #colorLiteral(red: 0.9450980392, green: 0.9294117647, blue: 0.9882352941, alpha: 1)
        category.image = "ic_mobile"
        categories.append(category)
        
//        category = CategoryModel()
//        category.id = "5"
//        category.name = "Quà tặng"
//        category.color = #colorLiteral(red: 0.8941176471, green: 0.9529411765, blue: 0.9176470588, alpha: 1)
//        category.image = "ic_gift"
//        categories.append(category)
        
        //MARK: -
        var item = ProductModel()
        item.id = "1"
        item.name = "TMA-2 HD Wireless"
        item.desc = "Bộ loa có màng loa được gia công chính xác từ cellulose sinh học của NAC Audio, làm cho nó cứng hơn, nhẹ hơn và mạnh hơn các đơn vị loa PET thông thường, đồng thời cho phép màng loa tạo ra âm thanh rung động mà không bị méo tiếng ở các loa khác."
        item.categoryId = "4"
        item.amount = 3500000
        item.amountOld = 4000000
        item.buyCount = 1000
        item.images = ["item_1"]
        item.shopName = "Shop a"
        item.maxSell = 10
        item.sold = 9
        items.append(item)
        
        item = ProductModel()
        item.id = "2"
        item.name = "Khoan tay KTM"
        item.desc = "Khoan tay KTM được thiết kế với nhiều dụng cụ đi kèm nhằm hỗ trợ tối đa cho nhu cầu sử dụng của người tiêu dùng. Máy có công suất hoạt động lớn cùng mũi khoan được làm từ chất liệu cao cấp giúp bề mặt được xử lý nhanh chóng cùng với màu xanh dương mang đến cảm giác thoải mái và dịu nhẹ cho người sử dụng."
        item.categoryId = "2"
        item.amount = 500000
        item.amountOld = 600000
        item.buyCount = 1300
        item.images = ["item_2", "item_2"]
        item.shopName = "Shop a"
        item.maxSell = 10
        item.sold = 0
        items.append(item)
        
        item = ProductModel()
        item.id = "3"
        item.name = "Gấu Dooner"
        item.desc = "- Ôm ngủ, gác chân siêu phê. Cùng bạn nằm cày phim, đọc truyện hoặc mang tới văn phòng thì còn gì bằng\n- Màu lông nâu sạch và sang trọng.\n- Thiết kế kích thước siêu to béo vừa làm đẹp căn phòng vừa ôm và từa vô cùng thoải mái."
        item.categoryId = "3"
        item.amount = 45999
        item.amountOld = 50000
        item.buyCount = 12000
        item.images = ["item_3", "item_3", "item_3"]
        item.shopName = "Shop b"
        item.maxSell = 10
        item.sold = 7
        items.append(item)
        
        item = ProductModel()
        item.id = "4"
        item.name = "Oto Bibomama"
        item.desc = "- Màu sắc đa dạng, đẹp\n- Nhỏ gọn bé có thể mang theo đi bất cứ đâu\n- Không sắc nhọn, an toàn với trẻ nhỏ\n- Phù hợp với các bé nhỏ tuổi\n- Chất liệu nhựa an toàn cho bé"
        item.categoryId = "3"
        item.amount = 99999
        item.amountOld = 120000
        item.buyCount = 2900
        item.images = ["item_4"]
        item.shopName = "Shop b"
        item.maxSell = 10
        item.sold = 3
        items.append(item)
        
        item = ProductModel()
        item.id = "5"
        item.name = "BUZZ LIGHTYEAR"
        item.desc = "- cảnh sát vũ trụ trong series TOY STORY\n- Chính hãng Disney Store\n- Tỉ lệ 1:1 với nhân vật thật trong phim (cao 30cm)\n- Mô hình cử động các khớp linh hoạt\n- Có nút bấm mở cánh, mũ, laze và nói thoại (2 nút)"
        item.categoryId = "3"
        item.amount = 56900
        item.amountOld = 60000
        item.buyCount = 3000
        item.images = ["item_5", "item_5"]
        item.shopName = "Shop b"
        item.maxSell = 10
        item.sold = 9
        items.append(item)
        
        item = ProductModel()
        item.id = "6"
        item.name = "Anchor"
        item.desc = "Whipping Cream của cửa hàng Hifood thuộc thương hiệu Anchor, được nhập khẩu trực tiếp từ New Zealand. Whipping Cream có thể được sử dụng như topping cream, để làm bánh, làm smoothie, làm kem trộn hoa quả…"
        item.categoryId = "1"
        item.amount = 289000
        item.amountOld = 300000
        item.buyCount = 1300
        item.images = ["item_6", "item_6", "item_6"]
        item.shopName = "Shop c"
        item.maxSell = 10
        item.sold = 0
        items.append(item)
        
        item = ProductModel()
        item.id = "7"
        item.name = "Mũ rơm"
        item.desc = "Phù hợp: đầu trẻ em, thanh niên, người trưởng thành!\nNón thoáng dễ dùng\nThích hợp đội trong nhiều hoàn cảnh"
        item.categoryId = "3"
        item.amount = 30000
        item.amountOld = 50000
        item.buyCount = 4550
        item.images = ["item_7"]
        item.shopName = "Shop c"
        item.maxSell = 10
        item.sold = 5
        items.append(item)
        
        item = ProductModel()
        item.id = "8"
        item.name = "Ghế vàng"
        item.desc = "Mô tả sản phẩm - Phối màu trang nhã , phù hợp với trẻ nhỏ - Thiết kế ghế cứng cáp, vững chắc và an toàn - Dễ dàng vệ sinh ghế - Khả năng chịu va đập hiệu quả, bền chắc và chịu lực tốt - Bề mặt sản phẩm nhẵn mịn, không có góc cạnh, không lo trầy xước da bé - Tạo hứng thú cho bé khi học tập cũng như chơi đùa - Có thể xếp gọn để mang theo dễ dàng."
        item.categoryId = "2"
        item.amount = 90000
        item.amountOld = 100000
        item.buyCount = 15900
        item.images = ["item_8"]
        item.shopName = "Shop c"
        item.maxSell = 10
        item.sold = 0
        items.append(item)
        
        searchText = ""
        
        UserDefaultsHelper.share().getShopIds().forEach { obj in
            if let item = items.filter({ $0.id == obj.id }).first {
                item.count = obj.count
                shopItems.append(item)
            }
        }
    }
    
    ///
    func getSpiderInfor() {
        let transData = UserDefaultsHelper.share().getTicketData()
        isLoading.accept(true)
        SpiderSDK.getUserInfo(muid: transData.muid, suid: transData.suid) { code, message, data in
            if code == "0", let data = data as? [String: Any] {
                self.spiderData.accept(data)
            }
            self.isLoading.accept(false)
        }
    }
    
    ///
    func getCategoryItems() -> [ProductModel] {
        guard let category = categorySelected else { return searchItems }
        let result = searchItems.filter { (s) -> Bool in
            return s.categoryId == category.id
        }
        return result.sorted { (s1, s2) -> Bool in
            return s1.name < s2.name
        }
    }
    
    func getShops() -> [String] {
        var result = [String]()
        shopItems.forEach { (item) in
            if !result.contains(item.shopName) {
                result.append(item.shopName)
            }
        }
        return result.sorted()
    }
    
    func getShopCount() -> Int {
        return getShops().count
    }
    
    func getShopName(_ section: Int) -> String {
        let shops = getShops()
        if section < shops.count {
            return shops[section]
        }
        return ""
    }
    
    func getShopItems(_ section: Int) -> [ProductModel] {
        let shopName = getShopName(section)
        let result = shopItems.filter { (s) -> Bool in
            return s.shopName == shopName
        }
        return result.sorted { (s1, s2) -> Bool in
            return s1.name < s2.name
        }
    }
    
    func getShopItemCount(_ section: Int) -> Int {
        return getShopItems(section).count
    }
    
    func getShopItem(_ indexPath: IndexPath) -> ProductModel? {
        let shopItems = getShopItems(indexPath.section)
        if indexPath.row < shopItems.count {
            return shopItems[indexPath.row]
        }
        return nil
    }
    
    func getBuyItems() -> [ProductModel] {
        let result = shopItems.filter { (s) -> Bool in
            return s.isChecked
        }
        return result.sorted { (s1, s2) -> Bool in
            return s1.name < s2.name
        }
    }
    
    func getBuyItemCount() -> Int {
        shopItems.filter { (s) -> Bool in
            return s.isChecked
        }
        return getBuyItems().count
    }
    
    func getBuyItem(index: Int) -> ProductModel? {
        let buyItems = getBuyItems()
        if index < buyItems.count {
            return buyItems[index]
        }
        return nil
    }
    
    func removeBuy(item: ProductModel) {
        for (index, obj) in shopItems.enumerated() {
            if obj.id == item.id {
                shopItems.remove(at: index)
            }
        }
        saveShopItems()
    }
    
    func getTotalAmount() -> Int {
        var amount = 0
        shopItems.forEach { (item) in
            if item.isChecked {
                amount += item.totalAmount()
            }
        }
        return amount
    }
    
    func getTotalAmountDisplay() -> String {
        return "\(getTotalAmount())".currencyToDisplay()
    }
    
    func getDetailItem(_ desc: String) -> [ProductModel] {
        let strs = desc.components(separatedBy: .newlines)
        var result = [ProductModel]()
        strs.forEach { str in
            let strs = str.components(separatedBy: " (")
            let name = strs.first
            let count = strs.last?.numberOnly.nsNumber.intValue ?? 1
            if let item = items.filter({$0.name == name}).first {
                item.count = count
                result.append(item)
            }
        }
        return result.sorted { (s1, s2) -> Bool in
            return s1.name < s2.name
        }
    }
    
    func saveShopItems() {
        UserDefaultsHelper.share().setShopIds( shopItems )
    }
}
