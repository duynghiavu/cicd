//
//  FilmVM.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 2/23/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CryptoSwift

class FilmVM: BaseVM {
    var banner = [String]()
    var reloadBanner = BehaviorRelay<Bool>(value: false)
    
    var categorys = [FilmCategoryModel]()
    var reloadCategorys = BehaviorRelay<Bool>(value: false)
    
    var searchNear = [String]()
    var reloadSearchNear = BehaviorRelay<Bool>(value: false)
    
    var searchQuick = [String]()
    var reloadSearchQuick = BehaviorRelay<Bool>(value: false)
    
    var films = [FilmModel]()
    var searchFilms = [FilmModel]()
    var reloadSearchFilms = BehaviorRelay<Bool>(value: false)
    
    var searchText = "" {
        didSet {
            let text = searchText.trim().removeVNSigned().lowercased()
            searchFilms = films.filter({ (item) -> Bool in
                return item.name.trim().removeVNSigned().lowercased().contains(find: text)
            })
            
            reloadSearchFilms.accept(true)
        }
    }
    
    override func bindRX() {
        banner.append("banner_1")
        banner.append("banner_2")
        banner.append("banner_3")
        banner.append("banner_4")
        banner.append("banner_5")
        reloadBanner.accept(true)
        
        ///
        var film = FilmModel()
        film.id = "1"
        film.name = "SALT"
        film.image = "film_1"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "2"
        film.name = "RUDY"
        film.image = "film_2"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "3"
        film.name = "Aladin"
        film.image = "film_3"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "4"
        film.name = "Iron Man"
        film.image = "film_4"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "5"
        film.name = "Wild"
        film.image = "film_5"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "6"
        film.name = "Archer"
        film.image = "film_6"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "7"
        film.name = "Brain"
        film.image = "film_7"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "8"
        film.name = "Amazon"
        film.image = "film_8"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "9"
        film.name = "Parasite"
        film.image = "film_9"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        film = FilmModel()
        film.id = "10"
        film.name = "Titanic"
        film.image = "film_10"
        film.rate = "3.5 sao"
        film.amount = "59.000đ"
        film.createAt = "2015"
        film.author = "Nguyễn Văn A"
        film.duration = "2:15"
        films.append(film)
        
        ///
        var item = FilmCategoryModel()
        item.id = "1"
        item.name = "PHIM MIỄN PHÍ"
        item.films = films
        categorys.append(item)
        
        ///
        item = FilmCategoryModel()
        item.id = "2"
        item.name = "PHIM BOM TẤN"
        for i in 0..<films.count/2 {
            item.films.append(films[i])
        }
        categorys.append(item)
        
        ///
        item = FilmCategoryModel()
        item.id = "3"
        item.name = "PHIM KINH DỊ"
        for i in films.count/2..<films.count {
            item.films.append(films[i])
        }
        categorys.append(item)
        
        ///
        item = FilmCategoryModel()
        item.id = "4"
        item.name = "PHIM HÀI"
        for i in 0..<films.count {
            if i%2 == 0 {
                item.films.append(films[i])
            }
        }
        categorys.append(item)
        
        ///
        item = FilmCategoryModel()
        item.id = "5"
        item.name = "PHIM HÓT"
        for i in 0..<films.count {
            if i%2 != 0 {
                item.films.append(films[i])
            }
        }
        categorys.append(item)
        reloadCategorys.accept(true)
        
        ///
        searchNear = UserDefaultsHelper.share().getString(key: .searchFilm).components(separatedBy: "|")
        reloadSearchNear.accept(true)
        
        ///
        searchQuick = ["Hoat hinh", "Phim", "Free", "Hành động", "Viễn tưởng"]
        reloadSearchQuick.accept(true)
        
        searchText = ""
    }
    
    func addSearchNear() {
        if searchText.count > 0 {
            for text in searchNear {
                if text == searchText {
                    let _ = searchNear.remove(text)
                    break
                }
            }
            searchNear.insert(searchText, at: 0)
            UserDefaultsHelper.share().set(value: searchNear.joined(separator: "|"), key: .searchFilm)
            reloadSearchNear.accept(true)
        }
    }
    
    func clearSearchNear() {
        UserDefaultsHelper.share().remove(key: .searchFilm)
        searchNear = []
        reloadSearchNear.accept(true)
    }
}
