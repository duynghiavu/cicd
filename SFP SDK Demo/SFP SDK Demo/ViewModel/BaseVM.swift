//
//  BaseVM.swift
//  VoiceRec
//
//  Created by Nghia Vu on 10/12/19.
//  Copyright © 2019 Thien Minh. All rights reserved.
//

import RxSwift
import RxCocoa

class BaseVM: NSObject {
    let disposeBag = DisposeBag()
    
    var isLoading = BehaviorRelay<Bool>(value: false)
    
    override init() {
        super.init()
        
        bindRX()
    }
    
    func bindRX() {
    }
}
