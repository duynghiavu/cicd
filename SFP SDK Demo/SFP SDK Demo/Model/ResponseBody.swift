//
//  ResponseBody.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 7/13/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation

class ResponseBody {
    var resultCode: String?
    var resultDesc: String?
    
    init(_ data: [String: Any]) {
        resultCode = data.string(of: "resultCode")
        resultDesc = data.string(of: "resultDesc")
    }
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["resultCode"] = resultCode
        json["resultDesc"] = resultDesc
        
        return json
    }
}
