//
//  RegisterData.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/2/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation

class RegisterData: ResponseBody {
    
    var customerName = ""
    var merchantCode = ""
    
    override init(_ data: [String: Any]) {
        super.init(data)
        
        customerName = data.string(of: "customerName")
        merchantCode = data.string(of: "merchantCode")
    }
    
    override func toJson() -> [String: Any] {
        var json = super.toJson()
        
        json["customerName"] = customerName
        json["merchantCode"] = merchantCode
        
        return json
    }
}
