//
//  LoginRequest.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/2/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation

class LoginRequest {
    var username = ""
    var password = ""
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["username"] = username
        json["password"] = password
        
        return json
    }
}
