//
//  RegisterRequest.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/2/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation

class RegisterRequest {
    var customerName = ""
    var customerPhone = ""
    var password = ""
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["customerName"] = customerName
        json["customerPhone"] = customerPhone
        json["password"] = password
        
        return json
    }
}
