//
//  ProductModel.swift
//  SFP SDK Demo
//
//  Created by Admin on 3/13/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation
import UIKit

class ShopItemModel {
    var id = ""
    var count = 1
    
    init(_ data: [String: Any] = [:]) {
        id = data.string(of: "id")
        count = data.int(of: "count", defaultValue: 1)
    }
    
    //MARK: -
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["id"] = id
        json["count"] = count
        
        return json
    }
    
    class func toListJson(_ array: [ShopItemModel]) -> [[String: Any]] {
        var arrayJson = [[String: Any]]()
        array.forEach { (item) in
            arrayJson.append(item.toJson())
        }
        return arrayJson
    }
}

class ProductModel: ShopItemModel {
    var name = ""
    var desc = ""
    var images = [String]()
    var categoryId = ""
    var buyCount = 0
    var amount = 0
    var amountOld = 0
    var shopName = ""
    var rate = "4.6"
    var viewCount = "86"
    
    var maxSell = 0
    var sold = 0
    
    var isChecked = false {
        didSet {
            onCheckChange?(isChecked)
        }
    }
    
    var onRemove: (() -> Void)?
    var onCheckChange: ((Bool) -> Void)?
    var onAmountDisplay: ((String, String, String) -> Void)?
    
    func addCount(count: Int) {
        self.count += count
        if self.count < 1 {
            self.count = 1
            onRemove?()
        }
        onAmountDisplay?("\(self.count)", totalAmountDisPlay(), totalOldAmountDisPlay())
    }
    
    func totalAmount() -> Int {
        return count * amount
    }
    
    func totalOldAmount() -> Int {
        return count * amountOld
    }
    
    func amountDisPlay() -> String {
        return "\(amount)".currencyToDisplay()
    }
    
    func amountOldDisPlay() -> String {
        return "\(amountOld)".currencyToDisplay()
    }
    
    func totalAmountDisPlay() -> String {
        return "\(totalAmount())".currencyToDisplay()
    }
    
    func totalOldAmountDisPlay() -> String {
        return "\(totalOldAmount())".currencyToDisplay()
    }
    
    func buyCountDisPlay() -> String {
        if buyCount > 999 {
            return String(format: "Đã bán %0.1fk", CGFloat(buyCount) / 1000)
        }
        return "Đã bán \(buyCount)k"
    }
    
    func discount() -> String {
        let d = amountOld - amount
        let p = d * 100 / amountOld
        return "-\(p)%"
    }
}
