//
//  CreateTransactionData.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 7/13/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation

class GetTicketData: ResponseBody {
    var type = ""
    var merchantId = ""
    var appId = ""
    var suid = ""
    var muid = ""
    var t2 = ""
    var ticket = ""
    var merchantTicketHash = ""
    var merchantSignature = ""
    var merchantTxnRef = ""
    
    override init(_ data: [String: Any]) {
        super.init(data)
        
        type = data.string(of: "type")
        merchantId = data.string(of: "merchantId")
        appId = data.string(of: "appId")
        suid = data.string(of: "suid")
        muid = data.string(of: "muid")
        t2 = data.string(of: "t2")
        ticket = data.string(of: "ticket")
        merchantTicketHash = data.string(of: "merchantTicketHash")
        merchantSignature = data.string(of: "merchantSignature")
        merchantTxnRef = data.string(of: "merchantTxnRef")
    }
    
    override func toJson() -> [String: Any] {
        var json = super.toJson()
        
        json["type"] = type
        json["merchantId"] = merchantId
        json["appId"] = appId
        json["suid"] = suid
        json["muid"] = muid
        json["t2"] = t2
        json["ticket"] = ticket
        json["merchantTicketHash"] = merchantTicketHash
        json["merchantSignature"] = merchantSignature
        json["merchantTxnRef"] = merchantTxnRef
        
        return json
    }
}
