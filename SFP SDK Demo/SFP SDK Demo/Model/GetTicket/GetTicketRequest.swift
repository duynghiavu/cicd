//
//  CreateTransactionRequest.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 7/13/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation

class GetTicketRequest {
    var type = ""
    var username = ""
    var params = [String: Any]()
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["type"] = type
        json["username"] = username
        params.keys.forEach { key in
            json[key] = params[key]
        }
        
        return json
    }
}
