//
//  AddressModel.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 3/3/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

class AddressModel {
    
    init(_ pm: CLPlacemark) {
        var places = [String]()
        
        if let subThoroughfare = pm.subThoroughfare {
            places.append(subThoroughfare)
        }
        if let thoroughfare = pm.thoroughfare {
            places.append(thoroughfare)
        }
        if let subLocality = pm.subLocality {
            places.append(subLocality)
        }
        if let locality = pm.locality {
            places.append(locality)
        }
        if let country = pm.country {
            places.append(country)
        }
        
        lines = places.joined(separator: ", ")
        coordinate = pm.location?.coordinate
    }
    
    init(_ address: GMSAddress) {
        
        if let lines = address.lines {
            self.lines = lines.joined(separator: ", ")
        }
        coordinate = address.coordinate
    }
    
    init(_ json: [String: Any]) {
        
        lines = json.string(of: "lines")
        coordinate = CLLocationCoordinate2D(latitude: json.double(of: "lat"), longitude: json.double(of: "long"))
    }
    
    var lines = ""
    var coordinate: CLLocationCoordinate2D?
    
    func toJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["lines"] = lines
        json["lat"] = coordinate?.latitude ?? 0
        json["long"] = coordinate?.longitude ?? 0
        return json
    }
    
    static func listAddress(_ array: [Any]) -> [AddressModel] {
        var list = [AddressModel]()
        array.forEach { (item) in
            if let dic = item as? [String: Any] {
                list.append(AddressModel(dic))
            }
        }
        return list
    }
}
