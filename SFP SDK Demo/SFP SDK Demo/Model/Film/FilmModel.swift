//
//  FilmModel.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 2/23/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation

class FilmModel {
    var id = ""
    var name = ""
    var image = ""
    var rate = ""
    var amount = ""
    var createAt = ""
    var author = ""
    var duration = ""
}

class FilmCategoryModel {
    var id = ""
    var name = ""
    var films = [FilmModel]()
    var searchFilms = [FilmModel]()
    
    var searchText = "" {
        didSet {
            let text = searchText.trim().removeVNSigned().lowercased()
            if text.isEmpty {
                searchFilms = films
            }
            else {
                searchFilms = films.filter({ (item) -> Bool in
                    return item.name.removeVNSigned().lowercased().contains(find: text)
                })
            }
        }
    }
}
