//
//  CategoryModel.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/8/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation
import UIKit

class CategoryModel {
    var id = ""
    var name = ""
    var image = ""
    var color = UIColor.black
}
