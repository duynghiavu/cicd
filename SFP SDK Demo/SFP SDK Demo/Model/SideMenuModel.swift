//
//  SideMenuModel.swift
//  SFP SDK Demo
//
//  Created by Admin on 3/17/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation

class SideMenuModel {
    var id = ""
    var name = ""
    var image = ""
}
