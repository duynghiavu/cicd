//
//  ServiceAPI.swift
//  
//
//  Created by NghiaVD on 7/31/17.
//
//

import UIKit
import AFNetworking
import NetworkExtension
import SpiderSDK
import CoreLocation

enum MethodAPI: String {
    case get = "GET"
    case post = "POST"
}

class ServiceAPI: NSObject {
    static let googleAPI = "AIzaSyA8ssVwu6Ud4HPaZL47p3uRI4tql9EIbwc"
    static let LinkServer = "https://spider-test.lienviettech.vn"
    static let LinkAPIURL = LinkServer + "/sfp-merchant/sfp-merchant-emulator/"
    static let timeOut: Double = 30
    
    ////////////////////
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    //MARK: - Show alertView
    static func showAlertView(content: String){
        AlertVC.show(message: content)
    }
    
    //MARK: -  Convert from NSData to json object
    static func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    static func stringToJSON(string: String) -> Any? {
        return dataToJSON(data: string.data(using: .utf8)!)
    }
    
    // Convert from JSON to nsdata
    static func jsonToData(json: Any) -> Data?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
    static func jsonToString(json: Any) -> String?{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            var convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            convertedString = convertedString?.replacingOccurrences(of: " ", with: "")
            convertedString = convertedString?.replacingOccurrences(of: "\n", with: "")
            convertedString = convertedString?.replacingOccurrences(of: "\r", with: "")
            print(convertedString!) // <-- here is ur string
            return convertedString
            
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    static func invoke(method: MethodAPI, url: String, params: [String: Any], completeHandle:@escaping(Bool, Any?)  -> Void){
        
        guard isConnectedToNetwork() else {
            showAlertView(content: "Quý khách vui lòng kiểm tra lại kết nối mạng & thử lại lần nữa")
            return
        }
        
        do {
            let request = try AFJSONRequestSerializer(writingOptions: JSONSerialization.WritingOptions.prettyPrinted).request(withMethod: method.rawValue, urlString: url, parameters: params)
            request.allHTTPHeaderFields!["Content-Type"] = "application/json"
            request.allHTTPHeaderFields!["Accept"] = "application/json"
            request.timeoutInterval = timeOut
            LogHelper.log(request.url!.absoluteString)
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.dataTask(with: request as URLRequest, uploadProgress: nil, downloadProgress: nil, completionHandler: { (response, data, error) in
                if error == nil{
                    var dic = data
                    if let data = data as? Data {
                        if let data = dataToJSON(data: data){
                            dic = data
                        }
                        else if let data = String(data: data, encoding: .utf8){
                            dic = data
                        }
                        else if let data = String(data: data, encoding: .ascii){
                            dic = data
                        }
                    }
                    
                    LogHelper.log(dic)
                    completeHandle(true, dic)
                }
                else{
                    showAlertView(content: error!.localizedDescription)
                    completeHandle(false, nil)
                }
            }).resume()
        } catch _ {
            completeHandle(false, nil)
        }
    }
    
    static func post(url: String, params: [String: Any], completeHandle:@escaping(Bool, [String: Any]?, String, String)  -> Void){
        if !isConnectedToNetwork() {
            AlertVC.show(message: "Quý khách vui lòng kiểm tra lại kết nối mạng & thử lại lần nữa")
            completeHandle(false, nil, "NETWORK", "")
            return
        }
        
        var message = "Dịch vụ không sẵn sàng, Quý khách vui lòng thử lại sau ít phút."
        do {
            let request = try AFJSONRequestSerializer(writingOptions: JSONSerialization.WritingOptions.prettyPrinted).request(withMethod: "POST", urlString: url, parameters: params)
            request.allHTTPHeaderFields!["Content-Type"] = "application/json"
            request.allHTTPHeaderFields!["Accept"] = "application/json"
            
            print("/////////////////////////////")
            print(request.url!.absoluteString)
            print(request.allHTTPHeaderFields!)
            print(params)

            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.dataTask(with: request as URLRequest, uploadProgress: nil, downloadProgress: nil, completionHandler: { (response, data, error) in
                if let httpResponse = response as? HTTPURLResponse {
                    if error == nil{
                        var dic = data
                        if let data = data as? Data {
                            if let data = dataToJSON(data: data){
                                dic = data
                            }
                            else if let data = String(data: data, encoding: .utf8){
                                dic = data
                            }
                            else if let data = String(data: data, encoding: .ascii){
                                dic = data
                            }
                        }
                        completeHandle(true, dic as! [String : Any], "HTTP-\(httpResponse.statusCode)", "")
                    }
                    else{
                        message = error!.localizedDescription
                        showAlertView(content: message)
                        completeHandle(false, nil, "HTTP-\(httpResponse.statusCode)", message)
                    }
                }
                else {
                    showAlertView(content: message)
                    completeHandle(false, nil, "UNEXPECTED", message)
                }
            }).resume()
        } catch _ {
            showAlertView(content: message)
            completeHandle(false, nil, "UNEXPECTED", message)
        }
    }
    
    //MARK: -
    static func login(model: LoginRequest, completeHandle:@escaping(Bool, RegisterData) -> Void){
        post(url: LinkAPIURL + "login", params: model.toJson()) { (success, data, _, _) in
            let responseData = RegisterData(data ?? [:])
            if success {
                if responseData.resultCode == "0", responseData.merchantCode.isNotEmpty {
                    completeHandle(true, responseData)
                    return
                }
                
                var message = responseData.resultDesc
                if message.isEmpty {
                    message = "Đăng nhập thất bại!"
                }
                AlertVC.show(message: message)
            }
            completeHandle(false, responseData)
        }
    }
    
    static func register(model: RegisterRequest, completeHandle:@escaping(Bool, RegisterData) -> Void){
        post(url: LinkAPIURL + "register", params: model.toJson()) { (success, data, _, _) in
            let responseData = RegisterData(data ?? [:])
            if success {
                if responseData.resultCode == "0", responseData.merchantCode.isNotEmpty {
                    completeHandle(true, responseData)
                    return
                }
                
                var message = responseData.resultDesc
                if message.isEmpty {
                    message = "Đăng ký thất bại!"
                }
                AlertVC.show(message: message)
            }
            completeHandle(false, responseData)
        }
    }
    
    //MARK: -
    static func getTicket(model: GetTicketRequest, completeHandle:@escaping(Bool, GetTicketData, String, String) -> Void){
        post(url: LinkAPIURL + "get-ticket", params: model.toJson()) { (success, data, code, message) in
            let responseData = GetTicketData(data ?? [:])
            if success {
                if success, responseData.resultCode == "0" {
                    completeHandle(true, responseData, responseData.resultCode ?? "", responseData.resultDesc ?? "")
                    return
                }
                
                var message = responseData.resultDesc ?? ""
                if message.isEmpty {
                    message = "Lấy vé thất bại!"
                }
                AlertVC.show(message: message)
                completeHandle(false, responseData, responseData.resultCode ?? "", message)
            }
            else {
                completeHandle(false, responseData, code, message)
            }
        }
    }
    
    //MARK: -
    static func directions(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completeHandle:@escaping(Bool, Any?) -> Void){
        let url = "https://maps.googleapis.com/maps/api/directions/json"
        var params = [String: Any]()
        params["origin"] = "\(origin.latitude),\(origin.longitude)"
        params["destination"] = "\(destination.latitude),\(destination.longitude)"
        params["mode"] = "driving"
        params["key"] = ServiceAPI.googleAPI
        
        invoke(method: .get, url: url, params: params) { (complete, data) in
            completeHandle(complete, data)
        }
    }
}
