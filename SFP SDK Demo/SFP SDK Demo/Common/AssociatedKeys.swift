//
//  AssociatedKeys.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 10/5/18.
//

import AFNetworking

struct AssociatedKeys {
  static var binder: UInt8 = 0
  static var activeDownloadRecipt: AFImageDownloadReceipt? = nil
}
