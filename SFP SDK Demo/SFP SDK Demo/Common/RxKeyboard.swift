//
//  RxKeyboard.swift
//  ViViet
//
//  Created by Long Hoang Giang on 7/31/19.
//

import Foundation
import RxSwift
import RxCocoa

class RxKeyboard {
  
  static let shared: RxKeyboard = RxKeyboard()
  let disposeBag = DisposeBag()
  let willShow = PublishRelay<CGRect>()
  let didShow = PublishRelay<CGRect>()
  let willHide = PublishRelay<()>()
  let didHide = PublishRelay<()>()
  let height = PublishRelay<CGFloat>()
  private(set) var currentHeight: CGFloat = 0
  private(set) var kbRect: CGRect = .zero
  
  init() {
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .keyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: .keyboardDidShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .keyboardWillHide, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: .keyboardDidHide, object: nil)
  }
  
  @objc private func keyboardWillShow(_ notification: Notification) {
    guard let userInfo = notification.userInfo else { return }
    let kbRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
    self.kbRect = kbRect
    currentHeight = kbRect.size.height
    willShow.accept(kbRect)
    height.accept(kbRect.size.height)
  }
  
  @objc private func keyboardDidShow(_ notification: Notification) {
    didShow.accept(kbRect)
  }
  
  @objc private func keyboardWillHide(_ notification: Notification) {
    kbRect = .zero
    currentHeight = 0
    willHide.accept(())
    height.accept(0)
  }
  
  @objc private func keyboardDidHide(_ notification: Notification) {
    didHide.accept(())
  }
  
}
