//
//  AppConstant.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 5/7/18.
//

import Foundation
import UIKit
import CoreLocation

struct AppConstant {
   
   struct Service {
      static let defaultTimeout: Int = 200
      static let transactionTimeout: Int = 200
      static let loginTimeout: Int = 30
   }
   
   struct ResultCode {
     static let success = "0"
     static let notFound = "NotFound"
     static let userNotFound = "ESM-1003"
     static let wrongOTPContinue = "OTP_FAIL_INPUT"
     static let wrongPassTooMuch = "OTP_PASS_FAIL_OVER_INPUT"
     static let mPassNotCorrect = "ESM-3014"
     static let wrongOTPTooMuch = "OTP_FAIL_OVER_INPUT"
     static let tokenNotMatch = "ESM-1046"
     static let otpSuccess = "OTP_SUCCESS_INPUT"
     static let addCustomerSuccess = "EWL-0001"
     static let passwordNotCorrect = "ESM-1004"
     static let logoutCodes = ["ESM-9002", "ESM-9003", "ESM-9004", "ESM-9008", "ESM-0009", "ESM-4006", "ESM-4007", "ESM-4008",
                               "ESM-9016", "ESM-4011", "ESM-4013", "ESM-4017", "ESM-1003", "ESM-1007", "ESM-1008",
                               "ESM-1009", "ESM-2013", "ESM-1011", "ESM-1012", "ESM-0084", "AUT-1004", wrongPassTooMuch]
     // ESM-4015: là mã báo user chưa có ví, chức năng chuyển tiền tới ví vẫn cho phép chuyển tới số ko có ví
     static let goHomeCodes = ["EWL-4026","EWL-4027","EWL-4003","EWL-4004","EWL-4005","EWL-4006","EWL-4008","EWL-4009","EWL-4011","EWL-4014","EWL-4016",
                               "ESM-4016","ESM-4027","ESM-9992","ESM-9996",
                               "BNK-13","BNK-15","BNK-21","BNK-24","GW-9000","CONNECT-1999", "ESM-9001"]
     static let publicKeyExpired = "DA-0006-EXPR"
     static let publicKeyNotFound = "EMPTY"
     static let publicKeyNeedRecreate = "DA-0011-SAME"
     static let publicKeyNotCorrectSameDeviceId = "DA-0004"
     static let flagInvalidPublicKey: [String] = [publicKeyExpired, publicKeyNotFound, publicKeyNeedRecreate, publicKeyNotCorrectSameDeviceId]
     static let publicKeyAlreadyExists = "DA-0011"
     static let eKYCProcessing = "IDTF-0013"
     static let cardLocked = "ESM-4082"
     static let resendOTPTooMuch = "OTP_OVER_RESEND"
     static let vvRecipientNotExist = "ESM-4015"
     static let lpbAccInvalid = "ESM-3004"
     static let notRegistered = "ESM-4028"
     static let overBalanceLimitNotVerifyAcc = "EWL-4033"
     static let loginInfoNotMatch = "ESM-1004"
     static let loginInfoNotMatchTmpLock = "ESM-1005"
     static let needChangePassword = "ESM-2040"
   }
   
   struct PrefKey {
      static let lastLoadAllProductList = "pref_lastLoadAllProductList"
      static let lastLoadUsualProductList = "pref_lastLoadUsualProductList"
      static let lastLoadBanner = "pref_lastLoadBanner"
      static let lastLoadMobileConfig = "pref_lastLoadMobileConfig"
      static let rememberUserInfo = "pref_rememberUserInfo"
      static let pushUsername = "pref_pushUsername"
      static let lastLoadHomeCategory = "pref_lastLoadHomeCategory"
      static let lastLoadMapCategory = "pref_lastLoadMapCategory"
      static let isFirstLogin = "pref_isFirstLogin"
      static let showPopupUpdateEmail = "pref_showPopupUpdateEmail"
      static let userNameWithBiometric = "pref_usernameWithBiometric"
      static let needRegisterFinger = "pref_needRegisterFinger"
      static let ringNumber = "pref_ringNumber"
      static let publicKeyState = "pref_publicKeyState"
      static let eKYC_onSuccess = "eKYC_onSuccess"
      static let eKYC_onFailure = "eKYC_onFailure"
      static let isLoginNewDevice = "isLoginNewDevice"
      static let lastLoadBankList = "pref_lastLoadBankList"
      static let firstLaunch = "pref_firstLaunch"
      static let fcmCurrentToken = "pref_currentFCMToken"
      static let fcmCurrentTokenInServer = "pref_currentFCMTokenInServer"
      static let biometricCurrentStatement = "pref_biometricCurrentStatement"
      static let isTrackingLocation = "pref_isTrackingLocation"
   }
   
   struct CacheTime {
      static let loadMobileConfig: Double = 60.0
      static let loadHomeCategory: Double = 60.0
      static let lastLoadMapCategory: Double = 60.0
      static let loadBanner: Double = 60.0
      static let productCacheTime: Double = 60.0
      static let usualProductCacheTime: Double = 60.0
   }
   
   struct OtpTimer {
      static let defaultTimeout: Int = 120
      static let defaultMCodeTimeout: Int = 120
   }
   
   struct Tag {
      static let leftMenuLogin = "leftMenuLogin"
      static let leftMenuUserInfo = "leftMenuUserInfo"
      static let leftMenuRegister = "leftMenuRegister"
      static let editCommonService = "OPEN_EDIT_VC"
      static let sessionTimeoutAlert = "sessionTimeoutAlert"
      static let otpUpdatePublicKey = "UPDATE_PUBLIC_KEY"
      static let updateAlert = "updateAlert"
   }
   
   struct Identifier {
      static let BiometricSettingVC = "BiometricSettingVC"
      static let SearchContactCell = "SearchContactCell"
   }
   
   struct MobileConfig {
      
      struct Code {
         static let session = "SS_TIMEOUT"
         static let share = "UIShare"
      }
      
      struct Name {
         static let session = "sessionTimeout"
         static let shareText = "shareText"
      }
   }
   
   struct KeyChain {
      static let stbPublicKey = "stbPublicKey"
      static let biometricKey = "stbBiometricKey"
   }
   
   struct DateFormat {
      static let defaultFormat = "yyyy-MM-dd HH:mm:ss"
      static let dateTimeFormatVN = "dd/MM/yyyy HH:mm:ss"
      static let dateFormatVN = "dd/MM/yyyy"
      static let serverDateTimeFormat = "yyyy-MM-dd HH:mm:ss"
      static let serverDateTimePromoFormat = "HH:mm:ss dd-MM-yyyy"
   }
   
   struct DeviceState {
      static let OK: Int = 1
      static let NO_DEVICE: Int = 0
      static let OTHER_DEVICE: Int = 2
   }
   
   struct Regex {
      static let phoneNumberPlusValidate = "^\\+[0-9  ]{12,15}$"
      static let phoneNumberValidate = "^\\d{10,11}$"
      static let emailValidate = "^[a-zA-Z0-9\\-_\\.]+@[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9]+){1,3}$"
      static let numberOnly = "^\\d+$"
      static let yyyyMMdd = "^\\d{8}$"
      static let serverDateTime = "^(19|20)\\d{2}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$"
      static let transDescEwallet = "^[a-zA-Z\\d\\s\\-\\+\\,\\.]+$"
      static let descriptionViaNapas = "^[a-zA-Z\\d\\s\\-\\+\\,\\.]+$"
   }
   
   struct Common {
      static let hardCode3DesKey = "1234567890123456"
      static let balanceProductCode = "AVAILABLE_BALANCE"
      static let homeSliderTimer: Double = 5.0
      static let passwordLength: Int = 6
      static let usernameLength: Int = 20
      static let otpMaxLength: Int = 6
      static let channelCode = "E_MOBILE"
      static let navigationHeight: CGFloat = 60.0
      static let phoneMaxLength: Int = 15
      static let phoneMinLength: Int = 10
      static let nameMaxLength: Int = 30
      static let emailMaxLength: Int = 50
      static let cardNumberMaxLength: Int = 16
      static let domesticCardNumberMaxLength: Int = 19
      static let domesticCardNumberMinLength: Int = 16
      static let visaMasterLength: Int = 16
      static let maxUsualProductItem: Int = 7
      static let maxUsualProductEditItem: Int = 4
      static let identifyLevelVerified = ["3", "4", "5"]
      static let pagesHistoryBack = "pagesHistory=['native:back']"
      static let cashInMinAmount: Int = 1
      static let defaultTopupAmount = "10000|20000|50000|100000|200000|500000"
      static let phonePrefix = "+84"
      static let qrCodeAmountMaxDigit: Int = 10
      static let accountMaxLength: Int = 25
      static let accountMinLength: Int = 4
      static let descriptionMaxLength: Int = 150
      static let alphabetChacters = "abcdefghijklmnopqrstuvwxyz"
      static let numericCharacters = "0123456789"
      static let accountNumberCharacters: String = alphabetChacters + numericCharacters
      static let validDescriptionViaNapasCharacters: String = alphabetChacters + numericCharacters + " -+,."
      static let alphabetSignedCharacters = "áàãảạăắằẵẳặâấầẫẩậđéèẽẻẹêếềễểệíìĩỉịóòõỏọôốồỗổộơớờỡởợúùũủụưứừữửựýỳỹỷỵ"
      static let transferEwalletValidCharacter = alphabetChacters + alphabetSignedCharacters + numericCharacters + " -+,."
      static let phoneNumberCharacters = "+0123456789  " // space 32 & space 160
      static let ewalletAccountLength: Int = 13
      static let keypairTag = "com.sacombank.ewallet.keys"
      static let defaultMapCoordinate = CLLocationCoordinate2D(latitude: 10.7876653, longitude: 106.6864466)
      static let mapFilterMaxLength: Int = 50
      static let sacombankPayUpdateHttpUrl = "https://itunes.apple.com/vn/app/sacombank-pay/id1436283663"
      static let sacombankPayUpdateAppstoreUrl = "itms-apps://itunes.apple.com/vn/app/sacombank-pay/id1436283663"
      static let errorCodeNeedBackHome = ["ESM-5014", "ESM-5015", "ESM-5016", "ESM-4105", "ESM-9998", "LOY-0002"]
      static let shadowRadiusLayerName = "shadowRadiusLayerName"
   }
   
   struct UIType {
      static let cashIn = "UIAddMoney"
      static let cashInOther = "UIDepositOtherway"
      static let domesticBank = "UIIBDomesticBank"
      static let counter = "UICounter"
      static let cashOut = "UIWithdrawMoney"
      static let transfer = "UITransferMoney"
      static let topup = "UIRechargePhone"
      static let creditCardPayment = "UICreditCard"
      static let linkCardAccount = "UILinkCardAcc"
      static let depositViaCardLinked = "UIOtherCardLink"
      static let depositViaOther = "UIOtherCard"
      static let payment = "UIPay_Grid"
      static let qrWithdraw = "UIQRWithdraw"
      static let qrPayment = "UIQRPayment"
      static let qrTransfer = "UIQRTransfer"
      static let transferToCard = "UITransferCard"
      static let transferToAccount = "UITransferCoreAcc"
      static let transferToEwallet = "UITransferEwallet"
      static let requestTransfer = "UIRequestMoney"
      static let shareBill = "UISharebill"
      static let share = "UIShare"
      static let otherCreditCard = "UILinkOtherCredit"
      static let smartQueue = "UISmartqueue"
      static let lixi = "UILuckyMoney"
      static let personalLoan = "UIPersonalLoan"
      static let loyalty = "UILoyalty"
      static let vnpayFlight = "UIFlightBooking"
      static let onlineShopping = "UIOnlineShopping"
      static let uiGrid = "UIGrid"
      static let cardMember = "UIMemberCard"
      static let gotit = "UIGotIt"
      static let vipCard = "UIVipCard"
      static let movieTicket = "UIMovieTicket"
      static let createCard = "UICreateCard"
      static let termSaving = "UISavingOnline"
      static let hotelBooking = "UIHotelBooking"
      static let trainBooking = "UITrainBooking"
      static let coachBooking = "UICoachBooking"
      static let mortageLoan = "UIMortgageLoan"
      static let login = "UI_LOGIN"
      static let register = "REGISTER"
      static let HOME_PROMOTION = "UI_PROM_SCROLL_HORISONTAL"
      static let HOME_PERSONAL = "UI_PERSONAL_HORISONTAL"
      static let HOME_GRID = "UI_GRID"
      static let HOME_GAME = "UI_GAME_SCROLL_HORISONTAL"
      static let HOME = "HOME"
      static let qr = "UIQR"
      static let payGrid = "UIPay_Grid"
      static let payElectricity = "UIPayElectricity"
      static let buyPhoneCard = "UIMobileCard"
      static let PROFILE_LIST = "UI_PROFILE_LIST"
      static let PROFILE_TOP = "PROFILE_TOP"
      static let PROFILE_GRID = "UI_PROFILE_GRID"
      static let gridOne = "UIGridOne"
      static let mascotScroll = "UIMascotScroll"
      static let personal = "UI_PERSONAL"
      static let logout = "UI_LOGOUT"
      static let notification = "UI_LIST_NOTI"
      static let cardAndAccount = "UI_CARD_N_ACCOUNT"
      static let listNotify = "UI_LIST_NOTI"
      static let settingList = "UI_SETTING_LIST"
      static let biometric = "UI_BIOMETRICS"
      static let mpassUpdate = "UI_MPASS_UPDATE"
      static let linkSacombank = "UILinkSacombank"
      static let linkOtherGrid = "UILinkOtherGrid"
      static let linkOtherNapas = "LINK_SOURCE_EXTERNAL_NAPAS"
      static let linkOtherGlobal = "LINK_SOURCE_EXTERNAL_INTERNATIONAL"
      static let usingRules = "UI_TNC"
      static let confirmPay = "UI_VERIFICATION"
      static let discoveryTopPromo = "UI_DEAL_SCROLL_HORISONTAL"
      static let discoveryLoyaltyGift = "UI_GIFT_SCROLL_HORISONTAL"
      static let discoveryNowPromo = "UI_NEWS_SCROLL_LIST"
      static let negativeAccount = "UIListPopup"
      static let manageBill = "UIProductRecentlyBill"
      static let manageAccount = "UIProductRecentlyAcc"
      static let manageWallet = "UIProductRecentlyEW"
      static let manageCard = "UIProductRecentlyCard"
      static let merchantSearch = "UIMerchantSearch"
      static let discoverPromotion = "UIPromotionDetail"
      static let contract = "UI_CONTRACT"
      static let documentation = "UI_IMG_SLIDE"
      static let group = "UI_GROUP"
      static let qna = "UI_QNA"
      static let paymentPlan = "UIPaymentPlan"
      static let exchangeGift = "UIexchangegift"
      static let myGift = "UIMygift"
      static let payBillBooking = "UIPaybillbooking"
      static let createCardDebit = "UICreateCardDebit"
      static let createCardPrepaid = "UICreateCardPrepaid"
   }
   
   struct ProductCode {
      static let withdraw = "WITHDRAW_EWALLET"
   }
   
   struct Character {
      static let space160 = " "
      static let delete = String(UnicodeScalar(UInt8(127)))
      static let passwordChar = "●"
   }
   
   struct Currency {
      static let vnd = "\(CurrencyType.vnd.rawValue)"
      static let usd = "\(CurrencyType.usd.rawValue)"
   }
   
   struct Procedure {
      static let logout = "TransactionService_logoutUser2"
   }
   
   struct MapLoction {
      static let hanoi = CLLocation(latitude: 21.024173, longitude: 105.839322)
      static let hcm = CLLocation(latitude: 10.7876653, longitude: 106.6864466)
   }
   
   struct CacheKey {
      static let smartQueueProvinceCache = "cache_smartqueue_province"
   }
   
   struct VNPServiceCode {
      static let Flight = "000001"
      static let Film = "000002"
      static let Hotel = "000003"
   }
   
   struct VCData {
      static var DiscoverMapCategoryHeight: CGFloat = 0
   }
   
   struct OpenWeather {
      static let apiKey = "409d91cac907855ec3b4287a838a1346"
      static let apiUrl = "http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=\(apiKey)&lang=vi&units=metric"
      static let iconUrl = "http://openweathermap.org/img/wn/%@@2x.png"
   }
}

