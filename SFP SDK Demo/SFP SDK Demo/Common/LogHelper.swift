//
//  LogHelper.swift
//  VoiceRec
//
//  Created by Nghia Vu on 11/14/19.
//  Copyright © 2019 Thien Minh. All rights reserved.
//

class LogHelper {
    class func log(_ any: Any) {
        #if DEBUG
        print(any)
        #endif
    }
}
