//
//  UIButton+OneClick.swift
//  FAS_MCredit
//
//  Created by NghiaVD on 5/23/17.
//  Copyright © 2017 NghiaVD. All rights reserved.
//

import UIKit
protocol UIButtonOneClickAction {
    func buttonSendAction(button: UIButtonOneClick)
}

class UIButtonOneClick: UIButton {
    static var buttonAction: UIButtonOneClick!
    
    var delegate: UIButtonOneClickAction!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(){
        self.addTarget(self, action: #selector(self.actionDown(button:)), for: UIControl.Event.touchDown)
        self.addTarget(self, action: #selector(self.actionCancel(button:)), for: UIControl.Event.touchCancel)
        self.addTarget(self, action: #selector(self.actionCancel(button:)), for: UIControl.Event.touchUpOutside)
        self.addTarget(self, action: #selector(self.actionUp(button:)), for: UIControl.Event.touchUpInside)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @objc func actionDown(button: UIButtonOneClick){
        if UIButtonOneClick.buttonAction == nil && delegate != nil{
            UIButtonOneClick.buttonAction = button
        }
    }
    
    @objc func actionCancel(button: UIButtonOneClick){
        if UIButtonOneClick.buttonAction != nil && UIButtonOneClick.buttonAction == button && delegate != nil{
            clearButtonAction()
        }
    }
    
    @objc func actionUp(button: UIButtonOneClick){
        if UIButtonOneClick.buttonAction != nil && UIButtonOneClick.buttonAction == button && delegate != nil{
            delegate.buttonSendAction(button: button)
            clearButtonAction()
        }
    }
    
    func clearButtonAction(){
        let when = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            UIButtonOneClick.buttonAction = nil
        }
    }
}
