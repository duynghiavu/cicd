//
//  Validation.swift
//  Forest
//
//  Created by NghiaVD on 7/21/17.
//  Copyright © 2017 NghiaVD. All rights reserved.
//

import Foundation

class Validation: NSObject {
    
    static func isValid(string: String, regEx: String) -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: string)
    }
    
    static func isValidEmail(string: String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return isValid(string: string, regEx: regEx)
    }
    
    static func isContainHtml(string: String) -> Bool {
        let regEx = "<[a-z][\\s\\S]*>"
        return isValid(string: string, regEx: regEx)
    }
    
    static func isContainSql(string: String) -> Bool {
        let strs = ["select", "insert", "delete", "create", "drop"]
        for str in strs {
            if string.lowercased().contains(find: str) {
                return true
            }
        }
        return false
    }
    
    static func isValidPhone(string: String) -> Bool {
        if string.count != 10, string.numberOnly.count != 10 {
            return false
        }
        
        let first = string.subString(from: 0, to: 1)
        if first != "0" {
            return false
        }
        
        return true
    }
}
