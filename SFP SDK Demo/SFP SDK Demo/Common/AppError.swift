//
//  AppError.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 5/21/19.
//

import UIKit

enum AppError: Swift.Error {
  case noNetwork
  case serviceNotAvailable
  case applicationDisabled
  case jailbroken
  case errorWithExecTime(String, Double)
  case error(String)
  case resultCodeError(ResultCode, String?)
  
  var displayMessage: String {
    return toString()
  }
  
  func message(orEmpty emptyMessage: String) -> String {
    let message = toString()
    return message.isEmpty ? emptyMessage : message
  }
  
  func toString() -> String {
    var message = ""
    switch self {
    case .jailbroken:
      message = "jailbroken_message".localized
    case .noNetwork:
      message = "error.network_not_available".localized
    case .serviceNotAvailable:
      message = "error.invoke_adapter_error".localized
    case .applicationDisabled:
      message = "error.application_disabled".localized
    case .errorWithExecTime(let text, _):
      message = text
    case .error(let text):
      message = text
    case .resultCodeError(_, let resultDesc):
      message = resultDesc ?? ""
    }
    return message
  }
}
