//
//  RoundedView.swift
//  SacombankPay
//
//  Created by Nghia Vu on 10/2/19.
//

import UIKit

class RoundedView: UIView {
   var corners: UIRectCorner = .allCorners {
      didSet {
         layoutSubviews()
      }
   }
   
   var radius: CGFloat = 0 {
      didSet {
         layoutSubviews()
      }
   }
   
   override func layoutSubviews() {
      roundCorners(corners, radius: radius)
   }
}
