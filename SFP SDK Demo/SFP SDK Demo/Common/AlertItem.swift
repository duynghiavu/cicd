//
//  AlertItem.swift
//  ViViet
//
//  Created by Long Hoang Giang on 6/18/19.
//

import Foundation
import RxSwift
import RxCocoa

class AlertItem {
  
  let disposeBag = DisposeBag()
  
  enum AlertType {
    case alert
    case confirm
  }
  
  var title: String?
  var message: String?
  var type: AlertType = .alert
  var okTitle: String?
  var cancelTitle: String?
  var okAction = PublishRelay<Bool>()
  var cancelAction = PublishRelay<Bool>()
  
  init(message: String,
       title: String? = nil,
       okTitle: String? = nil,
       cancelTitle: String? = nil,
       type: AlertType = .alert) {
    self.message = message
    self.title = title
    self.type = type
    self.okTitle = okTitle
    self.cancelTitle = cancelTitle
  }
  
  init(_ error: AppError, title: String? = nil) {
    self.message = error.displayMessage
    self.title = title
  }
  
}
