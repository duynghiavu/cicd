//
//  Locales.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 6/28/18.
//

import Foundation

struct Locales {
  
  static let vietnam = Locale(identifier: "vi_VN")
  static let us = Locale(identifier: "en_US")
  
  
}
