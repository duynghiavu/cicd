//
//  ResultCode.swift
//  ViViet
//
//  Created by Long Hoang Giang on 5/21/19.
//

import Foundation

public enum ResultCode: Equatable {
  case success
  case notfound
  case logoutCode
  case wrongOTPContinue
  case wrongOTPTooMuch
  case authNotMatch
  case resentOTPTooMuch
  case needChangePass
  case other(String)
  case none
  case goHomeCode
  case dummy
}

extension ResultCode: RawRepresentable {
  public typealias RawValue = String
  
  public init?(rawValue: String) {
    guard rawValue.isNotEmpty else {
      self = .none
      return
    }
    switch rawValue {
    case AppConstant.ResultCode.success: self = .success
    case AppConstant.ResultCode.notFound: self = .notfound
    case AppConstant.ResultCode.wrongOTPContinue: self = .wrongOTPContinue
    case AppConstant.ResultCode.wrongOTPTooMuch: self = .wrongOTPTooMuch
    case AppConstant.ResultCode.resendOTPTooMuch: self = .resentOTPTooMuch
    case AppConstant.ResultCode.passwordNotCorrect: self = .authNotMatch
    case AppConstant.ResultCode.needChangePassword: self = .needChangePass
    default:
      if AppConstant.ResultCode.logoutCodes.contains(rawValue) {
        self = .logoutCode
      } else if AppConstant.ResultCode.goHomeCodes.contains(rawValue) {
        self = .goHomeCode
      } else {
        self = .other(rawValue)
      }
    }
  }
  
  public var rawValue: String {
    switch self {
    case .success:
      return AppConstant.ResultCode.success
    case .notfound:
      return AppConstant.ResultCode.notFound
    case .wrongOTPContinue:
      return AppConstant.ResultCode.wrongOTPContinue
    case .wrongOTPTooMuch:
      return AppConstant.ResultCode.wrongOTPTooMuch
    case .resentOTPTooMuch:
      return AppConstant.ResultCode.resendOTPTooMuch
    case .authNotMatch:
      return AppConstant.ResultCode.passwordNotCorrect
    case .logoutCode:
      return AppConstant.ResultCode.logoutCodes.joined(separator: "|")
    case .none:
      return ""
    case .other(let resultCode):
      return resultCode
    case .goHomeCode:
      return AppConstant.ResultCode.goHomeCodes.joined(separator: "|")
    case .needChangePass:
      return AppConstant.ResultCode.needChangePassword
    case .dummy:
      return ""
    }
  }
  
  func inList(_ values: [ResultCode]) -> Bool {
    for value in values {
      if value == self { return true }
    }
    return false
  }
  
  func notInList(_ values: [ResultCode]) -> Bool {
    return !inList(values)
  }
  
}
