//
//  FormatHelper.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 5/18/18.
//

import Foundation

class FormatHelper {
   
   static func formatNumber(_ inputDouble: Double) -> String {
      return formatNumber(NSNumber(value: inputDouble))
   }
   
   static func formatNumber(_ inputInt: Int) -> String {
      return formatNumber(NSNumber(value: inputInt))
   }
   
   static func formatNumber(_ inputNumber: NSNumber) -> String {
      let numberFormatter = NumberFormatter(numberStyle: .decimal)
      numberFormatter.groupingSeparator = ","
      numberFormatter.decimalSeparator = "."
      numberFormatter.locale = Locales.vietnam
      return numberFormatter.string(from: inputNumber) ?? ""
   }
   
   static func formatCurrency(_ inputDouble: Double) -> String {
      return formatCurrency(NSNumber(value: inputDouble))
   }
   
   static func formatCurrency(_ inputInt: Int) -> String {
      return formatCurrency(NSNumber(value: inputInt))
   }
   
   static func formatCurrency(_ inputNumber: NSNumber, currency: CurrencyType = .vnd, currencySymbol: String? = nil) -> String {
      let symbol = currencySymbol ?? currency.symbol()
      return formatNumber(inputNumber) + symbol
   }
   
   static func normalizePhoneNumber(_ inputText: String) -> String {
      guard String.isNotEmpty(inputText) else { return inputText }
      var phoneNumber = inputText.trim()
      if phoneNumber.startsWith(AppConstant.Common.phonePrefix) {
         phoneNumber = "0\(String(phoneNumber[3...]))"
      }
      phoneNumber = phoneNumber.textNumber
      if phoneNumber.startsWith("84") && (phoneNumber.count == 11 || phoneNumber.count == 12) {
         phoneNumber = "0\(String(phoneNumber[2...]))"
      }
      return phoneNumber
   }
   
}
