//
//  AppTheme.swift
//  apacrs
//
//  Created by Nghia Vu on 12/22/19.
//  Copyright © 2019 Asia Pacific. All rights reserved.
//

import UIKit

class AppTheme {
    class Size {
        static let s4: CGFloat = 4
        static let s8: CGFloat = 8
        static let s16: CGFloat = 16
        static let s32: CGFloat = 32
        static let s44: CGFloat = 44
        static let s100: CGFloat = 100
    }
    
    class Color {
        static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        /** #C7EAFB */
        static let paleSkyBlue = #colorLiteral(red: 0.7803921569, green: 0.9176470588, blue: 0.9843137255, alpha: 1)
        /** #00b3ff */
        static let brightSkyBlue = #colorLiteral(red: 0, green: 0.7019607843, blue: 1, alpha: 1)
        /** #0573e0 */
        static let clearBlue = #colorLiteral(red: 0.01960784314, green: 0.4509803922, blue: 0.8784313725, alpha: 1)
        /** #006CB0 */
        static let blue = #colorLiteral(red: 0.2117647059, green: 0.4117647059, blue: 0.7882352941, alpha: 1)
        /** #046DAF */
        static let peacockBlue = #colorLiteral(red: 0.01568627451, green: 0.4274509804, blue: 0.6862745098, alpha: 1)
        /** #002F71 */
        static let marineBlue = #colorLiteral(red: 0, green: 0.1843137255, blue: 0.4431372549, alpha: 1)
        /** #002F71 */
        static let darkSlateBlue = #colorLiteral(red: 0, green: 0.1843137255, blue: 0.4431372549, alpha: 1)
        /** #002F71 */
        static let veryLightYellow = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.8745098039, alpha: 1)
        /** #FFCB05 */
        static let yellowLight = #colorLiteral(red: 1, green: 0.7960784314, blue: 0.01960784314, alpha: 1)
        /** #FFB20D */
        static let yellow = #colorLiteral(red: 1, green: 0.6980392157, blue: 0.05098039216, alpha: 1)
        /** #F37021 */
        static let orange = #colorLiteral(red: 0.9529411765, green: 0.4392156863, blue: 0.1294117647, alpha: 1)
        /** #DD322C */
        static let red = #colorLiteral(red: 0.9960784314, green: 0.2274509804, blue: 0.1882352941, alpha: 1)
        /** #009133 */
        static let green = #colorLiteral(red: 0, green: 0.568627451, blue: 0.2, alpha: 1)
        /** #F1F1F7 */
        static let paleGrey = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.968627451, alpha: 1)
        /** #F7F8FA */
        static let paleGrey2 = #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9803921569, alpha: 1)
        /** #CCCCCC */
        static let lightGray = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        /** #E6E6E6 */
        static let veryLightGray = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 0.5)
        /** #8C969F */
        static let gray8C969F = #colorLiteral(red: 0.5490196078, green: 0.5882352941, blue: 0.6235294118, alpha: 1)
        /** #767676 */
        static let brownishGray = #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.462745098, alpha: 1)
        /** #000A13 */
        static let veryDarkGray = #colorLiteral(red: 0.3450980392, green: 0.3490196078, blue: 0.3568627451, alpha: 1)
        /** #252525 */
        static let gray252525 = #colorLiteral(red: 0.1450980392, green: 0.1450980392, blue: 0.1450980392, alpha: 1)
        /** #0C1A30 */
        static let gray0C1A30 = #colorLiteral(red: 0.04705882353, green: 0.1019607843, blue: 0.1882352941, alpha: 1)
        
        //MARK: - Navigation
        static let navTint = UIColor.white
        static let navBtn = UIColor.white
        static let navBG = clearBlue
        
        static let navBlueTint = blue
        static let navBlueBtn = blue
        static let navBlueBG = UIColor.white
        
        //MARK: - Button
        static let btnTitle = white
        static let btnLink = blue
        static let btnLinkSelected = clearBlue
        static let btnDisable = veryLightGray
        static let btnEnable = clearBlue
        
        //MARK: - Text
        static let titleColor = blue
        static let primaryTextColor = veryDarkGray
        static let amountTextColor = blue
        static let secondTextColor = brownishGray
        static let threeTextColor = white
        
        //MARK: - TableView
        static let tableViewSeparatorColor = clearBlue
        
        //MARK: - Line
        static let lineBlue = clearBlue
        static let lineGray = veryLightGray
        
        //MARK: - View footer
        static let viewFooterBG = paleGrey
    }
    
    class Font {
        static let scale = UIScreen.main.bounds.width / CGFloat(UIDevice.current.userInterfaceIdiom == .pad ? 320 : 375)
        
        /** Font size - 14 */
        static let normalSize = 14 * scale
        static let normal = UIFont(typeface: .Regular, size: normalSize)
        static let normalItalic = UIFont(typeface: .Italic, size: normalSize)
        static let normalMedium = UIFont(typeface: .Medium, size: normalSize)
        static let normalSemiBold = UIFont(typeface: .Bold, size: normalSize)
        static let normalBold = UIFont(typeface: .Bold, size: normalSize)
        
        /** Font size - Dynamic */
        static func fontWith(size: CGFloat) -> UIFont {
            return UIFont(descriptor: normal.fontDescriptor, size: size)
        }
        static func fontItaticWith(size: CGFloat) -> UIFont {
            return UIFont(descriptor: normalItalic.fontDescriptor, size: size)
        }
        static func fontMediumWith(size: CGFloat) -> UIFont {
            return UIFont(descriptor: normalMedium.fontDescriptor, size: size)
        }
        static func fontSemiBoldWith(size: CGFloat) -> UIFont {
            return UIFont(descriptor: normalSemiBold.fontDescriptor, size: size)
        }
        static func fontBoldWith(size: CGFloat) -> UIFont {
            return UIFont(descriptor: normalBold.fontDescriptor, size: size)
        }
        
        static let tinySize = 10 * scale
        /** Font size - 10 */
        static let tiny = fontWith(size: tinySize)
        static let tinyItalic = fontItaticWith(size: tinySize)
        static let tinyMedium = fontMediumWith(size: tinySize)
        static let tinySemiBold = fontSemiBoldWith(size: tinySize)
        static let tinyBold = fontBoldWith(size: tinySize)
        
        static let smallSize = 12 * scale
        /** Font size - 12 */
        static let small = fontWith(size: smallSize)
        static let smallItalic = fontItaticWith(size: smallSize)
        static let smallMedium = fontMediumWith(size: smallSize)
        static let smallSemiBold = fontSemiBoldWith(size: smallSize)
        static let smallBold = fontBoldWith(size: smallSize)
        
        static let largeSize = 16 * scale
        /** Font size - 16 */
        static let large = fontWith(size: largeSize)
        static let largeItalic = fontItaticWith(size: largeSize)
        static let largeMedium = fontMediumWith(size: largeSize)
        static let largeSemiBold = fontSemiBoldWith(size: largeSize)
        static let largeBold = fontBoldWith(size: largeSize)
        
        static let large18Size = 18 * scale
        /** Font size - 18 */
        static let large18 = fontWith(size: large18Size)
        static let large18Italic = fontItaticWith(size: large18Size)
        static let large18Medium = fontMediumWith(size: large18Size)
        static let large18SemiBold = fontSemiBoldWith(size: large18Size)
        static let large18Bold = fontBoldWith(size: large18Size)
        
        static let large20Size = 20 * scale
        /** Font size - 20 */
        static let large20 = fontWith(size: large20Size)
        static let large20Italic = fontItaticWith(size: large20Size)
        static let large20Medium = fontMediumWith(size: large20Size)
        static let large20SemiBold = fontSemiBoldWith(size: large20Size)
        static let large20Bold = fontBoldWith(size: large20Size)
        
        static let large22Size = 22 * scale
        /** Font size - 20 */
        static let large22 = fontWith(size: large22Size)
        static let large22Italic = fontItaticWith(size: large22Size)
        static let large22Medium = fontMediumWith(size: large22Size)
        static let large22SemiBold = fontSemiBoldWith(size: large22Size)
        static let large22Bold = fontBoldWith(size: large22Size)
        
        static let large24Size = 24 * scale
        /** Font size - 24 */
        static let large24 = fontWith(size: large24Size)
        static let large24Italic = fontItaticWith(size: large24Size)
        static let large24Medium = fontMediumWith(size: large24Size)
        static let large24SemiBold = fontSemiBoldWith(size: large24Size)
        static let large24Bold = fontBoldWith(size: large24Size)
        
        static let largeSize32 = 32 * scale
        /** Font size - 32 */
        static let large32 = fontWith(size: largeSize32)
        static let large32Italic = fontItaticWith(size: largeSize32)
        static let large32Medium = fontMediumWith(size: largeSize32)
        static let large32SemiBold = fontSemiBoldWith(size: largeSize32)
        static let large32Bold = fontBoldWith(size: largeSize32)
    }
}
