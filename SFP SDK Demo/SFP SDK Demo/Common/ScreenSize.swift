//
//  ScreenSize.swift
//  ViViet
//
//  Created by Long Hoang Giang on 5/22/19.
//

import UIKit

struct ScreenSize: Comparable {

  var width: CGFloat = 0
  var height: CGFloat = 0
  
  static let main = ScreenSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
  static let iPhone5 = ScreenSize(width: 320.0, height: 568.0)
  static let iPhone678 = ScreenSize(width: 375.0, height: 667.0)
  static let iPhone678Plus = ScreenSize(width: 414.0, height: 736.0)
  static let iPhoneX = ScreenSize(width: 375.0, height: 812.0)
  static let iPhoneXsMax = ScreenSize(width: 414.0, height: 896.0)
  static let iPadPro9_7 = ScreenSize(width: 768.0, height: 1024.0)
  static let iPadPro10_5 = ScreenSize(width: 834, height: 1112.0)
  static let iPadPro12_9 = ScreenSize(width: 1024, height: 1366.0)
  static let iPadMini = ScreenSize(width: 768.0, height: 1024.0)
  
  static func < (lhs: ScreenSize, rhs: ScreenSize) -> Bool {
    if abs(lhs.width - rhs.width) > 0.9 {
      return lhs.width < rhs.width
    }
    return lhs.height < rhs.height
  }
  
}
