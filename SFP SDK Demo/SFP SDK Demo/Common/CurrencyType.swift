//
//  CurrencyType.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 9/14/18.
//

import Foundation

public enum CurrencyType: Int {
  case vnd = 704
  case usd = 840
  
  func symbol() -> String {
    return rawValue == CurrencyType.usd.rawValue ? "USD" : " ₫"
  }
}

class CurrencyHelper: NSObject {
  
  static func currencySymbol(type: CurrencyType) -> String {
    return type == .usd ? "USD" : " ₫"
  }
  
}
