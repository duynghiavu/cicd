//
//  RxSwift+Binding.swift
//  ViViet
//
//  Source at
//  https://github.com/ReactiveX/RxSwift/blob/master/RxExample/RxExample/Operators.swift
//


import RxSwift
import RxCocoa
import UIKit

infix operator <~>

// work ok except language that use IME, that simplistic method could cause unexpected issues because it will return intermediate results while text is being inputed.
func <~> <T>(property: ControlProperty<T>, relay: BehaviorRelay<T>) -> Disposable {
  let bindToUIDisposable = relay.bind(to: property)
  let bindToRelay = property
    .subscribe(onNext: { n in
      relay.accept(n)
    }, onCompleted:  {
      bindToUIDisposable.dispose()
    })
  
  return Disposables.create(bindToUIDisposable, bindToRelay)
}

func <~> <T>(relay: BehaviorRelay<T>, property: ControlProperty<T>) -> Disposable {
  let bindToUIDisposable = relay.bind(to: property)
  let bindToRelay = property
    .subscribe(onNext: { n in
      relay.accept(n)
    }, onCompleted:  {
      bindToUIDisposable.dispose()
    })
  return Disposables.create(bindToUIDisposable, bindToRelay)
}

extension ObservableType {
  func currentAndPrevious() -> Observable<(current: Element, previous: Element)> {
    return self.multicast({ () -> PublishSubject<Element> in PublishSubject<Element>() }) { (values: Observable<Element>) -> Observable<(current: Element, previous: Element)> in
      let pastValues = Observable.merge(values.take(1), values)
      
      return Observable.combineLatest(values.asObservable(), pastValues) { (current, previous) in
        return (current: current, previous: previous)
      }
    }
  }
}

func <~> <T: Equatable>(lhs: BehaviorRelay<T>, rhs: BehaviorRelay<T>) -> Disposable {
  typealias ItemType = (current: T, previous: T)
  
  return Observable.combineLatest(lhs.currentAndPrevious(), rhs.currentAndPrevious())
    .filter({ (first: ItemType, second: ItemType) -> Bool in
      return first.current != second.current
    })
    .subscribe(onNext: { (first: ItemType, second: ItemType) in
      if first.current != first.previous {
        rhs.accept(first.current)
      }
      else if (second.current != second.previous) {
        lhs.accept(second.current)
      }
    })
}

// hỗ trợ bind text nhanh cho textfield
func <~> (relay: BehaviorRelay<String?>, textField: UITextField) -> Disposable {
  return relay <~> textField.rx.text
}

infix operator ~>

func ~> <T>(event: ControlEvent<T>, relay: PublishRelay<T>) -> Disposable {
  return event.bind(to: relay)
}

func ~> <T>(relay: PublishRelay<T>, binder: RxCocoa.Binder<T>) -> Disposable {
  return relay.bind(to: binder)
}

func ~> <T>(relay: BehaviorRelay<T>, binder: RxCocoa.Binder<T>) -> Disposable {
  return relay.bind(to: binder)
}

func ~> <T>(lhs: PublishRelay<T>, rhs: PublishRelay<T>) -> Disposable {
  return lhs.bind(to: rhs)
}

func ~> <T>(lhs: BehaviorRelay<T>, rhs: BehaviorRelay<T>) -> Disposable {
  return lhs.bind(to: rhs)
}

func ~> <T>(lhs: BehaviorRelay<T>, rhs: PublishRelay<T>) -> Disposable {
  return lhs.bind(to: rhs)
}

func ~> <T>(lhs: PublishRelay<T>, rhs: @escaping (T) -> Void) -> Disposable {
  return lhs.bind(onNext: rhs)
}

func ~> <T>(lhs: BehaviorRelay<T>, rhs: @escaping (T) -> Void) -> Disposable {
  return lhs.bind(onNext: rhs)
}

func ~> <T>(lhs: PublishRelay<T>, rhs: @escaping () -> Void) -> Disposable {
  return lhs.bind(onNext: { _ in rhs() })
}

func ~> <T>(lhs: BehaviorRelay<T>, rhs: @escaping () -> Void) -> Disposable {
  return lhs.bind(onNext: { _ in rhs() })
}

func ~> <T>(lhs: ControlEvent<T>, rhs: @escaping () -> Void) -> Disposable {
  return lhs.bind(onNext: { _ in rhs() })
}

func ~> <T>(lhs: Observable<T>, rhs: BehaviorRelay<T>) -> Disposable {
  return lhs.bind(to: rhs)
}

func ~> <T>(lhs: Observable<T>, binder: RxCocoa.Binder<T>) -> Disposable {
  return lhs.bind(to: binder)
}

func ~> (lhs: Disposable, bag: DisposeBag) {
  lhs.disposed(by: bag)
}

func ~> <T>(relay: BehaviorRelay<T>, property: ControlProperty<T>) -> Disposable {
  return relay.bind(to: property)
}

func ~> <T>(relay: PublishRelay<T>, property: ControlProperty<T>) -> Disposable {
  return relay.bind(to: property)
}

// tiện ích bind cho label nhanh
func ~> (relay: BehaviorRelay<String?>, label: UILabel) -> Disposable {
  return relay.bind(to: label.rx.text)
}

func ~> (relay: PublishRelay<String?>, label: UILabel) -> Disposable {
  return relay.bind(to: label.rx.text)
}

// tiện ích bind enable cho button nhanh
func ~> (relay: BehaviorRelay<Bool>, button: UIButton) -> Disposable {
  return relay.bind(to: button.rx.isEnabled)
}

func ~> (relay: PublishRelay<Bool>, button: UIButton) -> Disposable {
  return relay.bind(to: button.rx.isEnabled)
}
