//
//  Numbers.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 6/27/18.
//

import Foundation

public struct Numbers {
  static let characterSet = CharacterSet(charactersIn: "0123456789")
  static let doubleCharacterSet = CharacterSet(charactersIn: "0123456789.")
}
