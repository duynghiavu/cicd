//
//  NumberFormatterExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 6/27/18.
//

import Foundation

extension NumberFormatter {
  
  convenience init(numberStyle: NumberFormatter.Style) {
    self.init()
    self.numberStyle = numberStyle
  }
  
}
