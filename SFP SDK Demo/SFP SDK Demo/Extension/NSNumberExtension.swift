//
//  NSNumberExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 8/21/18.
//

import Foundation

extension NSNumber {
  
  convenience init(value: String) {
    self.init(value: Double(value) ?? Double(0))
  }
   
   func decimal(digit: Int = 2) -> String{
      let formatter = NumberFormatter()
      formatter.numberStyle = .decimal
      formatter.maximumFractionDigits = digit
      return formatter.string(from: self) ?? "0.0"
   }
}
