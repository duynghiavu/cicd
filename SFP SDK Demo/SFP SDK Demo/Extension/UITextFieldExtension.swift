//
//  UITextFieldExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 6/27/18.
//

import UIKit

extension UITextField {
  
  struct PropertyKeys {
    static var maxLengthKey = "UITextFieldMaxLengthKey"
  }
  
  var string: String { return text ?? "" }
  var length: Int { return text?.count ?? 0 }
  
  var maxLength: Int? {
    get {
      return objc_getAssociatedObject(self, &PropertyKeys.maxLengthKey) as? Int
    }
    set(value) {
      objc_setAssociatedObject(self, &PropertyKeys.maxLengthKey, value, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      removeTarget(self, action: nil, for: .editingChanged)
      addTarget(self, action: #selector(onEditingChanged), for: .editingChanged)
    }
  }
  
  @objc func onEditingChanged() {
    guard let maxLength = maxLength, maxLength > 0,
      let text = text, text.count > maxLength else {
        return
    }
    self.text = String(text[0..<maxLength])
  }
  
}

extension UITextField {
    @objc func setClearButton(with image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
        rightView = clearButton
        rightViewMode = .whileEditing
    }
    
    @objc func clear(_ sender : AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
}
