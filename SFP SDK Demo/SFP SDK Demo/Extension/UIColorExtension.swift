//
//  UIColorExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/3/18.
//

import UIKit

extension UIColor {
    
  // usage: let red: UIColor = UIColor(hex: 0xF44336)
  convenience public init(hex: Int, alpha: CGFloat = 1.0) {
    let red = CGFloat(hex >> 16 & 0xFF) / 255.0
    let green = CGFloat(hex >> 8 & 0xFF) / 255.0
    let blue = CGFloat(hex & 0xFF) / 255.0
    self.init(red:red, green:green, blue:blue, alpha:alpha)
  }
  
  // usage: let red: UIColor = UIColor(hexString: "#FF4221")
  convenience init(hexString: String) {
    let hexStringNormalize = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
    var hex = UInt32()
    Scanner(string: hexStringNormalize).scanHexInt32(&hex)
    let alpha = hex > 0xFFFFFF ? CGFloat(hex >> 24) / 255.0 : 1.0
    let red = CGFloat(hex >> 16 & 0xFF) / 255.0
    let green = CGFloat(hex >> 8 & 0xFF) / 255.0
    let blue = CGFloat(hex & 0xFF) / 255.0
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  func toHexString() -> String {
    var r:CGFloat = 0
    var g:CGFloat = 0
    var b:CGFloat = 0
    var a:CGFloat = 0
    
    getRed(&r, green: &g, blue: &b, alpha: &a)
    let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
    
    return String(format:"#%06x", rgb)
  }
    
}
