//
//  AppExtension.swift
//  campp-ios-swift
//
//  Created by LamND on 12/25/17.
//  Copyright © 2017 BeeTech. All rights reserved.
//

import UIKit
import Photos

let SCREEN_WIDTH:CGFloat = UIScreen.main.bounds.width
let SCREEN_HEIGHT:CGFloat = UIScreen.main.bounds.height
var SCREEN_SCALE:CGFloat { return SCREEN_WIDTH / 320 }
var CURRENT_TIME:TimeInterval { return Date().timeIntervalSince1970 }

extension URL {
    func open() {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(self)
        } else {
            UIApplication.shared.openURL(self)
        }
    }
    
    func toString() -> String {
        return self.absoluteString
    }
}

extension Array where Element:Equatable {
    mutating func remove(_ element:Element) -> Element? {
        if let index = firstIndex(of: element) {
            return self.remove(at: index)
        }
        return nil
    }
    mutating func rearrange(from: Int, to: Int) {
        let element = self[from]
        self.remove(at: from)
        self.insert(element, at: to)
    }
}

extension UICollectionView {
    func register(_ cell:UICollectionViewCell.Type) {
        register(UINib.init(nibName: cell.className(), bundle: nil), forCellWithReuseIdentifier: cell.className())
    }
    func dequeueCell<T:UICollectionViewCell>(indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.className(), for: indexPath) as! T
    }
}

extension UITableView {
    func register(_ cell:UITableViewCell.Type) {
        register(UINib.init(nibName: cell.className(), bundle: nil), forCellReuseIdentifier: cell.className())
    }
    func dequeueCell<T:UITableViewCell>() -> T {
        return self.dequeueReusableCell(withIdentifier: T.className()) as! T
    }
    func title(forEmptyState title:String, color:UIColor = UIColor.darkGray) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        return NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.paragraphStyle: paragraphStyle,
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)])
    }
    
    func description(forEmptyState desc:String, color:UIColor = UIColor.darkGray) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        return NSAttributedString(string: desc,
                                  attributes: [
                                    NSAttributedString.Key.paragraphStyle: paragraphStyle,
                                    NSAttributedString.Key.foregroundColor: color,
                                    NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)])
    }
    
    func reloadDataAndView() {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.reloadData()
        }
        reloadData()
        CATransaction.commit()
    }
}

public extension UIImageView {
    func hnk_setImageFromURL(_ URL: Foundation.URL?) {
        guard let URL = URL else {return}
        self.hnk_setImageFromURL(URL)
    }
}

var bundleKey: UInt8 = 0

class AnyLanguageBundle: Bundle {
    
    override func localizedString(forKey key: String,
                                  value: String?,
                                  table tableName: String?) -> String {
        
        guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
            let bundle = Bundle(path: path) else {
                
                return super.localizedString(forKey: key, value: value, table: tableName)
        }
        
        return bundle.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    
    class func setLanguage(_ language: String) {
        
        defer {
            
            object_setClass(Bundle.main, AnyLanguageBundle.self)
        }
        
        objc_setAssociatedObject(Bundle.main, &bundleKey,    Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        OperationQueue.main.addOperation {
            var vc = UIApplication.shared.keyWindow?.rootViewController
            vc?.view.reloadxibLocKey()
            while vc?.presentedViewController != nil {
                vc = vc!.presentedViewController
                vc!.view.reloadxibLocKey()
            }
        }
    }
}
