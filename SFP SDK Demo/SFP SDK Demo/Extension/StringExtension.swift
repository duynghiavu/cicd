//
//  StringExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/2/18.
//

import Foundation
import UIKit

protocol OptionalString {}
extension String: OptionalString {}
// extension cho phep check su dung abc.isEmpty voi abc la optional string
extension Optional where Wrapped: OptionalString {
   var isEmpty: Bool {
      return ((self as? String) ?? "").isEmpty
   }
   
   var isNotEmpty: Bool {
      guard let s = self as? String else { return false }
      return !s.isEmpty
   }
   
}

extension String {
   
   var toBytes: [UInt8] {
      return Array(self.utf8)
   }
   
   // cach dung: "hello_world".localized
   var localized: String {
    return Bundle.main.localizedString(forKey: self, value: "", table: nil)
   }
   // get only number in string
   var numberOnly: String { return components(separatedBy: Numbers.characterSet.inverted).joined() }
   // get number and . accept double format in string
   var textNumber: String { return components(separatedBy: Numbers.doubleCharacterSet.inverted).joined() }
   var nsNumber: NSNumber { return NSNumber(value: Double(self) ?? 0) }
   
   func localizedFormat(_ args: CVarArg...) -> String {
    let localizedString = Bundle.main.localizedString(forKey: self, value: "", table: nil)
      return NSString(format: localizedString, arguments: getVaList(args)) as String
   }
   
   func format(_ args: CVarArg...) -> String {
      return NSString(format: self, arguments: getVaList(args)) as String
   }
   
   static func localized(for key: String, default value: String?) -> String {
    return Bundle.main.localizedString(forKey: key, value: value ?? "", table: nil)
   }
   
   var isNotEmpty: Bool {
      return !self.isEmpty
   }
   
   static func isEmpty(_ string: String?) -> Bool {
      guard let s = string else { return true }
      return s.count == 0
   }
   
   static func isNotEmpty(_ string: String?) -> Bool {
      if let s = string {
         return s.count > 0
      }
      return false
   }
   
   func contains(find: String) -> Bool {
      return self.range(of: find) != nil
   }
   
   func containsIgnoreCase(find: String) -> Bool {
      return self.range(of: find, options: .caseInsensitive) != nil
   }
   
   func startsWith(_ prefix: String) -> Bool {
      return self.hasPrefix(prefix)
   }
   
   func endsWith(_ suffix: String) -> Bool {
      return self.hasSuffix(suffix)
   }
   
   func removeCharacterNotIn(_ set: CharacterSet) -> String {
      return components(separatedBy: set.inverted).joined()
   }
   
   func trim() -> String {
      return self.trimmingCharacters(in: .whitespacesAndNewlines)
   }
   
   func toJSON() -> Any? {
      guard let inputData = self.data(using: .utf8) else { return nil }
      let data = try? JSONSerialization.jsonObject(with: inputData, options: .mutableContainers)
      return data ?? nil
   }
   
   func toUrlQueryAllowed() -> URL? {
      if let link = addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: link) {
         return url
      }
      return nil
   }
   
   func startsWith(_ prefixes: [String]) -> Bool {
      var result = false
      for prefix in prefixes {
         if self.hasPrefix(prefix) {
            result = true
            break
         }
      }
      return result
   }
   
   func endsWith(_ suffixes: [String]) -> Bool {
      var result = false
      for suffix in suffixes {
         if self.hasSuffix(suffix) {
            result = true
            break
         }
      }
      return result
   }
   
   func isValid(withRegex: String) -> Bool {
      let regexTest = NSPredicate(format: "SELF MATCHES %@", withRegex)
      let result = regexTest.evaluate(with: self)
      return result
   }
   
   func toInteger() -> Int {
      return Int(textNumber) ?? 0
   }
   
   func index(of subString: String) -> Int? {
      let maxIndex = self.count - subString.count
      let subStringCount = subString.count
      for index in 0...maxIndex {
         if String(self[index..<(index+subStringCount)]) == subString {
            return index
         }
      }
      return nil
   }
   
   func replacingCharacters(in range: NSRange, with replacement: String) -> String {
      return (self as NSString).replacingCharacters(in: range, with: replacement)
   }
   
   func size(withFont font: UIFont) -> CGSize {
      return (self as NSString).size(withAttributes: [NSAttributedString.Key.font: font])
   }
   
   func containsAnyPos(subString: String) -> Bool {
      var selfString = self.trim()
      var searchString = subString.trim()
      guard searchString.count <= selfString.count else { return false }
      if selfString.startsWith(searchString) { return true }
      let replaceCharacters = [",", "."]
      for replaceChar in replaceCharacters {
         selfString = selfString.replacingOccurrences(of: replaceChar, with: " ")
      }
      selfString = selfString.trim()
      for replaceChar in replaceCharacters {
         searchString = searchString.replacingOccurrences(of: replaceChar, with: " ")
      }
      let wordList = selfString.components(separatedBy: " ").filter({ String.isNotEmpty($0) })
      let searchWordList = searchString.components(separatedBy: " ").filter({ String.isNotEmpty($0) })
      if searchWordList.isEmpty || wordList.count < searchWordList.count {
         return false
      }
      if wordList.count < searchWordList.count {
         return false
      }
      var j = 0
      for i in 0..<wordList.count {
         let word = wordList[i]
         let sword = searchWordList[j]
         if word.hasPrefix(sword) {
            j += 1
            if j == searchWordList.count { break }
         }
      }
      return j == searchWordList.count
   }
   
   func isValid(withString checkString: String) -> Bool {
      var scoreMap = [Character: Int]()
      for c in checkString {
         scoreMap[c] = 1
      }
      let selfLower = self.lowercased()
      var score: Int = 0
      for s in selfLower {
         score += scoreMap[s] ?? 0
         if score > 0 { break }
      }
      return score > 0
   }
   
   func isValidOnly(withString checkString: String, ignorecase: Bool = true) -> Bool {
      var checkMap = [Character: Int]()
      for c in checkString {
         checkMap[c] = 1
      }
      let selfString = ignorecase ? self.lowercased() : self
      for s in selfString {
         guard checkMap[s] == 1 else {
            return false
         }
      }
      return true
   }
   
   func addingPercentEncodingForRFC3986() -> String? {
      let unreserved = "-._~/?"
      let allowed = NSMutableCharacterSet.alphanumeric()
      allowed.addCharacters(in: unreserved)
      return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
   }
   
   func addingPercentEncodingForFormData(_ plusForSpace: Bool = false) -> String? {
      let unreserved = "*-._"
      let allowed = NSMutableCharacterSet.alphanumeric()
      allowed.addCharacters(in: unreserved)
      if plusForSpace {
         allowed.addCharacters(in: " ")
      }
      var encoded = addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
      if plusForSpace {
         encoded = encoded?.replacingOccurrences(of: " ", with: "+")
      }
      return encoded
   }
   
   func replaceSpace160() -> String {
      return self.replacingOccurrences(of: " ", with: " ")
   }
   
   func replaceNewLine() -> String {
      return self.replacingOccurrences(of: "\\n", with: "\n")
   }
   
   // convert html to attribute string
   func htmlToAttributedString(_ font: UIFont? = nil, _ width: CGFloat = 0) -> NSAttributedString {
      var string = "<html><header><style type=\"text/css\">"
      if let font = font {
         string += "body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px;}"
      }
      if width > 0 {
         string += "img{ max-height: 100%; max-width: \(width) !important; width: auto; height: auto;}"
      }
      
      string += "</style></header><body>\(self)</body><html>"
      
      guard let htmlData = string.data(using: String.Encoding.unicode, allowLossyConversion: false) else {
         return NSAttributedString()
      }
      let htmlString: NSAttributedString?
      do {
         htmlString = try NSAttributedString(data: htmlData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html], documentAttributes: nil)
      } catch _ {
         htmlString = nil
      }
      return htmlString ?? NSAttributedString()
   }
   
   func htmlToString() -> String {
      return htmlToAttributedString().string
   }
}

// MARK: extension ho tro substring nhanh hon, ho tro index theo Int thay vi chi theo String.Index
extension String {
   
   subscript(value: PartialRangeFrom<Int>) -> Substring {
      get {
         return self[index(startIndex, offsetBy: value.lowerBound)...]
      }
   }
   
   subscript(value: PartialRangeUpTo<Int>) -> Substring {
      get {
         return self[..<index(startIndex, offsetBy: value.upperBound)]
      }
   }
   
   subscript(value: PartialRangeThrough<Int>) -> Substring {
      get {
         return self[...index(startIndex, offsetBy: value.upperBound)]
      }
   }
   
   subscript(bounds: CountableRange<Int>) -> Substring {
      get {
         let beginIndex = index(startIndex, offsetBy: bounds.lowerBound)
         let endIndex = index(startIndex, offsetBy: bounds.upperBound)
         return self[beginIndex..<endIndex]
      }
   }
}

extension String {
   
   static let alphabetNumber = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
   
   static func randomString(_ length: Int) -> String {
      //    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0...length-1).map{ _ in alphabetNumber.randomElement()! })
   }
   
   func decodeBase64() -> String? {
      guard let data = Data(base64Encoded: self) else { return nil }
      return String(data: data, encoding: .utf8)
   }
   
   func subString(from: Int, to: Int) -> String {
      let start = index(startIndex, offsetBy: from)
      let end = index(start, offsetBy: to - from)
      return String(self[start ..< end])
   }
   
   func subString(range: NSRange) -> String {
      return subString(from: range.lowerBound, to: range.upperBound)
   }
   
   func widthOfString(usingFont font: UIFont) -> CGFloat {
      let fontAttributes = [NSAttributedString.Key.font: font]
      let size = self.size(withAttributes: fontAttributes)
      return size.width
   }
   
   func separate(every stride: Int = 4, with separator: Character = " ") -> String {
      return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
   }
}

extension String {
   func capitalizedFirst() -> String {
      let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
      let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
      return first.uppercased() + rest.lowercased()
   }
   
   func capitalizedFirst(with: Locale?) -> String {
      let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
      let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
      return first.uppercased(with: with) + rest.lowercased(with: with)
   }
}

extension String{
   public func dateStringtoDisplay() -> String{
      if self.isValid(withRegex: AppConstant.Regex.yyyyMMdd) {
        let paymentDueDateObj = Date.from(self, "yyyyMMdd")
        return paymentDueDateObj?.toString(format: "dd/MM/yyyy") ?? self
      }
      return self
   }
   
    public func decimalToDisplay() -> String{
        return FormatHelper.formatNumber(Double(self) ?? Double(0))
   }
   
   public func currencyToDisplay() -> String{
     return FormatHelper.formatCurrency(Double(self) ?? Double(0))
   }
}

extension String {
    func removeVNSigned() -> String {
        let vnSignedCharacters = ["á|à|ả|ạ|ã|â|ấ|ầ|ẩ|ậ|ẫ|ă|ắ|ằ|ẳ|ặ|ẵ", "Á|À|Ả|Ạ|Ã|Â|Ấ|Ầ|Ẩ|Ậ|Ẫ|Ă|Ắ|Ằ|Ẳ|Ặ|Ẵ",
                                  "đ", "Đ", "í|ì|ỉ|ị|ĩ", "Í|Ì|Ỉ|Ị|Ĩ", "é|è|ẻ|ẹ|ẽ|ê|ế|ề|ể|ệ|ễ", "É|È|Ẻ|Ẹ|Ẽ|Ê|Ế|Ề|Ể|Ệ|Ễ",
                                  "ó|ò|ỏ|ọ|õ|ô|ố|ồ|ổ|ộ|ỗ|ơ|ớ|ờ|ở|ợ|ỡ", "Ó|Ò|Ỏ|Ọ|Õ|Ô|Ố|Ồ|Ổ|Ộ|Ỗ|Ơ|Ớ|Ờ|Ở|Ợ|Ỡ",
                                  "ú|ù|ủ|ụ|ũ|ư|ứ|ừ|ử|ự|ữ", "Ú|Ù|Ủ|Ụ|Ũ|Ư|Ứ|Ừ|Ử|Ự|Ữ", "ý|ỳ|ỷ|ỵ|ỹ", "Ý|Ỳ|Ỷ|Ỵ|Ỹ"]
        
        let vnNoneSignedCharacters = ["a", "A", "d", "D", "i", "I", "e", "E", "o", "O", "u", "U", "y", "Y"]
        
        guard count > 0 else { return self }
        var result = self
        for index in 0..<vnSignedCharacters.count {
            guard index < vnNoneSignedCharacters.count else { break }
            let sourceChars = vnSignedCharacters[index].components(separatedBy: "|")
            let replaceChar = vnNoneSignedCharacters[index]
            for sourceChar in sourceChars {
                result = result.replacingOccurrences(of: sourceChar, with: replaceChar)
            }
        }
        return result
    }
}

extension String {
    func strikeThrough(font: UIFont = AppTheme.Font.normal, color: UIColor = AppTheme.Color.primaryTextColor) -> NSAttributedString {
        let options: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color, .strikethroughStyle: NSUnderlineStyle.single.rawValue]
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttributes(options, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
