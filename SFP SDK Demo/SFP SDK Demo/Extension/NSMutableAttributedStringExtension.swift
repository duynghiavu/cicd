//
//  NSMutableAttributedStringExtension.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 12/9/21.
//  Copyright © 2021 LienVietPostBank. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    
    func add(value:String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font: font,
            .foregroundColor: color
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font: AppTheme.Font.normalBold
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font: AppTheme.Font.normal
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  AppTheme.Font.normal,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
