//
//  DateExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 5/10/18.
//

import Foundation

extension Date {

  func toString(format: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    let result = formatter.string(from: self)
    return result
  }
  
  static func from(_ string: String, _ format: String) -> Date? {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    let result = formatter.date(from: string)
    return result
  }
  
  var tomorrow: Date {
    return Calendar.current.date(byAdding: .day, value: 1, to: beginDay)!
  }
  
  var yesterday: Date {
    return Calendar.current.date(byAdding: .day, value: -1, to: beginDay)!
  }
  
  var beginDay: Date {
    return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
  }
  
  var noon: Date {
    return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
  }
  
  var endDay: Date {
    return Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: self)!
  }
  
  var month: Int {
    return Calendar.current.component(.month, from: self)
  }

  func numberOfDays(to: Date) -> Int {
    let components = Calendar.current.dateComponents([.day], from: self, to: to)
    return components.day ?? 0
  }
  
  func numberOfDays(from: Date) -> Int {
    let components = Calendar.current.dateComponents([.day], from: from, to: self)
    return components.day ?? 0
  }
  
  func currentTimeMillis() -> Double {
    return (self.timeIntervalSince1970 * 1000.0)
  }
  
  func currentTimeMillisInDay() -> Int64 {
    return Int64(currentTimeMillis()) % 86400000
  }
  
  func toDate(numberOfDays: Int) -> Date {
    return Calendar.current.date(byAdding: .day, value: numberOfDays, to: self)!
  }
  
  func addDay(day: Int) -> Date {
    return Calendar.current.date(byAdding: .day, value: day, to: self)!
  }
  
  func addMonth(month: Int) -> Date {
    return Calendar.current.date(byAdding: .month, value: month, to: self)!
  }
  
}
