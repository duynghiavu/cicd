//
//  UIComponent.swift
//  CargoLinkDriver
//
//  Created by Nghia Vu on 5/17/19.
//  Copyright © 2019 Nghia Vu. All rights reserved.
//

import UIKit

protocol XIBView {
    var xibCornerRadius: CGFloat { get set }
    var xibIsCircle: Bool { get set }
}

protocol XIBLocalizable {
    var xibIsUppercase: Bool { get set }
    var xibLocKey: String? { get set }
}

protocol XIBTextField {
    var xibPlaceholderColor: UIColor? { get set }
    var xibPlaceholderFont: UIFont? { get set }
}

extension UIView: XIBView{
    struct AssociateKeys {
        static var isUppercase = "isUppercase"
        static var locKey = "locKey"
        static var isCircle = "isCircle"
    }
    
    func reloadxibLocKey(){
      self.subviews.forEach { (subView) in
         if var view: XIBLocalizable = subView as? XIBLocalizable, view.xibLocKey != nil, view.xibLocKey != ""{
            view.xibLocKey = view.xibLocKey
         }
         else{
            subView.reloadxibLocKey()
         }
      }
    }
   
   func reloadxibIsCircle(){
       if self.xibIsCircle {
           self.xibIsCircle = true
       }
      self.subviews.forEach { (subView) in
         subView.reloadxibIsCircle()
      }
   }
    
    @IBInspectable var xibIsCircle: Bool {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.isCircle, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            xibCornerRadius = CGFloat(xibCornerRadius)
        }
        get {
            guard let number = objc_getAssociatedObject(self, &AssociateKeys.isCircle) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
   
    @IBInspectable var xibRotate: CGFloat {
        get {
            return 0
        }
        set(key) {
            transform = CGAffineTransform(rotationAngle: CGFloat(key * .pi / 180))
        }
    }
    
    @IBInspectable var xibCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(key) {
            DispatchQueue.main.async {
                self.layer.cornerRadius = self.xibIsCircle ? min(self.frame.height, self.frame.width) / 2 : key
                self.layer.masksToBounds = true
            }
        }
    }
    
    @IBInspectable var xiBorderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set(key) {
            layer.borderWidth = key
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var xibBorderColor: UIColor {
        get {
            return (layer.borderColor != nil) ? UIColor(cgColor: layer.borderColor!) : UIColor.clear
        }
        set(key) {
            layer.borderColor = key.cgColor
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var xibShadowColor: UIColor {
        get {
            return (layer.shadowColor != nil) ? UIColor(cgColor: layer.shadowColor!) : UIColor.clear
        }
        set(key) {
            layer.shadowColor = key.cgColor
        }
    }
    
    @IBInspectable var xibShadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set(key) {
            layer.shadowOffset = key
        }
    }
    
    @IBInspectable var xibShadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set(key) {
            layer.shadowOpacity = key
        }
    }
}

extension UILabel: XIBLocalizable {
    @IBInspectable var xibIsUppercase: Bool {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.isUppercase, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue{
                text = text?.uppercased()
            }
        }
        
        get {
            guard let number = objc_getAssociatedObject(self, &AssociateKeys.isUppercase) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
    
    @IBInspectable var xibLocKey: String? {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.locKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                text = newValue?.localized
                if xibIsUppercase{
                    text = text?.uppercased()
                }
            }
        }
        
        get {
            guard let string = objc_getAssociatedObject(self, &AssociateKeys.locKey) as? String else {
                return ""
            }
            return string
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable var xibIsUppercase: Bool {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.isUppercase, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue{
                setTitle(title(for: .normal)?.uppercased(), for: .normal)
            }
        }
        
        get {
            guard let number = objc_getAssociatedObject(self, &AssociateKeys.isUppercase) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
    
    @IBInspectable var xibLocKey: String? {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.locKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                let title = newValue?.localized
                if xibIsUppercase{
                    setTitle(title?.uppercased(), for: .normal)
                }
                else{
                    setTitle(title, for: .normal)
                }
            }
        }
        
        get {
            guard let string = objc_getAssociatedObject(self, &AssociateKeys.locKey) as? String else {
                return ""
            }
            return string
        }
    }
}

extension UITextField: XIBLocalizable, XIBTextField {
    struct TFKeys {
        static var placeholderColor = "placeholderColor"
        static var placeholderFont = "placeholderFont"
    }
    
    @IBInspectable var xibIsUppercase: Bool {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.isUppercase, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue{
                placeholder = placeholder?.uppercased()
            }
        }
        
        get {
            guard let number = objc_getAssociatedObject(self, &AssociateKeys.isUppercase) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
    
    @IBInspectable var xibLocKey: String? {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.locKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                DispatchQueue.main.async {
                    self.placeholder = newValue?.localized
                    if self.xibIsUppercase{
                        self.placeholder = self.placeholder?.uppercased()
                    }
                    if let color = self.xibPlaceholderColor{
                        self.xibPlaceholderColor = color
                    }
                }
                
            }
        }
        
        get {
            guard let string = objc_getAssociatedObject(self, &AssociateKeys.locKey) as? String else {
                return ""
            }
            return string
        }
    }
    
    @IBInspectable var xibPlaceholderColor: UIColor? {
        set {
            objc_setAssociatedObject(self, &TFKeys.placeholderColor, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                let options: [NSAttributedString.Key: Any] = [.foregroundColor: newValue as Any, .font: (xibPlaceholderFont ?? font) as Any]
                attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                           attributes: options)
            }
        }
        
        get {
            guard let value = objc_getAssociatedObject(self, &TFKeys.placeholderColor) as? UIColor else {
                return nil
            }
            return value
        }
    }
    
    @IBInspectable var xibPlaceholderFont: UIFont? {
        set {
            objc_setAssociatedObject(self, &TFKeys.placeholderFont, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                let options: [NSAttributedString.Key: Any] = [.foregroundColor: (xibPlaceholderColor ?? textColor) as Any, .font: newValue as Any]
                attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                           attributes: options)
            }
        }
        
        get {
            guard let value = objc_getAssociatedObject(self, &TFKeys.placeholderFont) as? UIFont else {
                return nil
            }
            return value
        }
    }
}

extension UITextView: XIBLocalizable {
    @IBInspectable var xibIsUppercase: Bool {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.isUppercase, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue{
                text = text?.uppercased()
            }
        }
        
        get {
            guard let number = objc_getAssociatedObject(self, &AssociateKeys.isUppercase) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
    
    @IBInspectable var xibLocKey: String? {
        set {
            objc_setAssociatedObject(self, &AssociateKeys.locKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                text = newValue?.localized
                if xibIsUppercase{
                    text = text?.uppercased()
                }
            }
        }
        
        get {
            guard let string = objc_getAssociatedObject(self, &AssociateKeys.locKey) as? String else {
                return ""
            }
            return string
        }
    }
}

extension UITabBarItem: XIBLocalizable {
    struct AssociatedKeys {
        static var isUppercase = "isUppercase"
        static var locKey = "locKey"
    }
    
    @IBInspectable var xibIsUppercase: Bool {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.isUppercase, NSNumber(booleanLiteral: newValue), .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue{
                title = title?.uppercased()
            }
        }
        
        get {
            guard let number = objc_getAssociatedObject(self, &AssociatedKeys.isUppercase) as? NSNumber else {
                return false
            }
            return number.boolValue
        }
    }
    
    @IBInspectable var xibLocKey: String? {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.locKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue != nil{
                title = newValue?.localized
                if xibIsUppercase{
                    title = title?.uppercased()
                }
            }
        }
        
        get {
            guard let string = objc_getAssociatedObject(self, &AssociatedKeys.locKey) as? String else {
                return ""
            }
            return string
        }
    }
}
