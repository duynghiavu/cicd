//
//  ObservableExtension.swift
//  ViViet
//
//  Created by Long Hoang Giang on 5/29/19.
//

import Foundation
import RxSwift


/// Represent an optional value
///
/// This is needed to restrict our Observable extension to Observable that generate
/// .Next events with Optional payload
protocol OptionalType {
  associatedtype Wrapped
  var asOptional: Wrapped? { get }
}

/// Implementation of the OptionalType protocol by the Optional type
extension Optional: OptionalType {
  var asOptional: Wrapped? { return self }
}

extension ObservableType where Element: OptionalType {
  func unwrap() -> Observable<Element.Wrapped> {
    return self.filter { $0.asOptional != nil }.map { $0.asOptional! }
  }
}
