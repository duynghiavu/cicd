//
//  ObservableTypeExtension.swift
//  ViViet
//
//  Created by Long Hoang Giang on 5/29/19.
//

import Foundation
import RxSwift


extension ObservableType where Element: Equatable {
  
  public func ignore(_ valuesToIgnore: Element...) -> Observable<Element> {
    return self.asObservable().filter { !valuesToIgnore.contains($0) }
  }
  
  public func ignore<Sequence: Swift.Sequence>(_ valuesToIgnore: Sequence) -> Observable<Element> where Sequence.Element == Element {
    return self.asObservable().filter { !valuesToIgnore.contains($0) }
  }
  
}

extension ObservableType {
  func always(_ closure: @escaping () -> Void) -> Disposable {
    return self.subscribe(onNext: { _ in
      closure()
    }, onError: { _ in
      closure()
    })
  }
  
  func next(_ onNext: @escaping (Element) -> Void) -> Disposable {
    return self.subscribe(onNext: { (data) in
      onNext(data)
    }, onError: { (error) in
      print("Error: \(error.localizedDescription)")
    })
  }
  
  func next(_ onNext: @escaping (Element) -> Void, _ onError: @escaping (AppError) -> Void) -> Disposable {
    return self.subscribe(onNext: { (data) in
      onNext(data)
    }, onError: { (error) in
      onError( error is AppError ? (error as! AppError) : AppError.error(error.localizedDescription) )
    })
  }
  
  func bind(_ onNext: @escaping (Element) -> Void, _ onError: ((AppError) -> Void)? = nil) -> Disposable {
    return self.subscribe(onNext: { (data) in
      onNext(data)
    }, onError: { (error) in
      onError?( error is AppError ? (error as! AppError) : AppError.error(error.localizedDescription) )
    })
  }
}
