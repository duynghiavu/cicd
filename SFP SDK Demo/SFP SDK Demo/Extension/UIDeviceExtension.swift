//
//  UIDeviceExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/5/18.
//

import UIKit

extension UIDevice {
    
    class var isTypeOfIphoneX: Bool {
        if #available(iOS 11.0, *) {
            if let top = UIApplication.shared.keyWindow?.safeAreaInsets.top {
                return top > CGFloat(0)
            }
        }
        return false
    }
    
    class var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    class var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    class var isIphoneOrIPad: Bool {
        return isIpad || isIPhone
    }
    
    class var safeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            if let insets = UIApplication.shared.keyWindow?.safeAreaInsets {
                return insets
            }
        }
        return .zero
    }
    
    class var isIphone5: Bool {
        return UIScreen.main.bounds.size.width < 321.0
    }
    
    class var isIphone678: Bool {
        return abs(UIScreen.main.bounds.size.width - (ScreenSize.iPhone678.width + 0.1)) < 1.0
    }
    
    class var isIphone678Plus: Bool {
        return abs(UIScreen.main.bounds.size.width - (ScreenSize.iPhone678Plus.width + 0.1)) < 1.0
    }
    
}
