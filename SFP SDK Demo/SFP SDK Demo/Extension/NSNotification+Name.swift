//
//  NSNotificationName.swift
//  ViViet
//
//  Created by Long Hoang Giang on 6/7/19.
//

import UIKit

extension NSNotification.Name {
  // keyboard notification
  public static let keyboardWillShow = UIResponder.keyboardWillShowNotification
  public static let keyboardDidShow = UIResponder.keyboardDidShowNotification
  public static let keyboardWillHide = UIResponder.keyboardWillHideNotification
  public static let keyboardDidHide = UIResponder.keyboardDidHideNotification
  public static let keyboardDidChangeFrame = UIResponder.keyboardDidChangeFrameNotification
  // application notification
  public static let didBecomeActiveNotification = UIApplication.didBecomeActiveNotification
  public static let didEnterBackgroundNotification = UIApplication.didEnterBackgroundNotification
  public static let willEnterForegroundNotification = UIApplication.willEnterForegroundNotification
  public static let didFinishLaunchingNotification = UIApplication.didFinishLaunchingNotification
  public static let didReceiveMemoryWarningNotification = UIApplication.didReceiveMemoryWarningNotification
  public static let willChangeStatusBarOrientationNotification = UIApplication.willChangeStatusBarOrientationNotification
  public static let didChangeStatusBarOrientationNotification = UIApplication.didChangeStatusBarOrientationNotification
  // app notification
  public static let appDidBecomeActive = Notification.Name(rawValue: "appDidBecomeActive")
  public static let appMainContainerSwitchVC = Notification.Name(rawValue: "appMainContainerSwitchVC")
  public static let appMainContainerGoHome = Notification.Name(rawValue: "appMainContainerGoHome")
  public static let appTimerReset = Notification.Name(rawValue: "appTimerReset")
  public static let appTimerStop = Notification.Name(rawValue: "appTimerStop")
  public static let sessionExpired = Notification.Name(rawValue: "sessionExpired")
  public static let goHomeAndLogout = Notification.Name(rawValue: "goHomeAndLogout")
  public static let appLanguageChanged = Notification.Name(rawValue: "appLanguageChanged")
  public static let forceHome = Notification.Name(rawValue: "forceHome")
  // otp notification
  public static let otpPauseTimer = Notification.Name(rawValue: "otpPauseTimer")
  public static let otpResetTimer = Notification.Name(rawValue: "otpResetTimer")
  public static let otpContinueTimer = Notification.Name(rawValue: "otpContinueTimer")
  public static let otpStopTimer = Notification.Name(rawValue: "otpStopTimer")
  public static let otpDismiss = Notification.Name(rawValue: "otpDismiss")
  public static let otpDismissAndGoHome = Notification.Name(rawValue: "otpDismissAndGoHome")
  public static let otpClearValue = Notification.Name(rawValue: "otpClearValue")
  public static let otpDidDismiss = Notification.Name(rawValue: "otpDidDismiss")
  public static let otpShowLoading = Notification.Name(rawValue: "otpShowLoading")
  public static let otpHideLoading = Notification.Name(rawValue: "otpHideLoading")
  
  public static let tabControllerHideTabBar = Notification.Name(rawValue: "tabControllerHideTabBar")
  public static let tabControllerShowTabBar = Notification.Name(rawValue: "tabControllerShowTabBar")
  public static let tabControllerDidShowTabBar = Notification.Name(rawValue: "tabControllerDidShowTabBar")
  public static let didLoadMobileConfig = Notification.Name(rawValue: "didLoadMobileConfig")
  public static let viewModelClearValue = Notification.Name(rawValue: "viewModelClearValue")
  
  
}
