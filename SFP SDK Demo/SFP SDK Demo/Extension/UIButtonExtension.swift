//
//  UIButtonExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/5/18.
//

import UIKit

extension UIButton {
    
   func setBackgroundColor(color: UIColor, for state: UIControl.State) {
        setBackgroundImage(UIImage.from(color: color), for: state)
    }
    
    func addTarget(_ target: Any?, action: Selector) {
        addTarget(target, action: action, for: .touchUpInside)
    }
    
    
}
