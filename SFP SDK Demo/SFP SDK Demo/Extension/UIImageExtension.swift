//
//  UIImageExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/4/18.
//

import UIKit

extension UIImage {
    
    var hasAlpha: Bool {
        guard let alpha: CGImageAlphaInfo = self.cgImage?.alphaInfo else { return false }
        return alpha == .first || alpha == .last || alpha == .premultipliedFirst || alpha == .premultipliedLast
    }
    
    var base64String: String {
        return self.pngData()?.base64EncodedString(options: .init(rawValue: 0)) ?? ""
    }
    
    convenience init(view: UIView) {
        if let image = view.snapshot() {
            self.init(cgImage: image.cgImage!)
        } else {
            self.init()
        }
    }
    
    class func initWithName(_ name: String) -> UIImage? {
        return UIImage(named: name, in: nil, compatibleWith: nil)
    }
    
    class func from(color: UIColor) -> UIImage? {
        return from(color: color, size: CGSize(width: 1.0, height: 1.0))
    }
    
    class func from(color: UIColor, size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        defer {
            UIGraphicsEndImageContext()
        }
        color.setFill()
        UIRectFill(CGRect(origin: .zero, size: size))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resize(newWidth: CGFloat, newHeight: CGFloat, scale: CGFloat = 0.0) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
        let hasAlpha = self.hasAlpha
        UIGraphicsBeginImageContextWithOptions(rect.size, !hasAlpha, scale)
        defer {
            UIGraphicsEndImageContext()
        }
        draw(in: rect)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    func resize(newWidth: CGFloat, scale: CGFloat = 0.0) -> UIImage {
        let newHeight: CGFloat = (newWidth * size.height) / max(1.0, size.width)
        return resize(newWidth: newWidth, newHeight: newHeight, scale: scale)
    }
    
    func resize(newHeight: CGFloat, scale: CGFloat = 0.0) -> UIImage {
        let newWidth: CGFloat = (newHeight * size.width) / max(1.0, size.height)
        return resize(newWidth: newWidth, newHeight: newHeight, scale: scale)
    }
    
    func resize(_ size: CGSize, scale: CGFloat = UIScreen.main.scale) -> UIImage {
        return resize(newWidth: size.width, newHeight: size.height, scale: scale)
    }
    
    func resize(scale: CGFloat) -> UIImage? {
        let newSize = CGSize(width: self.size.width*scale, height: self.size.height*scale)
        let rect = CGRect(origin: CGPoint.zero, size: newSize)
        
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
    
    func imageBlur(opacity: CGFloat) -> UIImage? {
        guard let ciImage = CIImage(image: self) else { return nil }
        
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(opacity, forKey: kCIInputRadiusKey)
        
        guard let outputImage = blurFilter?.outputImage else { return nil }
        
        return UIImage(ciImage: outputImage)
    }
}
