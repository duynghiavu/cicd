//
//  UIFontExtension.swift
//  LVTFace
//
//  Created by Long Hoang Giang on 8/20/20.
//  Copyright © 2020 Liên Việt Technology. All rights reserved.
//

import UIKit

enum FontTypeface: String {
    case Regular = "Roboto-Regular"
    case Italic = "Roboto-Italic"
    case Medium = "Roboto-Medium"
    case Bold = "Roboto-Bold"
}

extension UIFont {
    
    convenience init(typeface: FontTypeface, size: CGFloat) {
        self.init(name: typeface.rawValue, size: size)!
    }
    
//    var bold: UIFont { return withWeight(.bold) }
//    var medium: UIFont { return withWeight(.medium) }
//    var semibold: UIFont { return withWeight(.semibold) }
//
//    private func withWeight(_ weight: UIFont.Weight) -> UIFont {
//        var attributes = fontDescriptor.fontAttributes
//        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
//
//        traits[.weight] = weight
//
//        attributes[.name] = nil
//        attributes[.traits] = traits
//        attributes[.family] = familyName
//
//        let descriptor = UIFontDescriptor(fontAttributes: attributes)
//
//        return UIFont(descriptor: descriptor, size: pointSize)
//    }
    
}
