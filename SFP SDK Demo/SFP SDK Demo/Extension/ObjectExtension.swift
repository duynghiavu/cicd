//
//  ObjectExtension.swift
//  MidasWallet
//
//  Created by LamND on 10/22/18.
//  Copyright © 2018 Midas Core Pte Ltd. All rights reserved.
//

import Foundation

extension NSObject {
    class func className() -> String {
        let parts = String(describing: self).components(separatedBy: ".")
        return parts.count == 2 ? parts[1] : parts[0]
    }
    
    func className() -> String {
        return type(of: self).className()
    }
    
    var identifier:UInt {
        return UInt(bitPattern: ObjectIdentifier(self))
    }
}
