//
//  DictionaryExtension.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 7/13/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation
typealias KeyValue = [String:Any]
extension Dictionary where Key == String {
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)]
        }
    }
    
    func contain(_ key:String) -> Bool {
        let tmp = self[key]
        return !(tmp is NSNull || tmp == nil)
    }
    
    func array<T>(of key:String, defaultValue:[T] = [T]()) -> [T] {
        if self.contain(key) {
            guard let result = self[key] as? [T] else {
                LogHelper.log("Error: Cannot cast Array to [\(String.init(describing: T.self))]")
                return defaultValue
            }
            return result
        }
        return defaultValue
    }
    
    func dictionary(of keys:String..., defaultValue:[String:Any] = [String:Any]()) -> [String:Any] {
        for key in keys {
            if self.contain(key) {
                return self[key] as? [String:Any] ?? defaultValue
            }
        }
        return defaultValue
    }
    
    func string(of keys:String..., defaultValue:String = "") -> String {
        for key in keys {
            if self.contain(key) {
                return String.init(describing: self[key]!)
            }
        }
        return defaultValue
    }
    
    func bool(of keys: String..., defaultValue:Bool = false) -> Bool {
        for k in keys {
            if let val = self[k] {
                if let b = val as? Bool { return b }    // Check if value is bool
                if let b = Bool("\(val)") { return b }  // Check if value is a string representation of the Boolean.
                if let num = Int("\(val)") { return num != 0 }  // Check if value is a number and not zero
                return defaultValue
            }
        }
        return defaultValue
    }
    
    func int(of keys:String..., defaultValue:Int = 0) -> Int {
        for key in keys {
            if self.contain(key) {
                return Int(double(of: key, defaultValue:Double(defaultValue)))
            }
        }
        return defaultValue
    }
    
    func double(of keys:String..., defaultValue:Double = 0) -> Double {
        for key in keys {
            if self.contain(key) {
                return Double(string(of: key)) ?? defaultValue
            }
        }
        return defaultValue
    }
    
    /// Get date from dictionary
    ///
    /// - Parameters:
    ///   - keys: key of date field in dictionary
    ///   - format: if format is nil, create date from timeIntervalSince1970 else create date from format (example: dd/MM/yyyy)
    /// - Returns: date corresponding with keys
//    func date(of keys:String..., format:String? = nil) -> Date {
//        if let format = format {
//            for key in keys {
//                if self.contain(key) {
//                    return Date.init(self.string(of: key), pattern: format)
//                }
//            }
//        } else {
//            for key in keys {
//                if self.contain(key) {
//                    return Date.init(timeIntervalSince1970: self.double(of: key))
//                }
//            }
//        }
//        return Date.init(timeIntervalSince1970: 0)
//    }
}
