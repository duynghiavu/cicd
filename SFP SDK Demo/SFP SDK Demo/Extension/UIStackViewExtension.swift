//
//  UIStackViewExtension.swift
//  ViViet
//
//  Created by Long Hoang Giang on 7/12/19.
//

import UIKit
import SnapKit

extension UIStackView {
  
  convenience init(axis: NSLayoutConstraint.Axis, spacing: CGFloat = 0) {
    self.init()
    self.axis = axis
    self.spacing = spacing
    self.translatesAutoresizingMaskIntoConstraints = false
  }
 
  func addArrangedSubviews(_ views: UIView...) {
    views.forEach { self.addArrangedSubview($0) }
  }
  
  func removeArrangedSubviews(_ views: UIView...) {
    views.forEach { self.removeArrangedSubview($0) }
    // Deactivate all constraints
    NSLayoutConstraint.deactivate(views.flatMap({ $0.constraints }))
    // Remove the views from self
    views.forEach({ $0.removeFromSuperview() })
  }
  
  func removeAllArrangedSubviews() {
    let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
      self.removeArrangedSubview(subview)
      return allSubviews + [subview]
    }
    
    // Deactivate all constraints
    NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
    
    // Remove the views from self
    removedSubviews.forEach({ $0.removeFromSuperview() })
  }
  
  func hideViews(_ views: UIView...) {
    views.forEach { $0.isHidden = true }
  }
  
  func showViews(_ views: UIView...) {
    views.forEach { $0.isHidden = false }
  }
  
}


