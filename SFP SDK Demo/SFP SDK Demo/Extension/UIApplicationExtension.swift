//
//  UIApplicationExtension.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 1/13/22.
//  Copyright © 2022 LienVietPostBank. All rights reserved.
//

import UIKit

extension UIApplication {
    
    @objc class var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    @objc class var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    class var safeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *), UIDevice.isTypeOfIphoneX, let keyWindow = UIApplication.shared.keyWindow {
            return keyWindow.safeAreaInsets
        }
        return .zero
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController, includeMain: Bool = false) -> UIViewController? {
        if let navController = controller as? UINavigationController {
            return topViewController(controller: navController.visibleViewController, includeMain: includeMain)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected, includeMain: includeMain)
            }
        }
        if let controller = controller, let presented = controller.presentedViewController {
            return topViewController(controller: presented, includeMain: includeMain)
        }
        return controller
    }
    
}
