//
//  UIViewExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/2/18.
//

import UIKit

public enum BorderType {
   case Top
   case Bottom
   case TopBottom
   case Left
   case Right
   case LeftRight
   case All
}

public extension UIView {
   
   var leftBottomPoint: CGPoint {
      return CGPoint(x: frame.origin.x, y: frame.origin.y + ((bounds.size.height > 0) ? bounds.size.height : frame.size.height))
   }
   
   class func instantiateFromNib<T: UIView>(viewType: T.Type) -> T {
      let nibName = String(describing: viewType)
      return Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)?.first as! T
   }
   
   class func instantiateFromNib() -> Self {
      return instantiateFromNib(viewType: self)
   }
   
   func addSubViews(_ views: UIView...) {
      views.forEach { self.addSubview($0) }
   }
   
   func loadNib() -> UIView {
      let bundle = Bundle(for: type(of: self))
      let nibName = type(of: self).description().components(separatedBy: ".").last!
      let nib = UINib(nibName: nibName, bundle: bundle)
      return nib.instantiate(withOwner: self, options: nil).first as! UIView
   }
   
   func findConstraint(attribute: NSLayoutConstraint.Attribute, identifier: String? = nil) -> NSLayoutConstraint? {
      var constraints: [NSLayoutConstraint] = superview?.constraints ?? []
      constraints += self.constraints
      
      for constraint in constraints {
         if constraint.firstItem === self && (constraint.firstAttribute == attribute) && constraint.isActive {
            if String.isEmpty(identifier) {
               return constraint
            }
            if let _identifierOfConstraint = constraint.identifier, _identifierOfConstraint == identifier! {
               return constraint
            }
         }
      }
      return nil
   }
   
   func addConstraints(format: String, options: NSLayoutConstraint.FormatOptions = NSLayoutConstraint.FormatOptions(), views: UIView...) {
      addConstraints(format: format, options: options, views: views)
   }
   
   func addConstraints(format: String, options: NSLayoutConstraint.FormatOptions = NSLayoutConstraint.FormatOptions(), views: [UIView]) {
      var viewsArgs = [String: Any]()
      for (index, view) in views.enumerated() {
         viewsArgs["v\(index)"] = view
      }
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: viewsArgs))
   }
   
   // childview in center y of parent
   func addConstraintCenterY(view: UIView) {
      addConstraint(NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0))
   }
   
   // childview in center x of parent
   func addConstraintCenterX(view: UIView) {
      addConstraint(NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0))
   }
   
   // add childview in center of parent
   func addConstraintCenterXY(view: UIView) {
      addConstraintCenterX(view: view)
      addConstraintCenterY(view: view)
   }
   
   func removeAllConstraints() {
      guard superview != nil else { return }
      var spV: UIView? = superview
      while spV != nil {
         for constraint in (spV?.constraints ?? []) {
            if constraint.firstItem === self || constraint.secondItem === self {
               spV?.removeConstraint(constraint)
            }
         }
         spV = spV?.superview
      }
      self.translatesAutoresizingMaskIntoConstraints = true
   }
   
   // ham render view -> image
   func snapshot() -> UIImage? {
      if #available(iOS 10.0, *) {
         let renderer = UIGraphicsImageRenderer(bounds: bounds)
         return renderer.image(actions: { (renderContext) in
            layer.render(in: renderContext.cgContext)
         })
      } else {
         UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0.0)
         defer { UIGraphicsEndImageContext() }
         guard let context = UIGraphicsGetCurrentContext() else { return nil }
         layer.render(in: context)
         return UIGraphicsGetImageFromCurrentImageContext()
      }
   }
   
   func snapshotView() -> UIView? {
      if let image = self.snapshot() {
         let snapshotView = UIImageView(image: image)
         return snapshotView
      }
      return nil
   }
   
   
   func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
      layer.masksToBounds = true
      if #available(iOS 11.0, *) {
         layer.cornerRadius = radius
         layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
      } else {
         let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         layer.mask = mask
      }
   }
   
   func shake() {
      let animation = CAKeyframeAnimation()
      animation.keyPath = "position.x"
      animation.values = [-20, 20, -20, 20, -10, 10, -5, 5, 0]
      animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
      //    animation.keyTimes = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
      animation.duration = 0.5
      animation.isAdditive = true
      self.layer.add(animation, forKey: "shake")
   }
   
   func addDoubleTapGesture(target: Any?, action: Selector?, cancelInView: Bool = true) {
      let tap = UITapGestureRecognizer(target: target, action: action)
      tap.numberOfTapsRequired = 2
      tap.cancelsTouchesInView = cancelInView
      addGestureRecognizer(tap)
   }
   
   // neu tableview, collectionview su dung thi de thuoc tinh cancelInView = false
   func addTapGesture(target: Any?, action: Selector?, cancelInView: Bool = true, delegate: UIGestureRecognizerDelegate? = nil) {
     let tap = UITapGestureRecognizer(target: target, action: action)
     tap.cancelsTouchesInView = cancelInView
     tap.delegate = delegate
     addGestureRecognizer(tap)
   }
   
   var parentVC: UIViewController? {
      var parentResponder: UIResponder? = self
      while parentResponder != nil {
         parentResponder = parentResponder!.next
         if let viewController = parentResponder as? UIViewController {
            return viewController
         }
      }
      return nil
   }
   
   var rootView: UIView {
      var rootView = self
      while rootView.superview != nil {
         rootView = rootView.superview!
      }
      return rootView
   }
   
   func contains(_ point: CGPoint, from fromView: UIView) -> Bool {
      return bounds.contains(convert(point, from: fromView))
   }
   
    func addBorder(type: BorderType, color: UIColor = .black, thickness: CGFloat = 0.5, tag: Int = 0) {
      func border() -> UIView {
         let border = UIView(frame: .zero)
         border.backgroundColor = color
         border.translatesAutoresizingMaskIntoConstraints = false
         border.tag = tag
         return border
      }
      if type == .Top || type == .All || type == .TopBottom {
         let top = border()
         addSubview(top)
         addConstraints(format: "H:|-0-[v0]-0-|", views: top)
         addConstraints(format: String(format: "V:|-0-[v0(%f)]", thickness), views: top)
      }
      if type == .Right || type == .All || type == .LeftRight {
         let right = border()
         addSubview(right)
         addConstraints(format: String(format: "H:[v0(%f)]-0-|", thickness), views: right)
         addConstraints(format: "V:|-0-[v0]-0-|", views: right)
      }
      if type == .Bottom || type == .All || type == .TopBottom {
         let bottom = border()
         addSubview(bottom)
         addConstraints(format: "H:|-0-[v0]-0-|", views: bottom)
         addConstraints(format: String(format: "V:[v0(%f)]-0-|", thickness), views: bottom)
      }
      if type == .Left || type == .All || type == .LeftRight {
         let left = border()
         addSubview(left)
         addConstraints(format: String(format: "H:|-0-[v0(%f)]", thickness), views: left)
         addConstraints(format: "V:|-0-[v0]-0-|", views: left)
      }
   }
   
   func addBottomRoundedEdge(desiredCurve: CGFloat?) {
      let offset: CGFloat = self.frame.width / desiredCurve!
      let bounds: CGRect = self.bounds
      
      let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
      let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
      let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: 0, width: bounds.size.width + offset, height: bounds.size.height)
      let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
      rectPath.append(ovalPath)
      
      // Create the shape layer and set its path
      let maskLayer: CAShapeLayer = CAShapeLayer()
      maskLayer.frame = bounds
      maskLayer.path = rectPath.cgPath
      
      // Set the newly created shape layer as the mask for the view's layer
      self.layer.mask = maskLayer
   }
   
   func setShadowWithCornerRadius(corners : CGFloat, fillColor: UIColor = .white, shadowColor: UIColor = UIColor.black.withAlphaComponent(0.1), offset: CGSize = CGSize(width: 1, height: 3), opacity: Float = 1,  shadowRadius: CGFloat = 7){
      if let index = layer.sublayers?.firstIndex(where: { $0.name == AppConstant.Common.shadowRadiusLayerName }) {
         layer.sublayers?.remove(at: index)
      }
      
      let shadowLayer = CAShapeLayer()
      shadowLayer.name = AppConstant.Common.shadowRadiusLayerName
      
      shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: corners).cgPath
      shadowLayer.fillColor = fillColor.cgColor
      
      shadowLayer.shadowColor = shadowColor.cgColor
      shadowLayer.shadowPath = shadowLayer.path
      shadowLayer.shadowOffset = offset
      shadowLayer.shadowOpacity = opacity
      shadowLayer.shadowRadius = shadowRadius
      shadowLayer.shouldRasterize = true
      shadowLayer.rasterizationScale = UIScreen.main.scale
      
      layer.insertSublayer(shadowLayer, at: 0)
      
   }
   
   func addGradientBackground(colors: [CGColor], startPoint: CGPoint = CGPoint(x: 0.5, y: 1.0), endPoint: CGPoint = CGPoint(x: 0.5, y: 0.0)) {
      if let gradientLayer = layer.sublayers?.first as? CAGradientLayer {
         gradientLayer.frame = bounds
         gradientLayer.colors = colors
      } else {
         let gradientLayer = CAGradientLayer()
         gradientLayer.colors = colors
         gradientLayer.startPoint = startPoint
         gradientLayer.endPoint = endPoint
         gradientLayer.locations = [0, 1]
         gradientLayer.frame = bounds
         
         layer.insertSublayer(gradientLayer, at: 0)
      }
   }
}
