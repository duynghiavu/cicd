//
//  UIViewControllerExtension.swift
//  SacombankPay
//
//  Created by Long Hoang Giang on 4/24/18.
//

import UIKit


extension UIViewController {
  
  func addChild(_ controller: UIViewController, to targetView: UIView) {
    self.addChild(controller)
    let childView: UIView = controller.view
    childView.frame = targetView.bounds
    targetView.addSubview(childView)
    controller.didMove(toParent: self)
  }
  
  func removeChildViewController(_ controller: UIViewController) {
    controller.willMove(toParent: nil)
    controller.view.removeFromSuperview()
    controller.removeFromParent()
  }
  
  @objc func onBackPressed() {
    popNavigationOrDismiss(true)
  }
  
  func popNavigationOrDismiss(_ animated: Bool, completion: (() -> Void)? = nil) {
    if let nav = self as? UINavigationController, nav.viewControllers.count > 1 {
      nav.popViewController(animated: animated)
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { completion?() }
    } else if let nav = navigationController, nav.viewControllers.count > 1 {
      nav.popViewController(animated: animated)
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { completion?() }
    } else {
      dismiss(animated: animated) {
        completion?()
      }
    }
  }
  
  @objc var endEditingExceptView: UIView? { return nil }
  @objc func viewDidTapped(_ gesture: UIGestureRecognizer) {
    if let exceptView = self.endEditingExceptView,
      exceptView.bounds.contains(gesture.location(in: exceptView)) {
      return
    }
    view.endEditing(true)
  }

  func hideKeyboardOnTap(_ cancelInView: Bool = false, delegate: UIGestureRecognizerDelegate? = nil) {
    view.addTapGesture(target: self, action: #selector(viewDidTapped(_:)), cancelInView: cancelInView, delegate: delegate)
  }
  
  func screenLog() {
    print("System: \(NSStringFromClass(self.classForCoder))------")
  }
}

extension UIViewController {
  
  var safeAreaInsets: UIEdgeInsets {
    if #available(iOS 11.0, *), let keyWindow = UIApplication.shared.keyWindow {
      return keyWindow.safeAreaInsets
    }
    return .zero
  }
  
  var topAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11, *) {
      return view.safeAreaLayoutGuide.topAnchor
    }
    return topLayoutGuide.bottomAnchor
  }
  
  var bottomAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11, *) {
      return view.safeAreaLayoutGuide.bottomAnchor
    }
    return bottomLayoutGuide.topAnchor
  }
  
  @discardableResult
  func anchorViewOnTop(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint? {
    let constraint = topAnchor.constraint(equalTo: view.topAnchor, constant: constant)
    constraint.isActive = true
    return constraint
  }
  
  @discardableResult
  func anchorViewOnBottom(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint? {
    let constraint = bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: constant)
    constraint.isActive = true
    return constraint
  }

  func removeSubViews(_ subViews: UIView...) {
    for view in subViews {
      view.removeFromSuperview()
    }
  }
}
