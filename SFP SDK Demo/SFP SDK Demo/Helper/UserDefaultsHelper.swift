//
//  UserDefaultsHelper.swift
//  SFP SDK Demo
//
//  Created by NghiaVD on 10/27/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import Foundation

enum UserDefaultsKey: String {
    case customerName
    case username
    case password
    case merchantCode
    
    case ticketData
    case searchFilm
    case addressHome
    case addressOffice
    case addressNear
    
    case shopItems
}

class UserDefaultsHelper: UserDefaults {
    static private var shared: UserDefaultsHelper!
    
    class func share() -> UserDefaultsHelper {
        if shared == nil {
            shared = UserDefaultsHelper()
        }
        return shared
    }
    
    func set(value: Any, key: UserDefaultsKey) {
        setValue(value, forKey: key.rawValue)
    }
    
    func contain(key: UserDefaultsKey) -> Bool {
        return value(forKey: key.rawValue) != nil
    }
    
    //MARK: -
    func getString(key: UserDefaultsKey, defaultValue: String = "") -> String {
        return value(forKey: key.rawValue) as? String ?? defaultValue
    }
    
    func getBool(key: UserDefaultsKey, defaultValue: Bool = false) -> Bool {
        return value(forKey: key.rawValue) as? Bool ?? defaultValue
    }
    
    func getData(key: UserDefaultsKey, defaultValue: Data = Data()) -> Data {
        return value(forKey: key.rawValue) as? Data ?? defaultValue
    }
    
    func getDic(key: UserDefaultsKey, defaultValue: [String: Any] = [:]) -> [String: Any] {
        return value(forKey: key.rawValue) as? [String: Any] ?? defaultValue
    }
    
    func getArray(key: UserDefaultsKey, defaultValue: [Any] = []) -> [Any] {
        return value(forKey: key.rawValue) as? [Any] ?? defaultValue
    }
    
    //MARK: -
    func setTicketData(_ data: GetTicketData) {
        set(value: data.toJson(), key: .ticketData)
    }
    
    func getTicketData() -> GetTicketData {
        return GetTicketData(getDic(key: .ticketData))
    }
    
    //MARK: -
    func setShopIds(_ array: [ShopItemModel]) {
        set(value: ShopItemModel.toListJson(array), key: .shopItems)
    }
    
    func getShopIds() -> [ShopItemModel] {
        var result = [ShopItemModel]()
        if let array = getString(key: .shopItems).toJSON() as? [[String: Any]] {
            array.forEach { data in
                result.append(ShopItemModel(data))
            }
        }
        return result
    }
    
    //MARK: -
    func remove(key: UserDefaultsKey) {
        removeObject(forKey: key.rawValue)
    }
}
