//
//  AppDelegate.swift
//  SFP SDK Demo
//
//  Created by Admin on 2/10/20.
//  Copyright © 2020 LienVietPostBank. All rights reserved.
//

import UIKit
import SpiderSDK
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        SpiderSDK.clearCached()
//        UserDefaults.standard.removeObject(forKey: "data")
        
        SpiderSDK.requestTicket = { (type, params, callBack) in
            let model = GetTicketRequest()
            model.type = type
            model.username = UserDefaultsHelper.share().getString(key: .username)
            model.params = params
            
            ServiceAPI.getTicket(model: model) { (success, data, code, message) in
                if success {
                    UserDefaultsHelper.share().setTicketData(data)
                }
                callBack(code, message, data.toJson())
            }
        }
        SpiderSDK.callBack = { (type, data) in
            switch type {
            case .LOGIN_OTHER_DEVICE:
                self.popToRootView()
                
            case .TOKEN_EXPIRED, .TRANSACTION_CANCEL, .BACK_TO_HOME, .LOGIN_OUT:
                self.popToHomeView(data["isNeedReload"] as? Bool ?? true)
                
            case .TRANSACTION_DETAIL:
                if let topVC = UIApplication.topViewController() {
                    let vc: TransDetailVC = TransDetailVC.newInstance()
                    if let str = data["transId"] as? String {
                        vc.transId = str
                    }
                    if let str = data["transTime"] as? String {
                        vc.transTime = str
                    }
                    if let str = data["transDesc"] as? String {
                        vc.transDesc = str
                    }
                    if let str = data["transAmount"] as? String {
                        vc.transAmount = str
                    }
                    vc.modalPresentationStyle = .custom
                    topVC.show(vc, sender: nil)
                }
            }
        }
        SpiderSDK.config()
        
        GMSServices.provideAPIKey(ServiceAPI.googleAPI)
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        }
        
        return true
    }
    
    // MARK:
    func popToHomeView(_ isNeedReload: Bool = true) {
        if let topVC = UIApplication.topViewController() {
            var nav: UINavigationController?
            if let vc = topVC as? UINavigationController {
                nav = vc
            }
            else if let vc = topVC.navigationController {
                nav = vc
            }
            
            if let nav = nav {
                nav.viewControllers.forEach { vc in
                    if let vc = vc as? HomeTabVC {
                        vc.tab1VC.isNeedReload = isNeedReload
                        nav.popToViewController(vc, animated: true)
                        return
                    }
                }
            }
            else {
                self.dismissToHomeView()
            }
        }
    }
    
    func dismissToHomeView() {
        if let topVC = UIApplication.topViewController() {
            topVC.dismiss(animated: false) {
                self.popToHomeView()
            }
        }
    }
    
    // MARK:
    func popToRootView() {
        if let topVC = UIApplication.topViewController() {
            print("popToRootView: \(topVC)")
            if let nav = topVC as? UINavigationController {
                nav.popToRootViewController(animated: true)
            }
            else if let nav = topVC.navigationController {
                nav.popToRootViewController(animated: true)
            }
            else {
                self.dismissToRootView()
            }
        }
    }
    
    func dismissToRootView() {
        if let topVC = UIApplication.topViewController() {
            print("dismissToRootView: \(topVC)")
            topVC.dismiss(animated: false) {
                self.popToRootView()
            }
        }
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

